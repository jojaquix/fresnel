unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Fresnel.Controls,
  Fresnel.DOM, fresnel.events, fcl.events,Fresnel.Layouter, Fresnel.LCLControls;

type

  { TForm1 }

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    procedure DoAdditionalClickEvent(Event: TAbstractEvent);
    procedure DoClick(Event: TAbstractEvent);

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
var
  Body1: TBody;
  Div1, Div2: TDiv;
  Label1: TLabel;
  ViewPort: TFresnelViewport;
  Layouter: TSimpleFresnelLayouter;
  Fresnel1: TFresnelLCLControl;
  Span1: TSpan;
begin
  TFresnelEventDispatcher.RegisterFresnelEvents;
  Fresnel1:=TFresnelLCLControl.Create(Self);
  with Fresnel1 do
  begin
    Name:='Fresnel1';
    Align:=alClient;
    Viewport.Stylesheet.Text:='div { padding: 2px; border: 3px; margin: 6px; }';
    Parent:=Self;
  end;
  ViewPort:=Fresnel1.Viewport;
  Layouter:=Fresnel1.Layouter;

  Body1:=TBody.Create(Self);
  with Body1 do begin
    Name:='Body1';
    Parent:=ViewPort;
    Style:='border: 2px; border-color: blue;';
    OnClick:=@DoClick;
  end;

  Div1:=TDiv.Create(Self);
  with Div1 do begin
    Name:='Div1';
    Parent:=Body1;
    Style:='background-color: blue; border-color: black; height:50px;';
    OnClick:=@DoClick;
  end;

  Span1:=TSpan.Create(Self);
  with Span1 do begin
    Name:='Span1';
    Parent:=Body1;
    Style:='width: 50px; height:70px; background-color: red; border: 3px; border-color: black; margin: 3px;';
    OnClick:=@DoClick;
  end;

  Label1:=TLabel.Create(Self);
  with Label1 do
  begin
    Name:='Label1';
    Caption:='Label1Caption';
    Parent:=Body1;
    Style:='background-color: green; ';
    OnClick:=@DoClick;
    AddEventListener('click',@DoAdditionalClickEvent);
  end;

  Div2:=TDiv.Create(Self);
  with Div2 do begin
    Name:='Div2';
    Parent:=Body1;
    Style:='border-color: black; height:50px; position: absolute; left: 30px; top: 100px; width: 50px; height: 60px;';
    OnClick:=@DoClick;

  end;
end;

procedure TForm1.DoAdditionalClickEvent(Event: TAbstractEvent);
begin
  ShowMessage('We repeat: You clicked '+(Event.sender as TComponent).Name);
end;

procedure TForm1.DoClick(Event: TAbstractEvent);
begin
  ShowMessage('You clicked '+(Event.sender as TComponent).Name);
end;

end.

