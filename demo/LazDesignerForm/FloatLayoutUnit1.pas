unit FloatLayoutUnit1;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.LCLControls, Fresnel.DOM, Fresnel.Controls;

type

  { TFresnelForm2 }

  TFresnelForm2 = class(TFresnelForm)
    Body1: TBody;
    Div1: TDiv;
    Div2: TDiv;
    Label1: TLabel;
    Span1: TSpan;
  private

  public

  end;

var
  FresnelForm2: TFresnelForm2;

implementation

{$R *.lfm}

end.

