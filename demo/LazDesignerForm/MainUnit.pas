unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  FloatLayoutUnit1;

type

  { TForm1 }

  TForm1 = class(TForm)
    ShowFloatLayoutButton: TButton;
    procedure ShowFloatLayoutButtonClick(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.ShowFloatLayoutButtonClick(Sender: TObject);
var
  Frm: TFresnelForm2;
begin
  Frm:=TFresnelForm2.Create(nil);
  Frm.Show;
end;

end.

