unit fresnel.lclevents;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fresnel.events, controls;

Function InitMouseEvent(aCtrl : TControl; Out aInit : TFresnelMouseEventInit) : Boolean;

implementation

Function InitMouseEvent(aCtrl : TControl; Out aInit : TFresnelMouseEventInit) : Boolean;

begin
  aInit.ScreenPos:=Mouse.CursorPos;
  aInit.PagePos:=aCtrl.ScreenToClient(aInit.ScreenPos);
  Result:=True;
end;

end.

