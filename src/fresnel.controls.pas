unit Fresnel.Controls;

{$mode ObjFPC}{$H+}
{$IF FPC_FULLVERSION>30300}
{$WARN 6060 off} // Case statement does not handle all possible cases
{$ENDIF}

interface

uses
  Classes, SysUtils, Math, Fresnel.Dom, LazLoggerBase, fpCSSResolver,
  fpCSSTree;

type

  { TDiv }

  TDiv = class(TFresnelElement)
  private
    class var FFresnelDivTypeID: TCSSNumericalID;
    class constructor InitFresnelDivClass;
  public
    function GetCSSInitialAttribute(const AttrID: TCSSNumericalID): TCSSString;
      override;
    procedure ClearCSSValues; override;
    class function CSSTypeID: TCSSNumericalID; override;
    class function CSSTypeName: TCSSString; override;
  end;

  { TSpan }

  TSpan = class(TFresnelElement)
  private
    class var FFresnelSpanTypeID: TCSSNumericalID;
    class constructor InitFresnelSpanClass;
  public
    function GetCSSInitialAttribute(const AttrID: TCSSNumericalID): TCSSString;
      override;
    procedure ClearCSSValues; override;
    class function CSSTypeID: TCSSNumericalID; override;
    class function CSSTypeName: TCSSString; override;
  end;

  { TReplacedElement }

  TReplacedElement = class(TFresnelElement)
  public
    procedure GetMinMaxPreferred(out MinWidth, MinHeight, PreferredWidth, PreferredHeight, MaxWidth, MaxHeight: TFresnelLength); virtual;
    function AcceptChildrenAtDesignTime: boolean; override;
  end;

  TFresnelLabelState = (
    flsMinCaptionValid,
    flsMinWidthValid,
    flsSizeValid
    );
  TFresnelLabelStates = set of TFresnelLabelState;

  { TCustomLabel }

  TCustomLabel = class(TReplacedElement)
  private
    class var FFresnelLabelTypeID: TCSSNumericalID;
    class constructor InitFresnelLabelClass;
  private
    FCaption: String;
    FRenderedCaption: String;
  protected
    FLabelStates: TFresnelLabelStates;
    FMinCaption: String; // Caption with linebreak after each word
    FMinWidth: TFresnelLength; // width of longest word of Caption
    FSize: TFresnelPoint; // width+height of Caption
    procedure ComputeMinCaption; virtual;
    procedure SetCaption(const AValue: String); virtual;
    procedure SetName(const NewName: TComponentName); override;
  public
    function GetMinWidthIntrinsicContentBox: TFresnelLength; override;
    function GetPreferredContentBox_MaxWidth(MaxWidth: TFresnelLength): TFresnelPoint;
      override;
    function GetCSSInitialAttribute(const AttrID: TCSSNumericalID): TCSSString;
      override;
    procedure ClearCSSValues; override;
    procedure UpdateRenderedAttributes; override;
    class function CSSTypeID: TCSSNumericalID; override;
    class function CSSTypeName: TCSSString; override;
    property Caption: String read FCaption write SetCaption;
    property RenderedCaption: String read FRenderedCaption write FRenderedCaption;
  end;

  { TLabel }

  TLabel = class(TCustomLabel)
  published
    property Caption;
  end;

  { TBody }

  TBody = class(TFresnelElement)
  private
    class var FFresnelBodyTypeID: TCSSNumericalID;
    class constructor InitFresnelBodyClass;
  protected
    procedure SetCSSElAttribute(Attr: TFresnelCSSAttribute; const AValue: string); override;
  public
    class function CSSTypeID: TCSSNumericalID; override;
    class function CSSTypeName: TCSSString; override;
    function GetCSSInitialAttribute(const AttrID: TCSSNumericalID): TCSSString;
      override;
    procedure ClearCSSValues; override;
  end;

implementation

{ TReplacedElement }

procedure TReplacedElement.GetMinMaxPreferred(out MinWidth, MinHeight,
  PreferredWidth, PreferredHeight, MaxWidth, MaxHeight: TFresnelLength);
begin
  MinWidth:=NaN;
  MinHeight:=NaN;
  PreferredWidth:=NaN;
  PreferredHeight:=NaN;
  MaxWidth:=NaN;
  MaxHeight:=NaN;
end;

function TReplacedElement.AcceptChildrenAtDesignTime: boolean;
begin
  Result:=false;
end;

{ TSpan }

class constructor TSpan.InitFresnelSpanClass;
begin
  // register type
  FFresnelSpanTypeID:=RegisterCSSType(CSSTypeName);
end;

function TSpan.GetCSSInitialAttribute(const AttrID: TCSSNumericalID
  ): TCSSString;
var
  Attr: TFresnelCSSAttribute;
begin
  if (AttrID<FresnelElementBaseAttrID) or (AttrID>ord(High(TFresnelCSSAttribute))+FresnelElementBaseAttrID) then
    exit('');
  Attr:=TFresnelCSSAttribute(AttrID-FresnelElementBaseAttrID);
  case Attr of
  fcaDisplayOutside: Result:='inline';
  fcaDisplayInside: Result:='flow';
  else
    Result:=inherited GetCSSInitialAttribute(AttrID);
  end;
end;

procedure TSpan.ClearCSSValues;
begin
  inherited ClearCSSValues;
  FCSSAttributes[fcaDisplayOutside]:='inline';
  FCSSAttributes[fcaDisplayInside]:='flow';
end;

class function TSpan.CSSTypeID: TCSSNumericalID;
begin
  Result:=FFresnelSpanTypeID;
end;

class function TSpan.CSSTypeName: TCSSString;
begin
  Result:='span';
end;

{ TDiv }

class constructor TDiv.InitFresnelDivClass;
begin
  // register type
  FFresnelDivTypeID:=RegisterCSSType(CSSTypeName);
end;

function TDiv.GetCSSInitialAttribute(const AttrID: TCSSNumericalID
  ): TCSSString;
var
  Attr: TFresnelCSSAttribute;
begin
  if (AttrID<FresnelElementBaseAttrID) or (AttrID>ord(High(TFresnelCSSAttribute))+FresnelElementBaseAttrID) then
    exit('');
  Attr:=TFresnelCSSAttribute(AttrID-FresnelElementBaseAttrID);
  case Attr of
  fcaDisplayOutside: Result:='block';
  fcaDisplayInside: Result:='';
  else
    Result:=inherited GetCSSInitialAttribute(AttrID);
  end;
end;

procedure TDiv.ClearCSSValues;
begin
  inherited ClearCSSValues;
  FCSSAttributes[fcaDisplayOutside]:='block';
  FCSSAttributes[fcaDisplayInside]:='';
end;

class function TDiv.CSSTypeID: TCSSNumericalID;
begin
  Result:=FFresnelDivTypeID;
end;

class function TDiv.CSSTypeName: TCSSString;
begin
  Result:='div';
end;

{ TBody }

procedure TBody.SetCSSElAttribute(Attr: TFresnelCSSAttribute;
  const AValue: string);
begin
  case Attr of
  fcaDisplay,
  fcaDisplayBox,
  fcaDisplayInside,
  fcaDisplayOutside,
  fcaZIndex,
  fcaPosition,
  fcaLeft,
  fcaTop,
  fcaBottom,
  fcaRight,
  fcaWidth,
  fcaHeight,
  fcaMinWidth,
  fcaMinHeight,
  fcaMaxWidth,
  fcaMaxHeight: exit;
  end;
  inherited SetCSSElAttribute(Attr,AValue);
end;

class constructor TBody.InitFresnelBodyClass;
begin
  FFresnelBodyTypeID:=RegisterCSSType(CSSTypeName);
end;

class function TBody.CSSTypeID: TCSSNumericalID;
begin
  Result:=FFresnelBodyTypeID;
end;

class function TBody.CSSTypeName: TCSSString;
begin
  Result:='body';
end;

function TBody.GetCSSInitialAttribute(const AttrID: TCSSNumericalID
  ): TCSSString;
var
  Attr: TFresnelCSSAttribute;
begin
  if (AttrID<FresnelElementBaseAttrID) or (AttrID>FresnelElementBaseAttrID+ord(High(TFresnelCSSAttribute))) then
    exit('');
  Attr:=TFresnelCSSAttribute(AttrID-FresnelElementBaseAttrID);
  case Attr of
  fcaDisplayOutside: Result:='block';
  fcaPosition: Result:='absolute';
  else
    Result:=inherited GetCSSInitialAttribute(AttrID);
  end;
end;

procedure TBody.ClearCSSValues;
begin
  inherited ClearCSSValues;
  FCSSAttributes[fcaDisplayOutside]:='block';
  FCSSAttributes[fcaPosition]:='absolute';
end;

{ TCustomLabel }

class constructor TCustomLabel.InitFresnelLabelClass;
begin
  FFresnelLabelTypeID:=RegisterCSSType(CSSTypeName);
end;

procedure TCustomLabel.ComputeMinCaption;
// create FMinCaption from FCaption by putting every word on a line of its own
var
  LineBreakLen, SrcP, l, StartP, WordLen, TargetP: Integer;
  MyLineBreak: string;
begin
  if flsMinCaptionValid in FLabelStates then exit;
  Include(FLabelStates,flsMinCaptionValid);
  if FCaption='' then
  begin
    FMinCaption:='';
    exit;
  end;
  MyLineBreak:=sLineBreak;
  LineBreakLen:=length(MyLineBreak);
  SrcP:=1;
  l:=length(FCaption);
  SetLength(FMinCaption,l);
  TargetP:=1;
  while (SrcP<=l) and (FCaption[SrcP] in [' ',#9]) do inc(SrcP);
  if SrcP>l then
  begin
    // only spaces
    FMinCaption:=' ';
    exit;
  end;
  while SrcP<=l do begin
    StartP:=SrcP;
    while (SrcP<=l) and not (FCaption[SrcP] in [' ',#9]) do inc(SrcP);
    WordLen:=SrcP-StartP;
    if TargetP+WordLen+LineBreakLen>length(FMinCaption) then
      SetLength(FMinCaption,Max(TargetP+WordLen+LineBreakLen,length(FMinCaption)*5 div 4));
    System.Move(FCaption[StartP],FMinCaption[TargetP],WordLen);
    inc(TargetP,WordLen);
    if SrcP<=l then
    begin
      System.Move(MyLineBreak[1],FMinCaption[TargetP],LineBreakLen);
      inc(TargetP,LineBreakLen);
    end;
    while (SrcP<=l) and (FCaption[SrcP] in [' ',#9]) do inc(SrcP);
  end;
  SetLength(FMinCaption,TargetP-1);
end;

procedure TCustomLabel.SetCaption(const AValue: String);
begin
  if FCaption=AValue then Exit;
  FCaption:=AValue;
  FMinCaption:='';
  FLabelStates:=FLabelStates-[flsMinCaptionValid,flsMinWidthValid,flsSizeValid];
  DomChanged;
end;

procedure TCustomLabel.SetName(const NewName: TComponentName);
var
  ChangeCaption: Boolean;
begin
  if Name=NewName then exit;
  ChangeCaption :=
    not (csLoading in ComponentState)
    and (Name = Caption)
    and ((Owner = nil) or not (csLoading in Owner.ComponentState));
  inherited SetName(NewName);
  if ChangeCaption then Caption := NewName;
end;

function TCustomLabel.GetMinWidthIntrinsicContentBox: TFresnelLength;
var
  p: TFresnelPoint;
begin
  if not (flsMinWidthValid in FLabelStates) then
  begin
    if not (flsMinCaptionValid in FLabelStates) then
      ComputeMinCaption;
    p:=Font.TextSize(FMinCaption);
    FMinCaption:='';
    Exclude(FLabelStates,flsMinCaptionValid);
    FMinWidth:=p.X;
    Include(FLabelStates,flsMinWidthValid);
  end;
  Result:=FMinWidth;
end;

function TCustomLabel.GetPreferredContentBox_MaxWidth(MaxWidth: TFresnelLength
  ): TFresnelPoint;
begin
  if not (flsSizeValid in FLabelStates) then
  begin
    FSize:=Font.TextSize(FCaption);
    Include(FLabelStates,flsSizeValid);
  end;
  if MaxWidth>=FSize.X then
    Result:=FSize
  else
    Result:=Font.TextSizeMaxWidth(FCaption,MaxWidth);
end;

function TCustomLabel.GetCSSInitialAttribute(const AttrID: TCSSNumericalID
  ): TCSSString;
var
  Attr: TFresnelCSSAttribute;
begin
  if (AttrID<FresnelElementBaseAttrID) or (AttrID>ord(High(TFresnelCSSAttribute))+FresnelElementBaseAttrID) then
    exit('');
  Attr:=TFresnelCSSAttribute(AttrID-FresnelElementBaseAttrID);
  case Attr of
  fcaDisplayOutside: Result:='inline';
  else
    Result:=inherited GetCSSInitialAttribute(AttrID);
  end;
end;

procedure TCustomLabel.ClearCSSValues;
begin
  inherited ClearCSSValues;
  FCSSAttributes[fcaDisplayOutside]:='inline';
end;

procedure TCustomLabel.UpdateRenderedAttributes;
begin
  inherited UpdateRenderedAttributes;
  FRenderedCaption:=Caption;
end;

class function TCustomLabel.CSSTypeID: TCSSNumericalID;
begin
  Result:=FFresnelLabelTypeID;
end;

class function TCustomLabel.CSSTypeName: TCSSString;
begin
  Result:='label';
end;

end.

