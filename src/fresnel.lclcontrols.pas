unit Fresnel.LCLControls;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Types, Math, FPImage, Fresnel.DOM, Fresnel.Layouter,
  Fresnel.Controls, fresnel.renderer, AvgLvlTree, Laz_AVL_Tree, LazLoggerBase,
  Graphics, Controls, LCLIntf, Forms, LCLStrConsts, LResources, fresnel.events;

type
  TFresnelLCLFontEngine = class;

  { TFresnelLCLFont }

  TFresnelLCLFont = class(TInterfacedObject,IFresnelFont)
  public
    Engine: TFresnelLCLFontEngine;
    Family: string;
    Kerning: string;
    Size: string;
    Style: string;
    Variant_: string;
    Weight: string;
    LCLFont: TFont;
    destructor Destroy; override;
    function GetFamily: string;
    function GetKerning: string;
    function GetSize: string;
    function GetStyle: string;
    function GetVariant: string;
    function GetWeight: string;
    function TextSize(const aText: string): TFresnelPoint; virtual;
    function TextSizeMaxWidth(const aText: string; MaxWidth: TFresnelLength
      ): TFresnelPoint; virtual;
    function GetTool: TObject;
  end;

  { TFresnelLCLFontEngine }

  TFresnelLCLFontEngine = class(TFresnelFontEngine)
  private
    FCanvas: TCanvas;
    FFonts: TAvgLvlTree; // tree of TFresnelLCLFont sorted for
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function FindFont(const Desc: TFresnelFontDesc): TFresnelLCLFont; virtual;
    function Allocate(const Desc: TFresnelFontDesc): IFresnelFont; override;
    function TextSize(aFont: TFresnelLCLFont; const aText: string): TPoint; virtual;
    function TextSizeMaxWidth(aFont: TFresnelLCLFont; const aText: string; MaxWidth: integer): TPoint; virtual;
    function NeedLCLFont(aFont: TFresnelLCLFont): TFont; virtual;
    property Canvas: TCanvas read FCanvas write FCanvas;
  end;

  { TFresnelLCLRenderer }

  TFresnelLCLRenderer = class(TFresnelRenderer)
  private
    FCanvas: TCanvas;
  protected
    procedure FillRect(const aColor: TFPColor; const aRect: TFresnelRect);
      override;
    procedure Line(const aColor: TFPColor; const x1, y1, x2, y2: TFresnelLength); override;
    procedure TextOut(const aLeft, aTop: TFresnelLength;
      const aFont: IFresnelFont; const aColor: TFPColor;
      const aText: string); override;
  public
    constructor Create(AOwner: TComponent); override;
    property Canvas: TCanvas read FCanvas write FCanvas;
  end;

  { TFresnelLCLControl }

  TFresnelLCLControl = class(TCustomControl)
  private
    FClearing: boolean;
    FFontEngine: TFresnelLCLFontEngine;
    FLayouter: TSimpleFresnelLayouter;
    FLayoutQueued: boolean;
    FRenderer: TFresnelLCLRenderer;
    FViewport: TFresnelViewport;
    procedure HandleClick(Sender: TObject);
    procedure OnDomChanged(Sender: TObject);
    procedure OnQueuedLayout({%H-}Data: PtrInt);
    procedure SetLayoutQueued(const AValue: boolean);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Viewport: TFresnelViewport read FViewport;
    property Layouter: TSimpleFresnelLayouter read FLayouter;
    property FontEngine: TFresnelLCLFontEngine read FFontEngine;
    property Renderer: TFresnelLCLRenderer read FRenderer;
    property LayoutQueued: boolean read FLayoutQueued write SetLayoutQueued;
  end;

  { TOldFresnelCustomForm }

  TOldFresnelCustomForm = class(TCustomForm,IFresnelStreamRoot)
  private
    FFontEngine: TFresnelLCLFontEngine;
    FLayouter: TSimpleFresnelLayouter;
    FRenderer: TFresnelLCLRenderer;
    FViewport: TFresnelViewport;
    FLayoutQueued: boolean;
    FClearing: boolean;
    function GetStylesheet: TStrings;
    procedure OnDomChanged(Sender: TObject);
    procedure OnQueuedLayout({%H-}Data: PtrInt);
    procedure SetLayoutQueued(const AValue: boolean);
    procedure SetStylesheet(const AValue: TStrings);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure Paint; override;
    function GetViewport: TFresnelViewport; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    property Viewport: TFresnelViewport read FViewport;
    property FontEngine: TFresnelLCLFontEngine read FFontEngine;
    property Layouter: TSimpleFresnelLayouter read FLayouter;
    property LayoutQueued: boolean read FLayoutQueued write SetLayoutQueued;
    property Renderer: TFresnelLCLRenderer read FRenderer;
    property Stylesheet: TStrings read GetStylesheet write SetStylesheet;
  end;

  { TOldFresnelForm }

  TOldFresnelForm = class(TOldFresnelCustomForm)
  published
    property Stylesheet;
  end;

  IFresnelFormDesigner = interface
    ['{095CB7DD-E291-45B6-892E-F486A38E597C}']
    procedure InvalidateRect(Sender: TObject; ARect: TRect; Erase: boolean);
    procedure SetDesignerFormBounds(Sender: TObject; NewBounds: TRect);
    function GetDesignerClientWidth: integer;
    function GetDesignerClientHeight: integer;
  end;

  { TCustomFresnelForm }

  TCustomFresnelForm = class(TFresnelViewport)
  private
    FCanvas: TCanvas;
    FClearing: boolean;
    FDesigner: IFresnelFormDesigner;
    FFontEngineLCL: TFresnelLCLFontEngine;
    FForm: TCustomForm;
    FLayouter: TSimpleFresnelLayouter;
    FLayoutQueued: boolean;
    FRenderer: TFresnelLCLRenderer;
    FVisible: boolean;
    procedure DoFormClick(Sender: TObject);
    function GetAllowDropFiles: Boolean;
    function GetAlphaBlend: Boolean;
    function GetAlphaBlendValue: Byte;
    function GetBorderIcons: TBorderIcons;
    function GetCaption: TCaption;
    function GetDefaultMonitor: TDefaultMonitor;
    function GetEffectiveShowInTaskBar: TShowInTaskBar;
    function GetFormBorderStyle: TFormBorderStyle;
    function GetFormBoundsRect: TRect;
    function GetFormHeight: integer;
    function GetFormLeft: integer;
    function GetFormState: TFormState;
    function GetFormStyle: TFormStyle;
    function GetFormTop: integer;
    function GetFormWidth: integer;
    function GetIcon: TIcon;
    function GetPosition: TPosition;
    function GetShowInTaskbar: TShowInTaskbar;
    function GetVisible: boolean;
    function GetWindowState: TWindowState;
    function IsIconStored: Boolean;
    procedure OnFormResize(Sender: TObject);
    procedure SetAllowDropFiles(const AValue: Boolean);
    procedure SetAlphaBlend(const AValue: Boolean);
    procedure SetAlphaBlendValue(const AValue: Byte);
    procedure SetBorderIcons(const AValue: TBorderIcons);
    procedure SetCanvas(const AValue: TCanvas);
    procedure SetCaption(const AValue: TCaption);
    procedure SetDefaultMonitor(const AValue: TDefaultMonitor);
    procedure SetFormBorderStyle(const AValue: TFormBorderStyle);
    procedure SetFormBoundsRect(const AValue: TRect);
    procedure SetFormHeight(const AValue: integer);
    procedure SetFormLeft(const AValue: integer);
    procedure SetFormStyle(const AValue: TFormStyle);
    procedure SetFormTop(const AValue: integer);
    procedure SetFormWidth(const AValue: integer);
    procedure SetIcon(const AValue: TIcon);
    procedure SetLayoutQueued(const AValue: boolean);
    procedure SetPosition(const AValue: TPosition);
    procedure SetShowInTaskBar(const AValue: TShowInTaskbar);
    procedure SetVisible(const AValue: boolean);
    procedure SetWindowState(const AValue: TWindowState);
    function VisibleIsStored: Boolean;
  protected
    function GetHeight: TFresnelLength; override;
    function GetWidth: TFresnelLength; override;
    procedure FormPaint(Sender: TObject); virtual;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure OnQueuedLayout({%H-}Data: PtrInt); virtual;
    procedure ProcessResource; virtual;
    procedure SetName(const NewName: TComponentName); override;
    procedure SetHeight({%H-}AValue: TFresnelLength); override;
    procedure SetWidth({%H-}AValue: TFresnelLength); override;
  public
    constructor Create(AOwner: TComponent); override;
    constructor CreateNew(AOwner: TComponent); virtual;
    destructor Destroy; override;
    procedure DomChanged; override;
    procedure Hide;
    procedure Show; virtual;
    function ShowModal: TModalResult; virtual;
    property AllowDropFiles: Boolean read GetAllowDropFiles write SetAllowDropFiles default false;
    property AlphaBlend: Boolean read GetAlphaBlend write SetAlphaBlend default false;
    property AlphaBlendValue: Byte read GetAlphaBlendValue write SetAlphaBlendValue default 255;
    property BorderIcons: TBorderIcons read GetBorderIcons write SetBorderIcons
      default [biSystemMenu, biMinimize, biMaximize];
    property BorderStyle: TFormBorderStyle
                      read GetFormBorderStyle write SetFormBorderStyle default bsSizeable;
    property Canvas: TCanvas read FCanvas write SetCanvas;
    property Caption: TCaption read GetCaption write SetCaption;
    property DefaultMonitor: TDefaultMonitor read GetDefaultMonitor
      write SetDefaultMonitor default dmActiveForm;
    property Designer: IFresnelFormDesigner read FDesigner write FDesigner;
    property EffectiveShowInTaskBar: TShowInTaskBar read GetEffectiveShowInTaskBar;
    property FontEngineLCL: TFresnelLCLFontEngine read FFontEngineLCL;
    property Form: TCustomForm read FForm;
    property FormState: TFormState read GetFormState;
    property FormStyle: TFormStyle read GetFormStyle write SetFormStyle default fsNormal;
    property Icon: TIcon read GetIcon write SetIcon stored IsIconStored;
    property Layouter: TSimpleFresnelLayouter read FLayouter;
    property LayoutQueued: boolean read FLayoutQueued write SetLayoutQueued;
    property Position: TPosition read GetPosition write SetPosition default poDesigned;
    property Renderer: TFresnelLCLRenderer read FRenderer;
    property ShowInTaskBar: TShowInTaskbar read GetShowInTaskbar write SetShowInTaskBar
                                    default stDefault;
    property Visible: boolean read GetVisible write SetVisible stored VisibleIsStored default false;
    property WindowState: TWindowState read GetWindowState write SetWindowState
                                       default wsNormal;
    property FormBoundsRect: TRect read GetFormBoundsRect write SetFormBoundsRect;
    property FormLeft: integer read GetFormLeft write SetFormLeft;
    property FormTop: integer read GetFormTop write SetFormTop;
    property FormWidth: integer read GetFormWidth write SetFormWidth;
    property FormHeight: integer read GetFormHeight write SetFormHeight;
  end;

  { TFresnelForm }

  TFresnelForm = class(TCustomFresnelForm)
  published
    property AllowDropFiles;
    property AlphaBlend;
    property AlphaBlendValue;
    property BorderIcons;
    property BorderStyle;
    property Caption;
    property DefaultMonitor;
    property FormHeight;
    property FormLeft;
    property FormStyle;
    property FormTop;
    property FormWidth;
    property Icon;
    property Position;
    property ShowInTaskBar;
    property Stylesheet;
    property Visible;
    property WindowState;
  end;


function CompareFresnelLCLFont(Item1, Item2: Pointer): integer;
function CompareFresnelFontDescWithLCLFont(Key, Item: Pointer): integer;
procedure FresnelRectToRect(const Src: TFresnelRect; out Dest: TRect);

implementation

uses fresnel.lclevents;

function CompareFresnelLCLFont(Item1, Item2: Pointer): integer;
var
  Font1: TFresnelLCLFont absolute Item1;
  Font2: TFresnelLCLFont absolute Item2;
begin
  Result:=CompareText(Font1.Family,Font2.Family);
  if Result<>0 then exit;
  if Font1.Size<Font2.Size then
    exit(-1)
  else if Font1.Size>Font2.Size then
    exit(1);
  Result:=CompareText(Font1.Style,Font2.Style);
  if Result<>0 then exit;
  Result:=CompareText(Font1.Weight,Font2.Weight);
  if Result<>0 then exit;
  Result:=CompareText(Font1.Variant_,Font2.Variant_);
  if Result<>0 then exit;
  Result:=CompareText(Font1.Kerning,Font2.Kerning);
end;

function CompareFresnelFontDescWithLCLFont(Key, Item: Pointer): integer;
var
  Desc: PFresnelFontDesc absolute Key;
  aFont: TFresnelLCLFont absolute Item;
begin
  Result:=CompareText(Desc^.Family,aFont.Family);
  if Result<>0 then exit;
  if Desc^.Size<aFont.Size then
    exit(-1)
  else if Desc^.Size>aFont.Size then
    exit(1);
  Result:=CompareText(Desc^.Style,aFont.Style);
  if Result<>0 then exit;
  Result:=CompareText(Desc^.Weight,aFont.Weight);
  if Result<>0 then exit;
  Result:=CompareText(Desc^.Variant_,aFont.Variant_);
  if Result<>0 then exit;
  Result:=CompareText(Desc^.Kerning,aFont.Kerning);
end;

procedure FresnelRectToRect(const Src: TFresnelRect; out Dest: TRect);
begin
  Dest.Left:=floor(Src.Left);
  Dest.Top:=floor(Src.Top);
  Dest.Right:=ceil(Src.Right);
  Dest.Bottom:=ceil(Src.Bottom);
end;

{ TCustomFresnelForm }

procedure TCustomFresnelForm.DomChanged;
begin
  LayoutQueued:=true;
end;

procedure TCustomFresnelForm.Hide;
begin
  Form.Hide;
end;

procedure TCustomFresnelForm.Show;
begin
  Form.Show;
end;

function TCustomFresnelForm.ShowModal: TModalResult;
begin
  Result:=Form.ShowModal;
end;

procedure TCustomFresnelForm.OnQueuedLayout(Data: PtrInt);
begin
  try
    ApplyCSS;
    //Layouter.WriteLayoutTree;
    Layouter.Apply(Self);
    if Designer<>nil then
      Designer.InvalidateRect(Self,Bounds(0,0,ceil(Width),ceil(Height)),false)
    else
      Form.Invalidate;
  finally
    FLayoutQueued:=false;
  end;
end;

procedure TCustomFresnelForm.ProcessResource;
begin
  if not InitResourceComponent(Self, TFresnelForm) then
    raise EResNotFound.CreateFmt(rsFormResourceSNotFoundForResourcelessFormsCreateNew, [ClassName]);
end;

procedure TCustomFresnelForm.FormPaint(Sender: TObject);
begin
  if Designer<>nil then exit;
  Renderer.Draw(Self);
end;

procedure TCustomFresnelForm.SetLayoutQueued(const AValue: boolean);
begin
  if FLayoutQueued=AValue then Exit;
  if FClearing then exit;
  if csDestroyingHandle in Form.ControlState then exit;
  if csDestroying in ComponentState then exit;
  FLayoutQueued:=AValue;
  if FLayoutQueued then
    Application.QueueAsyncCall(@OnQueuedLayout,0);
end;

procedure TCustomFresnelForm.SetPosition(const AValue: TPosition);
begin
  Form.Position:=AValue;
end;

procedure TCustomFresnelForm.SetShowInTaskBar(const AValue: TShowInTaskbar);
begin
  Form.ShowInTaskBar:=AValue;
end;

procedure TCustomFresnelForm.SetVisible(const AValue: boolean);
begin
  if csDesigning in ComponentState then
    FVisible:=AValue
  else
    Form.Visible:=AValue;
end;

procedure TCustomFresnelForm.SetWindowState(const AValue: TWindowState);
begin
  Form.WindowState:=AValue;
end;

function TCustomFresnelForm.VisibleIsStored: Boolean;
begin
  Result:=Visible;
end;

function TCustomFresnelForm.GetHeight: TFresnelLength;
begin
  if Designer<>nil then
    Result:=Designer.GetDesignerClientHeight
  else
    Result:=Form.ClientHeight;
end;

function TCustomFresnelForm.GetWidth: TFresnelLength;
begin
  if Designer<>nil then
    Result:=Designer.GetDesignerClientWidth
  else
    Result:=Form.ClientWidth;
end;

function TCustomFresnelForm.GetAllowDropFiles: Boolean;
begin
  Result:=Form.AllowDropFiles;
end;

procedure TCustomFresnelForm.DoFormClick(Sender: TObject);

Var
  aInit : TFresnelMouseEventInit;
  aEl : TFresnelElement;
  evt : TFresnelMouseEvent;

begin
  InitMouseEvent(FForm,aInit);
  aEl:=GetElementAt(aInit.PagePos.X,aInit.PagePos.Y);
  if aEl=Nil then
    aEl:=Self;
  evt:=aEl.EventDispatcher.CreateEvent(aEl,evtClick) as TFresnelMouseEvent;
  try
    evt.initEvent(aInit);
    aEl.EventDispatcher.DispatchEvent(evt);
  finally
    evt.Free;
  end;
end;

procedure TCustomFresnelForm.OnFormResize(Sender: TObject);
begin
  inherited SetWidth(Form.ClientWidth);
  inherited SetHeight(Form.ClientHeight);
end;

function TCustomFresnelForm.GetAlphaBlend: Boolean;
begin
  Result:=Form.AlphaBlend;
end;

function TCustomFresnelForm.GetAlphaBlendValue: Byte;
begin
  Result:=Form.AlphaBlendValue;
end;

function TCustomFresnelForm.GetBorderIcons: TBorderIcons;
begin
  Result:=Form.BorderIcons;
end;

function TCustomFresnelForm.GetCaption: TCaption;
begin
  Result:=Form.Caption;
end;

function TCustomFresnelForm.GetDefaultMonitor: TDefaultMonitor;
begin
  Result:=Form.DefaultMonitor;
end;

function TCustomFresnelForm.GetEffectiveShowInTaskBar: TShowInTaskBar;
begin
  Result:=Form.EffectiveShowInTaskBar;
end;

function TCustomFresnelForm.GetFormBorderStyle: TFormBorderStyle;
begin
  Result:=Form.BorderStyle;
end;

function TCustomFresnelForm.GetFormBoundsRect: TRect;
begin
  Result:=Form.BoundsRect;
end;

function TCustomFresnelForm.GetFormHeight: integer;
begin
  Result:=Form.Height;
end;

function TCustomFresnelForm.GetFormLeft: integer;
begin
  Result:=Form.Left;
end;

function TCustomFresnelForm.GetFormState: TFormState;
begin
  Result:=Form.FormState;
end;

function TCustomFresnelForm.GetFormStyle: TFormStyle;
begin
  Result:=Form.FormStyle;
end;

function TCustomFresnelForm.GetFormTop: integer;
begin
  Result:=Form.Top;
end;

function TCustomFresnelForm.GetFormWidth: integer;
begin
  Result:=Form.Width;
end;

function TCustomFresnelForm.GetIcon: TIcon;
begin
  Result:=Form.Icon;
end;

function TCustomFresnelForm.GetPosition: TPosition;
begin
  Result:=Form.Position;
end;

function TCustomFresnelForm.GetShowInTaskbar: TShowInTaskbar;
begin
  Result:=Form.ShowInTaskBar;
end;

function TCustomFresnelForm.GetVisible: boolean;
begin
  if csDesigning in ComponentState then
    Result:=FVisible
  else
    Result:=Form.Visible;
end;

function TCustomFresnelForm.GetWindowState: TWindowState;
begin
  Result:=Form.WindowState;
end;

function TCustomFresnelForm.IsIconStored: Boolean;
begin
  Result:=Icon<>nil;
end;

procedure TCustomFresnelForm.SetAllowDropFiles(const AValue: Boolean);
begin
  Form.AllowDropFiles:=AValue;
end;

procedure TCustomFresnelForm.SetAlphaBlend(const AValue: Boolean);
begin
  Form.AlphaBlend:=AValue;
end;

procedure TCustomFresnelForm.SetAlphaBlendValue(const AValue: Byte);
begin
  Form.AlphaBlendValue:=AValue;
end;

procedure TCustomFresnelForm.SetBorderIcons(const AValue: TBorderIcons);
begin
  Form.BorderIcons:=AValue;
end;

procedure TCustomFresnelForm.SetCanvas(const AValue: TCanvas);
begin
  if FCanvas=AValue then Exit;
  FCanvas:=AValue;
  FontEngineLCL.Canvas:=FCanvas;
  Renderer.Canvas:=FCanvas;
end;

procedure TCustomFresnelForm.SetCaption(const AValue: TCaption);
begin
  Form.Caption:=AValue;
end;

procedure TCustomFresnelForm.SetDefaultMonitor(const AValue: TDefaultMonitor);
begin
  Form.DefaultMonitor:=AValue;
end;

procedure TCustomFresnelForm.SetFormBorderStyle(const AValue: TFormBorderStyle);
begin
  Form.BorderStyle:=AValue;
end;

procedure TCustomFresnelForm.SetFormBoundsRect(const AValue: TRect);
begin
  Form.BoundsRect:=AValue;
end;

procedure TCustomFresnelForm.SetFormHeight(const AValue: integer);
begin
  Form.Height:=AValue;
  if Designer<>nil then
    Designer.SetDesignerFormBounds(Self,Form.BoundsRect);
end;

procedure TCustomFresnelForm.SetFormLeft(const AValue: integer);
begin
  Form.Left:=AValue;
  if Designer<>nil then
    Designer.SetDesignerFormBounds(Self,Form.BoundsRect);
end;

procedure TCustomFresnelForm.SetFormStyle(const AValue: TFormStyle);
begin
  Form.FormStyle:=AValue;
end;

procedure TCustomFresnelForm.SetFormTop(const AValue: integer);
begin
  Form.Top:=AValue;
  if Designer<>nil then
    Designer.SetDesignerFormBounds(Self,Form.BoundsRect);
end;

procedure TCustomFresnelForm.SetFormWidth(const AValue: integer);
begin
  Form.Width:=AValue;
  if Designer<>nil then
    Designer.SetDesignerFormBounds(Self,Form.BoundsRect);
end;

procedure TCustomFresnelForm.SetIcon(const AValue: TIcon);
begin
  Form.Icon:=AValue;
end;

procedure TCustomFresnelForm.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if AComponent=FForm then
    begin
      FForm:=nil;
      FCanvas:=nil;
    end;
    if AComponent=FFontEngineLCL then
      FFontEngineLCL:=nil;
    if AComponent=FRenderer then
      FRenderer:=nil;
  end;
end;

procedure TCustomFresnelForm.SetName(const NewName: TComponentName);
begin
  inherited SetName(NewName);
  Form.Name:=NewName;
end;

procedure TCustomFresnelForm.SetHeight(AValue: TFresnelLength);
begin
  raise Exception.Create('TCustomFresnelForm.SetHeight is a derived value, set FormHeight instead');
end;

procedure TCustomFresnelForm.SetWidth(AValue: TFresnelLength);
begin
  raise Exception.Create('TCustomFresnelForm.SetWidth is a derived value, set FormWidth instead');
end;

constructor TCustomFresnelForm.Create(AOwner: TComponent);
begin
  GlobalNameSpace.BeginWrite;
  try
    CreateNew(AOwner);
    if (ClassType <> TFresnelForm) and not (csDesigning in ComponentState) then
    begin
      ProcessResource;
    end;
  finally
    GlobalNameSpace.EndWrite;
  end;
end;

constructor TCustomFresnelForm.CreateNew(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FForm:=TCustomForm.CreateNew(nil);
  FForm.Name:=Name;
  FForm.Visible:=false;
  FForm.OnResize:=@OnFormResize;
  FForm.OnClick:=@DoFormClick;
  FCanvas:=Form.Canvas;
  Form.OnPaint:=@FormPaint;

  FFontEngineLCL:=TFresnelLCLFontEngine.Create(nil);
  FontEngine:=FontEngineLCL;
  FontEngineLCL.Canvas:=Canvas;

  FLayouter:=TSimpleFresnelLayouter.Create(nil);
  Layouter.Viewport:=Self;

  FRenderer:=TFresnelLCLRenderer.Create(nil);
  FRenderer.Canvas:=Canvas;
end;

destructor TCustomFresnelForm.Destroy;
begin
  FClearing:=true;
  FreeAndNil(FRenderer);
  FreeAndNil(FLayouter);
  FontEngine:=nil;
  FreeAndNil(FFontEngineLCL);
  FCanvas:=nil;
  FreeAndNil(FForm);
  Application.RemoveAllHandlersOfObject(Self);
  inherited Destroy;
end;

{ TFresnelLCLRenderer }

procedure TFresnelLCLRenderer.FillRect(const aColor: TFPColor;
  const aRect: TFresnelRect);
begin
  Canvas.Brush.FPColor:=aColor;
  Canvas.Brush.Style:=bsSolid;
  Canvas.FillRect(Rect(floor(FOrigin.X+aRect.Left),floor(FOrigin.Y+aRect.Top),
                       ceil(FOrigin.X+aRect.Right),ceil(FOrigin.Y+aRect.Bottom)));
end;

procedure TFresnelLCLRenderer.Line(const aColor: TFPColor; const x1, y1, x2,
  y2: TFresnelLength);
begin
  Canvas.Pen.FPColor:=aColor;
  Canvas.Pen.Style:=psSolid;
  Canvas.Line(round(FOrigin.X+x1),round(FOrigin.Y+y1),round(FOrigin.X+x2),round(FOrigin.Y+y2));
end;

procedure TFresnelLCLRenderer.TextOut(const aLeft, aTop: TFresnelLength;
  const aFont: IFresnelFont; const aColor: TFPColor; const aText: string
  );
var
  ts: TTextStyle;
  aFresnelFont: TFresnelLCLFont;
begin
  aFresnelFont:=aFont.GetTool as TFresnelLCLFont;
  Canvas.Font:=aFresnelFont.LCLFont;
  Canvas.Font.FPColor:=aColor;
  ts:=Canvas.TextStyle;
  ts.Opaque:=false;
  Canvas.TextStyle:=ts;
  Canvas.TextOut(round(FOrigin.X+aLeft),round(FOrigin.Y+aTop),aText);
end;

constructor TFresnelLCLRenderer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

{ TOldFresnelCustomForm }

function TOldFresnelCustomForm.GetStylesheet: TStrings;
begin
  Result:=Viewport.Stylesheet;
end;

procedure TOldFresnelCustomForm.OnDomChanged(Sender: TObject);
begin
  LayoutQueued:=true;
end;

procedure TOldFresnelCustomForm.OnQueuedLayout(Data: PtrInt);
begin
  ViewPort.ApplyCSS;
  //Layouter.WriteLayoutTree;
  Layouter.Apply(ViewPort);
  Invalidate;
end;

procedure TOldFresnelCustomForm.SetLayoutQueued(const AValue: boolean);
begin
  if FLayoutQueued=AValue then Exit;
  if FClearing then exit;
  if csDestroyingHandle in ControlState then exit;
  if csDestroying in ComponentState then exit;
  FLayoutQueued:=AValue;
  if FLayoutQueued then
    Application.QueueAsyncCall(@OnQueuedLayout,0);
end;

procedure TOldFresnelCustomForm.SetStylesheet(const AValue: TStrings);
begin
  Viewport.Stylesheet:=AValue;
end;

procedure TOldFresnelCustomForm.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if AComponent=FViewport then
      FViewport:=nil;
    if AComponent=FFontEngine then
      FFontEngine:=nil;
    if AComponent=FLayouter then
    begin
      FLayouter:=nil;
      if FViewport<>nil then
        FViewport.Layouter:=nil;
    end;
    if AComponent=FRenderer then
      FRenderer:=nil;
  end;
end;

procedure TOldFresnelCustomForm.Paint;
begin
  inherited Paint;
  Renderer.Draw(Viewport);
end;

function TOldFresnelCustomForm.GetViewport: TFresnelViewport;
begin
  Result:=FViewport;
end;

constructor TOldFresnelCustomForm.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FViewport:=TFresnelViewport.Create(nil);
  FViewport.Name:='Viewport';
  FViewport.OnDomChanged:=@OnDomChanged;
  FFontEngine:=TFresnelLCLFontEngine.Create(nil);
  FViewport.FontEngine:=FontEngine;
  FontEngine.Canvas:=Canvas;
  FLayouter:=TSimpleFresnelLayouter.Create(nil);
  Layouter.Viewport:=ViewPort;
  FRenderer:=TFresnelLCLRenderer.Create(nil);
  FRenderer.Canvas:=Canvas;
end;

destructor TOldFresnelCustomForm.Destroy;
begin
  FClearing:=true;
  FreeAndNil(FRenderer);
  FreeAndNil(FLayouter);
  FreeAndNil(FViewport);
  FreeAndNil(FFontEngine);
  Application.RemoveAllHandlersOfObject(Self);
  inherited Destroy;
end;

procedure TOldFresnelCustomForm.GetChildren(Proc: TGetChildProc; Root: TComponent);
var
  i: Integer;
  OwnedComponent: TComponent;
  ChildEl: TFresnelElement;
begin
  if Root=Self then
  begin
    // first all children of Viewport
    for i:=0 to Viewport.NodeCount-1 do
    begin
      ChildEl:=Viewport.Nodes[i];
      if ChildEl.Owner=Root then
        Proc(ChildEl);
    end;
    // then all non visual components
    for i := 0 to ComponentCount - 1 do
    begin
      OwnedComponent := Components[i];
      if not OwnedComponent.HasParent then
        Proc(OwnedComponent);
    end;
  end;
end;

{ TFresnelLCLControl }

procedure TFresnelLCLControl.SetLayoutQueued(const AValue: boolean);
begin
  if FLayoutQueued=AValue then Exit;
  if FClearing then exit;
  if csDestroyingHandle in ControlState then exit;
  if csDestroying in ComponentState then exit;
  FLayoutQueued:=AValue;
  if FLayoutQueued then
    Application.QueueAsyncCall(@OnQueuedLayout,0);
end;

procedure TFresnelLCLControl.OnDomChanged(Sender: TObject);
begin
  LayoutQueued:=true;
end;

procedure TFresnelLCLControl.HandleClick(Sender: TObject);
Var
  aInit : TFresnelMouseEventInit;
  aEl : TFresnelElement;
  evt : TFresnelMouseEvent;

begin
  InitMouseEvent(Self,aInit);
  aEl:=FViewport.GetElementAt(aInit.PagePos.X,aInit.PagePos.Y);
  if aEl=Nil then
    aEl:=Self.Viewport;
  evt:=aEl.EventDispatcher.CreateEvent(aEl,evtClick) as TFresnelMouseEvent;
  try
    evt.initEvent(aInit);
    aEl.EventDispatcher.DispatchEvent(evt);
  finally
    evt.Free;
  end;
end;

procedure TFresnelLCLControl.OnQueuedLayout(Data: PtrInt);
begin
  ViewPort.ApplyCSS;
  //Layouter.WriteLayoutTree;
  Layouter.Apply(ViewPort);
  Invalidate;
end;

procedure TFresnelLCLControl.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if AComponent=FViewport then
      FViewport:=nil;
    if AComponent=FLayouter then
    begin
      FLayouter:=nil;
      if FViewport<>nil then
        FViewport.Layouter:=nil;
    end;
  end;
end;

procedure TFresnelLCLControl.Paint;
begin
  inherited Paint;

  Renderer.Draw(Viewport);
end;

constructor TFresnelLCLControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FViewport:=TFresnelViewport.Create(nil);
  FViewport.OnDomChanged:=@OnDomChanged;
  FFontEngine:=TFresnelLCLFontEngine.Create(nil);
  FViewport.FontEngine:=FontEngine;
  FontEngine.Canvas:=Canvas;
  FViewport.Name:='Viewport';
  FLayouter:=TSimpleFresnelLayouter.Create(nil);
  Layouter.Viewport:=ViewPort;
  FRenderer:=TFresnelLCLRenderer.Create(nil);
  FRenderer.Canvas:=Canvas;
  OnClick:=@HandleClick;
end;

destructor TFresnelLCLControl.Destroy;
begin
  FClearing:=true;
  FreeAndNil(FRenderer);
  FreeAndNil(FLayouter);
  FreeAndNil(FViewport);
  FreeAndNil(FFontEngine);
  inherited Destroy;
end;

{ TFresnelLCLFontEngine }

constructor TFresnelLCLFontEngine.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFonts:=TAvgLvlTree.Create(@CompareFresnelLCLFont);
end;

destructor TFresnelLCLFontEngine.Destroy;
var
  Node: TAVLTreeNode;
  aFont: TFresnelLCLFont;
begin
  Node:=FFonts.Root;
  while Node<>nil do
  begin
    aFont:=TFresnelLCLFont(Node.Data);
    Node.Data:=nil;
    aFont._Release;
    Node:=Node.Successor;
  end;
  FreeAndNil(FFonts);
  inherited Destroy;
end;

function TFresnelLCLFontEngine.FindFont(const Desc: TFresnelFontDesc
  ): TFresnelLCLFont;
var
  Node: TAVLTreeNode;
begin
  Node:=FFonts.FindKey(@Desc,@CompareFresnelFontDescWithLCLFont);
  if Node=nil then
    Result:=nil
  else
    Result:=TFresnelLCLFont(Node.Data);
end;

function TFresnelLCLFontEngine.Allocate(const Desc: TFresnelFontDesc
  ): IFresnelFont;
var
  aFont: TFresnelLCLFont;
begin
  aFont:=FindFont(Desc);
  if aFont<>nil then
    exit(aFont);
  aFont:=TFresnelLCLFont.Create;
  aFont.Engine:=Self;
  aFont._AddRef;
  aFont.Family:=Desc.Family;
  aFont.Kerning:=Desc.Kerning;
  aFont.Size:=Desc.Size;
  aFont.Style:=Desc.Style;
  aFont.Variant_:=Desc.Variant_;
  aFont.Weight:=Desc.Weight;
  FFonts.Add(aFont);
  Result:=aFont;
end;

function TFresnelLCLFontEngine.TextSize(aFont: TFresnelLCLFont;
  const aText: string): TPoint;
var
  aSize: TSize;
begin
  Canvas.Font:=NeedLCLFont(aFont);
  aSize:=Canvas.TextExtent(aText);
  Result.X:=aSize.cx;
  Result.Y:=aSize.cy;
end;

function TFresnelLCLFontEngine.TextSizeMaxWidth(aFont: TFresnelLCLFont;
  const aText: string; MaxWidth: integer): TPoint;
var
  aSize: TSize;
begin
  Canvas.Font:=NeedLCLFont(aFont);

  if LCLIntf.GetTextExtentExPoint(Canvas.Handle, PChar(aText), Length(aText),
    MaxWidth, nil, nil, aSize) then
  begin
    Result.X:=aSize.cx;
    Result.Y:=aSize.cy;
  end else begin
    Result.X:=0;
    Result.Y:=0;
  end;
end;

function TFresnelLCLFontEngine.NeedLCLFont(aFont: TFresnelLCLFont): TFont;
var
  aLCLFont: TFont;
  v: integer;
  aStyle: TFontStyles;
begin
  if aFont.LCLFont=nil then
  begin
    aLCLFont:=TFont.Create;
    aFont.LCLFont:=aLCLFont;
    case aFont.Size of
    'xx-small': aLCLFont.Size:=6;
    'x-small': aLCLFont.Size:=7;
    'smaller': aLCLFont.Size:=8;
    'small': aLCLFont.Size:=8;
    'medium': aLCLFont.Size:=10;
    'large': aLCLFont.Size:=13;
    'larger': aLCLFont.Size:=15;
    'x-large': aLCLFont.Size:=20;
    'xx-large': aLCLFont.Size:=30;
    else
      if TryStrToInt(aFont.Size,v) then
      begin
        if v<4 then v:=4;
        aLCLFont.Size:=v;
      end;
    end;
    aStyle:=[];
    case aFont.Weight of
    'bold',
    'bolder',
    '500',
    '600',
    '700': Include(aStyle,fsBold);
    end;
    case aFont.Style of
    'italic': Include(aStyle,fsItalic);
    end;
    aLCLFont.Style:=aStyle;
  end;
  Result:=aFont.LCLFont;
end;

{ TFresnelLCLFont }

destructor TFresnelLCLFont.Destroy;
begin
  FreeAndNil(LCLFont);
  inherited Destroy;
end;

function TFresnelLCLFont.GetFamily: string;
begin
  Result:=Family;
end;

function TFresnelLCLFont.GetKerning: string;
begin
  Result:=Kerning;
end;

function TFresnelLCLFont.GetSize: string;
begin
  Result:=Size;
end;

function TFresnelLCLFont.GetStyle: string;
begin
  Result:=Style;
end;

function TFresnelLCLFont.GetVariant: string;
begin
  Result:=Variant_;
end;

function TFresnelLCLFont.GetWeight: string;
begin
  Result:=Weight;
end;

function TFresnelLCLFont.TextSize(const aText: string): TFresnelPoint;
var
  p: TPoint;
begin
  p:=Engine.TextSize(Self,aText);
  Result.X:=p.X;
  Result.Y:=p.Y;
end;

function TFresnelLCLFont.TextSizeMaxWidth(const aText: string;
  MaxWidth: TFresnelLength): TFresnelPoint;
var
  p: TPoint;
begin
  p:=Engine.TextSizeMaxWidth(Self,aText,Floor(Max(1,MaxWidth)));
  Result.X:=p.X;
  Result.Y:=p.Y;
end;

function TFresnelLCLFont.GetTool: TObject;
begin
  Result:=Self;
end;

end.

