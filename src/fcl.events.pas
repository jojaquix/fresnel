unit fcl.events;

{$mode ObjFPC}
{$H+}
{$modeswitch functionreferences}

interface

uses
  Classes, SysUtils, Contnrs;

Type
  TEventID = Word;
  TEventName = UTF8String;

  EEvents = Class(Exception);

  { TAbstractEvent }

  TAbstractEvent = Class(TObject)
  private
    FSender: TObject;
    FEventID : TEventID;
  public
    Constructor Create(aSender : TObject; aID : TEventID);
    // Event name. Mandatory. Will be lowercased.
    Class Function EventName : TEventName; virtual; abstract;
    // Register the event class type. Returns the event ID.
    Class Function Register : TEventID; virtual;
    // Sender of the event
    Property Sender : TObject Read FSender Write FSender;
    // Event ID used for create
    Property EventID : TEventID Read FEventID;
  end;
  TAbstractEventClass = Class of TAbstractEvent;

  TEventHandler = Procedure(Event : TAbstractEvent) of object;
  TEventCallBack = Procedure(Event : TAbstractEvent);
  TEventHandlerRef = Reference to Procedure(Event : TAbstractEvent);

  { TEventDef }

  TEventDef = Class(TObject)
  private
    FClass: TAbstractEventClass;
    FID: TEventID;
  Public
    Constructor Create(aID : TEventID; aClass : TAbstractEventClass);
    Property ID : TEventID Read FID;
    Property EventClass : TAbstractEventClass Read FClass;
  end;
  TEventDefArray = Array of TEventDef;

  { TEventRegistry }

  // This class basically maps event names/ids to classes.

  TEventRegistry = class(TObject)
  private
    FIDOffset: TEventID;
    FEventDefs : Array of TEventDef;
    FHash : TFPObjectHashTable;
    FNextID : TEventID;
    class var _Instance: TEventRegistry;
    procedure SetCapacity(aCapacity: TEVentID);
  Protected
    Function GetNextID : TEventID;
    Class Function DefaultIDOffset : TEventID;  virtual;
    Class Procedure SetInstance(aInstance : TEventRegistry); static;
    Function DoRegisterEvent(aID : TEventID; aClass : TAbstractEventClass) : TEventDef;
    // Registered IDS "Custom" IDs are registered starting at IDOffset;
    Property IDOffset : TEventID Read FIDOffset;
  Public
    Class constructor init;
    Class destructor done;
    Constructor Create; virtual;
    Destructor Destroy; override;
    Procedure Clear;
    FUnction GetRegisteredEventCount : Integer;
    Function RegisterEventWithID(aID : TEventID; aClass : TAbstractEventClass) : TEventID;
    Function RegisterEvent(aClass : TAbstractEventClass) : TEventID;
    // Returns Nil if not found
    function FindEventClass(aEventID: TEventID): TAbstractEventClass; overload;
    // Raises exception if ID not valie
    Function GetEventClass(aEventID : TEventID) : TAbstractEventClass; overload;
    // Returns Nil if not found
    function FindEventClass(aEventName: TEventName): TAbstractEventClass; overload;
    // Raises exception if not found.
    Function GetEventClass(aEventName : TEventName) : TAbstractEventClass; overload;
    // Returns zero if not found.
    function FindEventID(aEventName: TEventName): TEventID;
    // Returns empty if not found.
    function FindEventName(aEventID : TEventID): TEventName;
    // Raises exception if not found.
    Function GetEventID(aEventName : TEventName) : TEventID;
    // Raises exception if not found.
    function GetEventName(aEventID : TEventID): TEventName;
    // Unregister event based on class, event ID or event name.
    Procedure UnRegisterEvent(aClass : TAbstractEventClass);
    Procedure UnRegisterEvent(aEventID : TEventID);
    Procedure UnRegisterEvent(aEventName: TEventName);
    Class property Instance : TEventRegistry Read _Instance Write SetInstance;
  end;


  { TEventHandlerItem }
  // One item to keep an event handler

  TEventHandlerItem = Class(TCollectionItem)
  private
    FDeleteScheduled: Boolean;
    FEventID: TEventID;
    FEventName: TEventName;
    function GetEventName: TEventName;
  Protected
    Procedure CallHandler(aEvent : TAbstractEvent); virtual; abstract;
    function Match(aEVent : TAbstractEvent) : Boolean; virtual;
    function MatchHandler(aHandler : TEventHandler; aEventID: TEventID) : Boolean; virtual;
    function MatchHandler(aHandler : TEventHandlerRef; aEventID: TEventID) : Boolean; virtual;
    function MatchHandler(aHandler : TEventCallBack; aEventID: TEventID) : Boolean; virtual;
    Procedure ScheduleDelete; virtual;
  Public
    Property DeleteScheduled : Boolean Read FDeleteScheduled;
    Property EventName : TEventName Read GetEventName Write FEventName;
    Property EventID : TEventID Read FEventID Write FEventID;
  end;


  { TEventHandlerList }

  // A helper list class for TEventDispatcher. It manages a list of event handlers.

  TEventContinueEvent = Procedure (aEvent: TAbstractEvent; var aContinue : Boolean) of object;
  TEventHandlerList = Class(TOwnedCollection)
  private
    FCalling : Integer;
    FOnContinueEvent: TEventContinueEvent;
    function GetH(aIndex : Integer): TEventHandlerItem;
    procedure SetH(aIndex : Integer; AValue: TEventHandlerItem);
  Protected
    Procedure BeginTraverse;
    Procedure EndTraverse;
    Function GetEventName(aID : TEventID) : TEVentName;
    Function FindHandler(aHandler : TEventHandler; aEventID : TEventID) : TEventHandlerItem;
    Function FindHandler(aHandler : TEventCallBack; aEventID : TEventID) : TEventHandlerItem;
    Function FindHandler(aHandler : TEventHandlerRef; aEventID : TEventID) : TEventHandlerItem;
    Function ContinueEvent(aEvent : TAbstractEvent) : Boolean; virtual;
    Procedure DeleteScheduled;
  Public
    Procedure UnregisterHandler(aHandler : TEventHandlerItem);
    Function CallAllHandlers(aEvent : TAbstractEvent) : Integer;
    Property Handlers[aIndex : Integer] : TEventHandlerItem Read GetH Write SetH; default;
    Property OnContinueEvent : TEventContinueEvent Read FOnContinueEvent Write FOnContinueEvent;
  end;


  { TObjectEventHandlerItem }

  TObjectEventHandlerItem = Class(TEventHandlerItem)
  Private
    FEventHandler : TEventHandler;
  Protected
    function MatchHandler(aHandler : TEventHandler; aEventID : TEventID) : Boolean; override;
    Procedure CallHandler(aEvent : TAbstractEvent); override;
  Public
    Property EventHandler : TEventHandler Read FEventHandler Write FEventHandler;
  end;

  { TCallBackEventHandlerItem }

  TCallBackEventHandlerItem = Class(TEventHandlerItem)
  Private
    FEventHandler : TEventCallback;
  Protected
    function MatchHandler(aHandler : TEventCallBack; aEventID : TEventID) : Boolean; override;
    Procedure CallHandler(aEvent : TAbstractEvent); override;
  Public
    Property EventHandler : TEventCallback Read FEventHandler;
  end;

  { TReferenceEventHandlerItem }

  TReferenceEventHandlerItem = Class(TEventHandlerItem)
  Private
    FEventHandler : TEventHandlerRef;
  Protected
    function MatchHandler(aHandler :TEventHandlerRef; aEventID : TEventID) : Boolean; override;
    Procedure CallHandler(aEvent : TAbstractEvent); override;
  Public
     Property EventHandler : TEventHandlerRef Read FEventHandler;
  end;


  { TEventDispatcher }

  TEventSetupHandler = Procedure(Event : TAbstractEvent) of object;
  TEventSetupCallBack = Procedure(Event : TAbstractEvent);
  TEventSetupHandlerRef = Reference to Procedure(Event : TAbstractEvent);

  // This class can be used by any other class to send events on its behalf.

  TEventDispatcher = class(TPersistent)
  Private
    FDefaultSender: TObject;
    FHandlerList : TEventHandlerList;
    FRegistry: TEventRegistry;
    function GetCount: Integer;
  Protected
    Class function GlobalRegistry : TEventRegistry; virtual;
    function GetRegistry : TEventRegistry; virtual;
    Function GetEventName(aID : TEventID) : TEventName;
    Function CreateHandlerList : TEventHandlerList; virtual;
    Function CreateHandlerItem(aHandler : TEventHandler) : TEventHandlerItem; virtual;
    Function CreateHandlerItem(aHandler : TEventCallBack) : TEventHandlerItem; virtual;
    Function CreateHandlerItem(aHandler : TEventHandlerRef) : TEventHandlerItem; virtual;
    Function DoRegisterHandler(aHandler : TEventHandler; aEventID : TEventID) : TEventHandlerItem; virtual;
    Function DoRegisterHandler(aHandler : TEventCallBack; aEventID : TEventID) : TEventHandlerItem; virtual;
    Function DoRegisterHandler(aHandler : TEventHandlerRef; aEventID : TEventID) : TEventHandlerItem; virtual;
  Public
    Constructor Create(aDefaultSender : TObject); virtual;
    Destructor Destroy; override;
    // Various forms to register an event handler
    Function RegisterHandler(aHandler : TEventHandler; aEventID : TEventID) : TEventHandlerItem;
    Function RegisterHandler(aHandler : TEventCallBack; aEventID : TEventID) : TEventHandlerItem;
    Function RegisterHandler(aHandler : TEventHandlerRef; aEventID : TEventID) : TEventHandlerItem;
    Function RegisterHandler(aHandler : TEventCallback; aEventName : TEventName) : TEventHandlerItem;
    Function RegisterHandler(aHandler : TEventHandler; aEventName : TEventName) : TEventHandlerItem;
    Function RegisterHandler(aHandler : TEventHandlerRef; aEventName : TEventName) : TEventHandlerItem;
    // Remove all event handlers for a given ID/name.
    Procedure UnregisterHandler(aEventID : TEventID);
    Procedure UnregisterHandler(aEventName : TEventName);
    Procedure UnregisterHandler(aItemID : Integer);
    // Remove all event handlers for a given callback
    Procedure UnRegisterHandler(aHandler : TEventHandler);
    Procedure UnRegisterHandler(aHandler : TEventCallBack);
    Procedure UnRegisterHandler(aHandler : TEventHandlerRef);
    // Return single handler using it's ID
    Procedure UnregisterHandler(aItem : TEventHandlerItem);
    // Remove single handler using combination of handler/event ID
    Procedure UnRegisterHandler(aHandler : TEventHandler; aEventID : TEventID);
    Procedure UnRegisterHandler(aHandler : TEventCallBack; aEventID : TEventID);
    Procedure UnRegisterHandler(aHandler : TEventHandlerRef; aEventID : TEventID);
    // Remove single handler using combination of handler/event name
    Procedure UnRegisterHandler(aHandler : TEventCallback; aEventName : TEventName);
    Procedure UnRegisterHandler(aHandler : TEventHandler; aEventName : TEventName);
    Procedure UnRegisterHandler(aHandler : TEventHandlerRef; aEventName : TEventName);
    // Create an event
    Function CreateEvent(aSender: TObject; aEventID : TEventID) : TAbstractEvent;
    Function CreateEvent(aSender: TObject; aEventName : TEventName) : TAbstractEvent;
    // Dispatch an event.
    // Calls the registered handlers for that event, in the ordeer they were registered.
    // Returns the number of handlers that were called;
    Function DispatchEvent(aEvent : TAbstractEvent) : Integer;
    // Using ID
    Function DispatchEvent(aEventID : TEventID) : Integer;
    Function DispatchEvent(aEventID : TEventID; aSender : TObject) : Integer;
    Function DispatchEvent(aEventID : TEventID; aSender : TObject; aOnSetup : TEventSetupHandler) : Integer;
    Function DispatchEvent(aEventID : TEventID; aSender : TObject; aOnSetup : TEventSetupCallBack) : Integer;
    Function DispatchEvent(aEventID : TEventID; aSender : TObject; aOnSetup : TEventSetupHandlerRef) : Integer;
    Function DispatchEvent(aEventID : TEventID; aOnSetup : TEventSetupHandler) : Integer;
    Function DispatchEvent(aEventID : TEventID; aOnSetup : TEventSetupCallBack) : Integer;
    Function DispatchEvent(aEventID : TEventID; aOnSetup : TEventSetupHandlerRef) : Integer;
    Function DispatchEvent(aEventName : TEventName) : Integer;
    Function DispatchEvent(aEventName : TEventName; aSender : TObject) : Integer;
    Function DispatchEvent(aEventName : TEventName; aSender : TObject; aOnSetup : TEventSetupHandler) : Integer;
    Function DispatchEvent(aEventName : TEventName; aSender : TObject; aOnSetup : TEventSetupCallBack) : Integer;
    Function DispatchEvent(aEventName : TEventName; aSender : TObject; aOnSetup : TEventSetupHandlerRef) : Integer;
    Function DispatchEvent(aEventName : TEventName; aOnSetup : TEventSetupHandler) : Integer;
    Function DispatchEvent(aEventName : TEventName; aOnSetup : TEventSetupCallBack) : Integer;
    Function DispatchEvent(aEventName : TEventName; aOnSetup : TEventSetupHandlerRef) : Integer;
    Property DefaultSender : TObject Read FDefaultSender;
    Property Count : Integer Read GetCount;
    Property Registry : TEventRegistry Read GetRegistry Write FRegistry;
  end;


implementation

{ TEventHandlerList }

function TEventHandlerList.GetH(aIndex : Integer): TEventHandlerItem;
begin
  Result:=TEventHandlerItem(Items[aIndex]);
end;

procedure TEventHandlerList.SetH(aIndex: Integer; AValue: TEventHandlerItem);
begin
  Items[aIndex]:=aValue;
end;

procedure TEventHandlerList.BeginTraverse;
begin
  InterlockedIncrement(FCalling);
end;

procedure TEventHandlerList.EndTraverse;
begin
  InterlockedDecrement(FCalling);
  If FCalling<=0 then
    DeleteScheduled;
end;

function TEventHandlerList.GetEventName(aID: TEventID): TEVentName;
begin
  Result:='';
  If Assigned(Owner) and (Owner is TEventDispatcher) then
    Result:=TEventDispatcher(Owner).GetEventName(aID);
end;

function TEventHandlerList.FindHandler(aHandler: TEventHandler;
  aEventID: TEventID): TEventHandlerItem;

Var
  I : Integer;

begin
  Result:=Nil;
  BeginTraverse;
  try
    I:=Count-1;
    While (I>=0) and Not GetH(i).MatchHandler(aHandler,aEventID) do
      Dec(I);
    if I>=0 then
      Result:=GetH(i);
  finally
    EndTraverse;
  end;
end;

function TEventHandlerList.FindHandler(aHandler: TEventCallBack;
  aEventID: TEventID): TEventHandlerItem;
Var
  I : Integer;

begin
  Result:=Nil;
  BeginTraverse;
  try
    I:=Count-1;
    While (I>=0) and Not GetH(i).MatchHandler(aHandler,aEventID) do
      Dec(I);
    if I>=0 then
      Result:=GetH(i);
  finally
    EndTraverse;
  end;
end;

function TEventHandlerList.FindHandler(aHandler: TEventHandlerRef;
  aEventID: TEventID): TEventHandlerItem;
Var
  I : Integer;

begin
  Result:=Nil;
  BeginTraverse;
  try
    I:=Count-1;
    While (I>=0) and Not GetH(i).MatchHandler(aHandler,aEventID) do
      Dec(I);
    if I>=0 then
      Result:=GetH(i);
  finally
    EndTraverse;
  end;
end;

function TEventHandlerList.ContinueEvent(aEvent: TAbstractEvent): Boolean;
begin
  Result:=True;
  If Assigned(OnContinueEvent) then
    OnContinueEvent(aEvent,Result);
end;

procedure TEventHandlerList.DeleteScheduled;

Var
  I : Integer;

begin
  InterlockedIncrement(FCalling);
  try
    I:=Count-1;
    While (I>=0) do
      begin
      if GetH(I).DeleteScheduled then
        Delete(I);
      Dec(I);
      end;
  finally
    InterlockedDecrement(FCalling);
  end;
end;

procedure TEventHandlerList.UnregisterHandler(aHandler: TEventHandlerItem);
begin
  if FCalling>0 then
    aHandler.ScheduleDelete
  else
    aHandler.Free;
end;

function TEventHandlerList.CallAllHandlers(aEvent: TAbstractEvent): Integer;

Var
  I : Integer;
  H : TEventHandlerItem;

begin
  Result:=0;
  InterlockedIncrement(FCalling);
  try
    I:=0;
    While I<Count do
      begin
      H:=GetH(I);
      if Not (H.DeleteScheduled) and H.Match(aEvent) then
        begin
        H.CallHandler(aEvent);
        Inc(Result);
        if Not ContinueEvent(aEvent) then
          I:=Count;
        end;
      Inc(I);
      end;
  finally
    InterlockedDecrement(FCalling);
    If FCalling<=0 then
      DeleteScheduled;
  end;

end;

{ TEeventHandlerItem }

function TEventHandlerItem.GetEventName: TEventName;

begin
  Result:=FEventName;
  if (Result='') and Assigned(Collection) and (Collection is TEventHandlerList) then
    Result:=TEventHandlerList(Collection).GetEventName(EventID);
  if Result='' then
    Result:=IntToStr(EventID);
end;

function TEventHandlerItem.Match(aEVent: TAbstractEvent): Boolean;
begin
  Result:=(aEvent<>Nil) and (aEvent.EventID=Self.FEVentID);
end;

function TEventHandlerItem.MatchHandler(aHandler: TEventHandler;
  aEventID: TEventID): Boolean;
begin
  Result:=(FEventID=aEventID) and (aHandler=Nil);
end;

function TEventHandlerItem.MatchHandler(aHandler: TEventHandlerRef;
  aEventID: TEventID): Boolean;
begin
  Result:=(FEventID=aEventID) and (aHandler=Nil);
end;

function TEventHandlerItem.MatchHandler(aHandler: TEventCallBack;
  aEventID: TEventID): Boolean;
begin
  Result:=(FEventID=aEventID) and (aHandler=Nil);
end;

procedure TEventHandlerItem.ScheduleDelete;
begin
  FDeleteScheduled:=True;
end;



{ TReferenceEventHandlerItem }

function TReferenceEventHandlerItem.MatchHandler(aHandler: TEventHandlerRef;
  aEventID: TEventID): Boolean;
begin
  Result:=(aHandler=FEventHandler) and (aEventID=FEventID);
end;

procedure TReferenceEventHandlerItem.CallHandler(aEvent: TAbstractEvent);
begin
  FEventHandler(aEvent);
end;


{ TCallBackEventHandlerItem }

function TCallBackEventHandlerItem.MatchHandler(aHandler: TEventCallBack;
  aEventID: TEventID): Boolean;
begin
  Result:=(aHandler=FEventHandler) and (aEventID=FEventID);
end;

procedure TCallBackEventHandlerItem.CallHandler(aEvent: TAbstractEvent);
begin
  FEventHandler(aEvent);
end;

{ TObjectEventHandlerItem }

function TObjectEventHandlerItem.MatchHandler(aHandler: TEventHandler;
  aEventID: TEventID): Boolean;
begin
  Result:=(aHandler=FEventHandler) and (aEventID=FEventID);
end;

procedure TObjectEventHandlerItem.CallHandler(aEvent: TAbstractEvent);
begin
  FEventHandler(aEvent);
end;

{ TEventDispatcher }

function TEventDispatcher.GetCount: Integer;
begin
  Result:=FHandlerList.Count;
end;

class function TEventDispatcher.GlobalRegistry: TEventRegistry;
begin
  Result:=TEventRegistry.Instance;
end;

function TEventDispatcher.GetRegistry: TEventRegistry;
begin
  if Assigned(FRegistry) then
    Result:=FRegistry
  else
    Result:=GlobalRegistry;
end;

function TEventDispatcher.GetEventName(aID: TEventID): TEventName;
begin
  Result:=Registry.FindEventName(aID);
end;

function TEventDispatcher.CreateHandlerList: TEventHandlerList;
begin
  Result:=TEventHandlerList.Create(Self,TEventHandlerItem);
end;

function TEventDispatcher.CreateHandlerItem(aHandler: TEventHandler
  ): TEventHandlerItem;
begin
  Result:=TObjectEventHandlerItem.Create(FHandlerList);
  TObjectEventHandlerItem(Result).FEventHandler:=aHandler;
end;

function TEventDispatcher.CreateHandlerItem(aHandler: TEventCallBack
  ): TEventHandlerItem;
begin
  Result:=TCallBackEventHandlerItem.Create(FHandlerList);
  TCallBackEventHandlerItem(Result).FEventHandler:=aHandler;
end;

function TEventDispatcher.CreateHandlerItem(aHandler: TEventHandlerRef
  ): TEventHandlerItem;
begin
  Result:=TReferenceEventHandlerItem.Create(FHandlerList);
  TReferenceEventHandlerItem(Result).FEventHandler:=aHandler;
end;

function TEventDispatcher.DoRegisterHandler(aHandler: TEventHandler;
  aEventID: TEventID): TEventHandlerItem;
begin
  Result:=CreateHandlerItem(aHandler);
  Result.FEventID:=aEventID;
end;


function TEventDispatcher.DoRegisterHandler(aHandler: TEventCallBack;
  aEventID: TEventID): TEventHandlerItem;
begin
  Result:=CreateHandlerItem(aHandler);
  Result.FEventID:=aEventID;
end;

function TEventDispatcher.DoRegisterHandler(aHandler: TEventHandlerRef;
  aEventID: TEventID): TEventHandlerItem;
begin
  Result:=CreateHandlerItem(aHandler);
  Result.FEventID:=aEventID;
end;

constructor TEventDispatcher.Create(aDefaultSender: TObject);
begin
  inherited create;
  FDefaultSender:=aDefaultSender;
  FHandlerList:=CreateHandlerList;
end;

destructor TEventDispatcher.Destroy;
begin
  FreeAndNil(FHandlerList);
  inherited Destroy;
end;

function TEventDispatcher.RegisterHandler(aHandler: TEventHandler;
  aEventID: TEventID): TEventHandlerItem;
begin
  Result:=DoRegisterHandler(aHandler,aEventID);
end;

function TEventDispatcher.RegisterHandler(aHandler: TEventCallBack;
  aEventID: TEventID): TEventHandlerItem;
begin
  Result:=DoRegisterHandler(aHandler,aEventID);
end;

function TEventDispatcher.RegisterHandler(aHandler: TEventHandlerRef;
  aEventID: TEventID): TEventHandlerItem;
begin
  Result:=DoRegisterHandler(aHandler,aEventID);
end;

function TEventDispatcher.RegisterHandler(aHandler: TEventCallback;
  aEventName: TEventName): TEventHandlerItem;
begin
  Result:=DoRegisterHandler(aHandler,Registry.GetEventID(aEventName));
end;

function TEventDispatcher.RegisterHandler(aHandler: TEventHandler;
  aEventName: TEventName): TEventHandlerItem;
begin
  Result:=DoRegisterHandler(aHandler,Registry.GetEventID(aEventName));
end;

function TEventDispatcher.RegisterHandler(aHandler: TEventHandlerRef;
  aEventName: TEventName): TEventHandlerItem;
begin
  Result:=DoRegisterHandler(aHandler,Registry.GetEventID(aEventName));

end;

procedure TEventDispatcher.UnregisterHandler(aItem: TEventHandlerItem);
begin
  FHandlerList.UnregisterHandler(aItem);
end;

procedure TEventDispatcher.UnregisterHandler(aEventID: TEventID);

Var
  I : Integer;
  Itm : TEventHandlerItem;

begin
  for I:=FHandlerList.Count-1 downto 0 do
    begin
    Itm:=FHandlerList[I];
    If Itm.EventID=aEventID then
      FHandlerList.UnregisterHandler(Itm);
    end;
end;

procedure TEventDispatcher.UnregisterHandler(aEventName: TEventName);
begin
  UnregisterHandler(Registry.GetEventID(aEventName));
end;

procedure TEventDispatcher.UnregisterHandler(aItemID: Integer);
Var
  aItem: TEventHandlerItem;
begin
  aItem:=TEventHandlerItem(FHandlerList.FindItemID(aItemID));
  if Assigned(aItem) then
    FHandlerList.UnregisterHandler(aItem);
end;

procedure TEventDispatcher.UnRegisterHandler(aHandler: TEventHandler);
Var
  I : Integer;
  Itm : TEventHandlerItem;

begin
  for I:=FHandlerList.Count-1 downto 0 do
    begin
    Itm:=FHandlerList[I];
    If Itm.MatchHandler(aHandler,Itm.EventID) then
      FHandlerList.UnregisterHandler(Itm);
    end;
end;

procedure TEventDispatcher.UnRegisterHandler(aHandler: TEventCallBack);
Var
  I : Integer;
  Itm : TEventHandlerItem;

begin
  for I:=FHandlerList.Count-1 downto 0 do
    begin
    Itm:=FHandlerList[I];
    If Itm.MatchHandler(aHandler,Itm.EventID) then
      FHandlerList.UnregisterHandler(Itm);
    end;
end;

procedure TEventDispatcher.UnRegisterHandler(aHandler: TEventHandlerRef);
Var
  I : Integer;
  Itm : TEventHandlerItem;

begin
  for I:=FHandlerList.Count-1 downto 0 do
    begin
    Itm:=FHandlerList[I];
    If Itm.MatchHandler(aHandler,Itm.EventID) then
      FHandlerList.UnregisterHandler(Itm);
    end;
end;

procedure TEventDispatcher.UnRegisterHandler(aHandler: TEventHandler;
  aEventID: TEventID);

Var
  aItem: TEventHandlerItem;

begin
  aItem:=FHandlerList.FindHandler(aHandler,aEventID);
  UnregisterHandler(aItem);
end;


procedure TEventDispatcher.UnRegisterHandler(aHandler: TEventCallBack;
  aEventID: TEventID);
Var
  aItem: TEventHandlerItem;

begin
  aItem:=FHandlerList.FindHandler(aHandler,aEventID);
  UnregisterHandler(aItem);
end;

procedure TEventDispatcher.UnRegisterHandler(aHandler: TEventHandlerRef;
  aEventID: TEventID);

Var
  aItem: TEventHandlerItem;

begin
  aItem:=FHandlerList.FindHandler(aHandler,aEventID);
  UnregisterHandler(aItem);
end;

procedure TEventDispatcher.UnRegisterHandler(aHandler: TEventCallback;
  aEventName: TEventName);
begin
  UnregisterHandler(aHandler,Registry.FindEventID(aEventName));
end;

procedure TEventDispatcher.UnRegisterHandler(aHandler: TEventHandler;
  aEventName: TEventName);
begin
  UnregisterHandler(aHandler,Registry.FindEventID(aEventName));
end;

procedure TEventDispatcher.UnRegisterHandler(aHandler: TEventHandlerRef;
  aEventName: TEventName);
begin
  UnregisterHandler(aHandler,Registry.FindEventID(aEventName));
end;

function TEventDispatcher.CreateEvent(aSender: TObject; aEventID: TEventID
  ): TAbstractEvent;

Var
  aClass : TAbstractEventClass;

begin
  If aSender=Nil then
    Raise EEvents.Create('Cannot create event without sender.');
  aClass:=Registry.GetEventClass(aEventID);
  Result:=aClass.Create(aSender,aEventID);
end;

function TEventDispatcher.CreateEvent(aSender: TObject; aEventName: TEventName
  ): TAbstractEvent;
begin
  Result:=CreateEvent(aSender,Registry.GetEventID(aEventName))
end;

function TEventDispatcher.DispatchEvent(aEvent: TAbstractEvent): Integer;
begin
  Result:=FHandlerList.CallAllHandlers(aEvent);
end;


function TEventDispatcher.DispatchEvent(aEventID: TEventID; aOnSetup: TEventSetupHandler): Integer;
begin
  If DefaultSender=Nil then
    Raise EEvents.Create('Cannot dispatch without sender: defaultsender not set');
  Result:=DispatchEvent(aEventID,DefaultSender,aOnSetup);
end;

function TEventDispatcher.DispatchEvent(aEventID: TEventID; aOnSetup: TEventSetupCallBack): Integer;
begin
  If DefaultSender=Nil then
    Raise EEvents.Create('Cannot dispatch without sender: defaultsender not set');
  Result:=DispatchEvent(aEventID,DefaultSender,aOnSetup);
end;

function TEventDispatcher.DispatchEvent(aEventID: TEventID; aOnSetup: TEventSetupHandlerRef): Integer;
begin
  If DefaultSender=Nil then
    Raise EEvents.Create('Cannot dispatch without sender: defaultsender not set');
  Result:=DispatchEvent(aEventID,DefaultSender,aOnSetup);
end;

function TEventDispatcher.DispatchEvent(aEventName: TEventName): Integer;
begin
  Result:=DispatchEvent(Registry.GetEventID(aEventName));
end;

function TEventDispatcher.DispatchEvent(aEventName: TEventName; aSender: TObject): Integer;
begin
  Result:=DispatchEvent(Registry.GetEventID(aEventName),aSender);
end;

function TEventDispatcher.DispatchEvent(aEventName: TEventName; aSender: TObject; aOnSetup: TEventSetupHandler): Integer;
begin
  Result:=DispatchEvent(Registry.GetEventID(aEventName),aSender,aOnSetup);
end;

function TEventDispatcher.DispatchEvent(aEventName: TEventName; aSender: TObject; aOnSetup: TEventSetupCallBack): Integer;
begin
  Result:=DispatchEvent(Registry.GetEventID(aEventName),aSender,aOnSetup);
end;

function TEventDispatcher.DispatchEvent(aEventName: TEventName; aSender: TObject; aOnSetup: TEventSetupHandlerRef): Integer;
begin
  Result:=DispatchEvent(Registry.GetEventID(aEventName),aSender,aOnSetup);
end;

function TEventDispatcher.DispatchEvent(aEventName: TEventName; aOnSetup: TEventSetupHandler): Integer;
begin
  Result:=DispatchEvent(Registry.GetEventID(aEventName),aOnSetup);
end;

function TEventDispatcher.DispatchEvent(aEventName: TEventName; aOnSetup: TEventSetupCallBack): Integer;
begin
  Result:=DispatchEvent(Registry.GetEventID(aEventName),aOnSetup);
end;

function TEventDispatcher.DispatchEvent(aEventName: TEventName; aOnSetup: TEventSetupHandlerRef): Integer;
begin
  Result:=DispatchEvent(Registry.GetEventID(aEventName),aOnSetup);
end;

function TEventDispatcher.DispatchEvent(aEventID: TEventID): Integer;
begin
  If DefaultSender=Nil then
    Raise EEvents.Create('Cannot dispatch without sender: defaultsender not set');
  Result:=DispatchEvent(aEventID,DefaultSender);
end;

function TEventDispatcher.DispatchEvent(aEventID: TEventID; aSender: TObject
  ): Integer;
begin
  Result:=DispatchEvent(aEventID,aSender,TEventSetupHandler(Nil));
end;

function TEventDispatcher.DispatchEvent(aEventID: TEventID; aSender: TObject;
  aOnSetup: TEventSetupHandler): Integer;

Var
  Evt : TAbstractEvent;

begin
  Evt:=CreateEvent(aSender,aEventID);
  try
    if Assigned(aOnSetup) then
      aOnSetup(Evt);
    Result:=DispatchEvent(Evt);
  finally
    Evt.Free;
  end;
end;

function TEventDispatcher.DispatchEvent(aEventID: TEventID; aSender: TObject;
  aOnSetup: TEventSetupCallBack): Integer;
Var
  Evt : TAbstractEvent;

begin
  Evt:=CreateEvent(aSender,aEventID);
  try
    if Assigned(aOnSetup) then
      aOnSetup(Evt);
    Result:=DispatchEvent(Evt);
  finally
    Evt.Free;
  end;
end;

function TEventDispatcher.DispatchEvent(aEventID: TEventID; aSender: TObject;
  aOnSetup: TEventSetupHandlerRef): Integer;
Var
  Evt : TAbstractEvent;

begin
  Evt:=CreateEvent(aSender,aEventID);
  try
    if Assigned(aOnSetup) then
      aOnSetup(Evt);
    Result:=DispatchEvent(Evt);
  finally
    Evt.Free;
  end;
end;


{ TEventDef }

constructor TEventDef.Create(aID: TEventID; aClass: TAbstractEventClass);
begin
  FID:=aID;
  FClass:=aClass;
end;

{ TEventRegistry }

function TEventRegistry.GetNextID: TEventID;
begin
  if FNextID>=Length(FEventDefs) then
    SetCapacity(Length(FEventDefs)+100);
  Result:=FNextID;
  Inc(FNextID);
end;

class function TEventRegistry.DefaultIDOffset: TEventID;
begin
  Result:=0;
end;

class procedure TEventRegistry.SetInstance(aInstance: TEventRegistry);
begin
  FreeandNil(_Instance);
  _Instance:=aInstance;
end;

function TEventRegistry.DoRegisterEvent(aID: TEventID;
  aClass: TAbstractEventClass): TEventDef;

begin
  Result:=TEventDef.Create(aID,aClass);
  FEventDefs[aID]:=Result;
  FHash.Add(LowerCase(aClass.EventName),Result);
end;

class constructor TEventRegistry.init;
begin
  TEventRegistry._instance:=TEventRegistry.Create;
end;

class destructor TEventRegistry.done;
begin
  FreeAndNil(TEventRegistry._instance);
end;

constructor TEventRegistry.Create;
begin
  FIDOffset:=DefaultIDOffset;
  FNextID:=FIDOffset+1;
  SetCapacity(FIDOffset+100);
  FHash:=TFPObjectHashTable.Create(False);
end;

destructor TEventRegistry.Destroy;
begin
  Clear;
  SetCapacity(0);
  FreeAndNil(FHash);
  inherited Destroy;
end;

procedure TEventRegistry.Clear;

Var
  I : Integer;

begin
  For I:=0 to Length(FEventDefs)-1 do
    FreeAndNil(FEventDefs[i]);
  FNextID:=FIDOffset+1;
  FHash.Clear;
end;

function TEventRegistry.GetRegisteredEventCount: Integer;

Var
  Def : TEventDef;

begin
  Result:=0;
  For Def in FEventDefs do
    if Def<>Nil then
      Inc(Result);
end;

procedure TEventRegistry.SetCapacity(aCapacity: TEVentID);

begin
  if aCapacity>=High(TEventID) then
    Raise EEvents.CreateFmt('Invalid capacity for event lists: %d',[aCapacity]);
  SetLength(FEventDefs,aCapacity);
end;

function TEventRegistry.RegisterEventWithID(aID: TEventID;
  aClass: TAbstractEventClass): TEventID;
begin
  Result:=0;
  if (aID=0) or (aID>=FIDOffset) then
    Raise EEvents.CreateFmt('Invalid event ID for registration: %d',[aID]);
  if FindEventID(aClass.EventName)<>0 then
    Raise EEvents.CreateFmt('Duplicate event name for registration: %s',[aClass.EventName]);
  if FEventDefs[aID]<>Nil then
    Raise EEvents.CreateFmt('Duplicate event ID for registration (%s): %d',[aClass.EventName,aid]);
  Result:=DoRegisterEvent(aID,aClass).ID;
end;

function TEventRegistry.RegisterEvent(aClass: TAbstractEventClass): TEventID;
begin
  if FindEventID(aClass.EventName)<>0 then
    Raise EEvents.CreateFmt('Duplicate event name for registration: %s',[aClass.EventName]);
  Result:=DoRegisterEvent(GetNextID,aClass).ID;
end;

function TEventRegistry.FindEventClass(aEventID: TEventID): TAbstractEventClass;

var
  aDef: TEventDef;
begin
  Result:=nil;
  aDef:=Nil;
  if (aEventID>=1) and (aEventID<Length(FEventDefs)) then
    aDef:=FEventDefs[aEventID];
  if Assigned(aDef) then
    Result:=aDef.EventClass;
end;

function TEventRegistry.GetEventClass(aEventID: TEventID): TAbstractEventClass;

begin
  Result:=FindEventClass(aEventID);
  if Result=nil then
    Raise EEvents.CreateFmt('Unknown event ID: %d',[aEventID]);
end;

function TEventRegistry.FindEventClass(aEventName: TEventName): TAbstractEventClass;

Var
  Def : TEventDef;

begin
  Result:=Nil;
  Def:=TEventDef(FHash.Items[LowerCase(aEventName)]);
  if Assigned(Def) then
    Result:=Def.EventClass;
end;

function TEventRegistry.GetEventClass(aEventName: TEventName): TAbstractEventClass;
begin
  Result:=FindEventClass(aEventName);
  if Result=Nil then
    Raise EEvents.CreateFmt('Unknown event name: %s',[aEventName]);
end;

function TEventRegistry.FindEventID(aEventName: TEventName): TEventID;

Var
  Def : TEventDef;

begin
  Result:=0;
  Def:=TEventDef(FHash.Items[LowerCase(aEventName)]);
  if Assigned(Def) then
    Result:=Def.ID;
end;

function TEventRegistry.FindEventName(aEventID: TEventID): TEventName;

Var
  Def : TEventDef;

begin
  Result:='';
  Def:=FEventDefs[aEventID];
  if Assigned(Def) and assigned(Def.EventClass) then
    Result:=Def.EventClass.EventName;
end;

function TEventRegistry.GetEventID(aEventName: TEventName): TEventID;
begin
  Result:=FindEventID(aEventName);
  if Result=0 then
    Raise EEvents.CreateFmt('Unknown event name: %s',[aEventName]);
end;

function TEventRegistry.GetEventName(aEventID: TEventID): TEventName;
begin
  Result:=FindEVentName(aEventID);
  if Result='' then
    Raise EEvents.CreateFmt('Unknown event ID: %d',[aEventID]);
end;

procedure TEventRegistry.UnRegisterEvent(aClass: TAbstractEventClass);
begin
  UnRegisterEvent(aClass.EventName);
end;

procedure TEventRegistry.UnRegisterEvent(aEventID: TEventID);

begin
  if (aEventID>0) and (aEventID<Length(FEventDefs)) then
    if Assigned(FEventDefs[aEventID]) then
      begin
      FHash.Delete(FEventDefs[aEventID].EventClass.EventName);
      FreeAndNil(FeventDefs[aEventID])
      end;
  // We ignore invalid aEventIDs.
end;

procedure TEventRegistry.UnRegisterEvent(aEventName: TEventName);

var
  aID : TEventID;
begin
  aID:=FindEventID(aEventName);
  if aID<>0 then
    UnregisterEvent(aID);
end;

{ TAbstractEvent }

constructor TAbstractEvent.Create(aSender: TObject; aID: TEventID);
begin
  FSender:=aSender;
  FEventID:=aID;
end;

class function TAbstractEvent.Register: TEventID;
begin
  Result:=TEventRegistry.Instance.RegisterEvent(Self);
end;

end.

