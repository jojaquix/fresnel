{
 *****************************************************************************
  This file is part of Fresnel.

  See the file COPYING.modifiedLGPL.txt, included in this distribution,
  for details about the license.
 *****************************************************************************

ToDo:

}
unit Fresnel.Layouter;

{$mode ObjFPC}{$H+}
{$Interfaces CORBA}
{$WARN 6060 off} // Case statement does not handle all possible cases

interface

uses
  Classes, SysUtils, Math, fpCSSResolver, Fresnel.DOM, Fresnel.Controls,
  LazLoggerBase;

type
  EFresnelLayout = class(Exception)
  end;

  // fresnel layout node preferred size flags
  TFLNPreferredSizeFlag = (
    flnpsApplyMinWidth,
    flnpsApplyMinHeight,
    flnpsApplyMaxWidth,
    flnpsApplyMaxHeight
    );
  TFLNPreferredSizeFlags = set of TFLNPreferredSizeFlag;

const
  flnpsApplyMinMax = [flnpsApplyMinWidth,flnpsApplyMinHeight,flnpsApplyMaxWidth,flnpsApplyMaxHeight];

type
  TSimpleFresnelLayoutNode = class;

  { TFLNodeLayouter }

  TFLNodeLayouter = class(TComponent)
  public
    Node: TSimpleFresnelLayoutNode;
    procedure Apply; virtual; abstract;
    procedure ComputedChildrenCSS; virtual;
    function GetMinWidthBorderBox: TFresnelLength; virtual; abstract;
    function GetPreferredBorderBox_MaxWidth(MaxWidth: TFresnelLength): TFresnelPoint; virtual; abstract;
    function GetViewport: TFresnelViewport;
  end;
  TFLNodeLayouterClass = class of TFLNodeLayouter;

  { TSimpleFresnelLayoutNode }

  TSimpleFresnelLayoutNode = class(TFresnelLayoutNode)
  public
    BlockContainer: TFresnelElement;
    Layouter: TFLNodeLayouter;
    SkipLayout: boolean; // e.g. element or descendant display-box:none
    SkipRendering: boolean; // e.g. element or descendant display-box:none or visibility<>visible
    SubParent: boolean; // a node in between: has no Layouter and has children
    ZIndex: single;
    destructor Destroy; override;
    function GetMinWidthBorderBox: TFresnelLength; virtual;
    function GetPreferredBorderBox_MaxWidth(MaxWidth: TFresnelLength; const Flags: TFLNPreferredSizeFlags = []): TFresnelPoint; virtual;
  end;

  { TFLBlockFormattingContext - flow layouter }

  TFLBlockFormattingContext = class(TFLNodeLayouter)
  protected
    type

      { TFLBFCNode - Fresnel layouter block formatting context node }

      TFLBFCNode = class
      public
        Node: TSimpleFresnelLayoutNode;
        BorderLeft: TFresnelLength;
        BorderRight: TFresnelLength;
        BorderTop: TFresnelLength;
        BorderBottom: TFresnelLength;
        PaddingLeft: TFresnelLength;
        PaddingRight: TFresnelLength;
        PaddingTop: TFresnelLength;
        PaddingBottom: TFresnelLength;
        MarginLeft: TFresnelLength;
        MarginRight: TFresnelLength;
        MarginTop: TFresnelLength;
        MarginBottom: TFresnelLength;
        Left: TFresnelLength;
        Right: TFresnelLength;
        Width: TFresnelLength;
        Height: TFresnelLength;
      end;
  protected
    FBorderLeft: TFresnelLength;
    FBorderRight: TFresnelLength;
    FBorderTop: TFresnelLength;
    FBorderBottom: TFresnelLength;
    FPaddingLeft: TFresnelLength;
    FPaddingRight: TFresnelLength;
    FPaddingTop: TFresnelLength;
    FPaddingBottom: TFresnelLength;
    FContentSize: TFresnelPoint;
    FContentSizeMaxWidth: TFresnelLength;
    // current line
    FFirstLineNodeIndex: integer;
    FLastLineNodeIndex: integer;
    FLineBorderBoxHeight: TFresnelLength;
    FLineBorderBoxLeft: TFresnelLength;
    FLineBorderBoxRight: TFresnelLength;
    FLineBorderBoxTop: TFresnelLength;
    FLineBorderBoxBottom: TFresnelLength;
    FLineMarginLeft: TFresnelLength;
    FLineMarginRight: TFresnelLength;
    FLineMarginTop: TFresnelLength;
    FLineMarginBottom: TFresnelLength;
    FLastLineBorderBoxBottom: TFresnelLength;
    FLastLineMarginBottom: TFresnelLength;
    FLineNodes: TFPList; // list of TFLBFCNode
    procedure EndLine(Commit: boolean); virtual;
    procedure StartLine; virtual;
    procedure PlaceLineNodes; virtual;
    procedure PlaceAbsoluteNode(ChildNode: TSimpleFresnelLayoutNode); virtual;
    procedure ClearLineNodes;
    function AddLineNode(ChildNode: TSimpleFresnelLayoutNode): TFLBFCNode; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Apply; override;
    procedure ComputedChildrenCSS; override;
    function ComputeLayoutBorderBox(MaxWidth: TFresnelLength; Commit: boolean): TFresnelPoint; virtual;
    function GetMinWidthBorderBox: TFresnelLength; override;
    function GetPreferredBorderBox_MaxWidth(MaxWidth: TFresnelLength
      ): TFresnelPoint; override;
  end;

  { TFLGridLayouter }

  TFLGridLayouter = class(TFLNodeLayouter)
  public
    procedure Apply; override;
  end;

  { TFLFlexLayouter }

  TFLFlexLayouter = class(TFLNodeLayouter)
  public
    procedure Apply; override;
  end;

  { TSimpleFresnelLayouter }

  TSimpleFresnelLayouter = class(TFresnelLayouter)
  private
    FViewport: TFresnelViewport;
    procedure SetViewport(const AValue: TFresnelViewport);
  protected
    procedure ErrorLayout(const ID: int64; const Msg: string); virtual;
    procedure Layout(Node: TSimpleFresnelLayoutNode); virtual;
    function CreateLayoutNode(El: TFresnelElement): TSimpleFresnelLayoutNode; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Apply(aViewport: TFresnelViewport);
    function NeedBlockFormattingContext(El: TFresnelElement): boolean; virtual;
    function GetBlockContainer(El: TFresnelElement): TFresnelElement; virtual;
    procedure UpdateLayouter(El: TFresnelElement; LNode: TSimpleFresnelLayoutNode); virtual;
    procedure UpdateLayoutParent(El: TFresnelElement; LNode: TSimpleFresnelLayoutNode); virtual;
    function GetPixPerUnit(El: TFresnelElement; anUnit: TFresnelCSSUnit; IsHorizontal: boolean): TFresnelLength; virtual;
    procedure ConvertCSSBorderWidthToPix(El: TFresnelElement; Attr: TFresnelCSSAttribute); virtual;
    procedure ConvertCSSValueToPixel(El: TFresnelElement; Attr: TFresnelCSSAttribute; IsHorizontal: boolean); virtual;
    procedure ComputeCSS(El: TFresnelElement); override; // called after basic CSS properties were computed, compute all lengths to px
    procedure ComputedChildrenCSS(El: TFresnelElement); override; // called after child nodes CSS properties were computed
    procedure SortStackingContext(LNode: TSimpleFresnelLayoutNode); virtual; // apply z-index
    procedure WriteLayoutTree;
    property Viewport: TFresnelViewport read FViewport write SetViewport;
    // needs:
    // viewport width, height, DPI, font size default, font size min
    // scrollbar width, height
    // current scroll x,y in ViewPort
    // measure text width

    // produces:
    // layout tree
    // boxes
    // margin, border, padding
    // left, top, width, height
    // clipping: left, top, right, bottom
    // font-size
    // font-style  normal italic oblique bold
  end;

function CompareLayoutNodesZIndexDomIndex(Item1, Item2, {%H-}Context: Pointer): integer;

implementation

function CompareLayoutNodesZIndexDomIndex(Item1, Item2, Context: Pointer): integer;
var
  Node1: TSimpleFresnelLayoutNode absolute Item1;
  Node2: TSimpleFresnelLayoutNode absolute Item2;
  DomIndex1, DomIndex2: Integer;
begin
  if Node1.ZIndex>Node2.ZIndex then
    Result:=1
  else if Node1.ZIndex<Node2.ZIndex then
    Result:=-1
  else begin
    DomIndex1:=Node1.Element.DOMIndex;
    DomIndex2:=Node2.Element.DOMIndex;
    if DomIndex1>DomIndex2 then
      Result:=1
    else if DomIndex1<DomIndex2 then
      Result:=-1
    else
      Result:=0;
  end;
end;

{ TFLNodeLayouter }

procedure TFLNodeLayouter.ComputedChildrenCSS;
begin

end;

function TFLNodeLayouter.GetViewport: TFresnelViewport;
var
  aRoot: TFresnelLayoutNode;
begin
  aRoot:=Node.GetRoot;
  if aRoot.Element is TFresnelViewport then
    Result:=TFresnelViewport(aRoot.Element)
  else
    Result:=nil;
end;


{ TFLFlexLayouter }

procedure TFLFlexLayouter.Apply;
begin

end;

{ TFLGridLayouter }

procedure TFLGridLayouter.Apply;
begin

end;

{ TFLBlockFormattingContext }

procedure TFLBlockFormattingContext.EndLine(Commit: boolean);
begin
  if FFirstLineNodeIndex<0 then
  begin
    // line empty
    exit;
  end;
  FLineBorderBoxTop:=FLastLineBorderBoxBottom
            +Max(FLastLineMarginBottom,FLineMarginTop); // margin collapsing
  FLineBorderBoxBottom:=FLineBorderBoxTop+FLineBorderBoxHeight;

  FContentSize.X:=Max(FContentSize.X,FLineBorderBoxRight+FLineMarginRight+FPaddingRight+FBorderRight);
  FContentSize.Y:=FLineBorderBoxBottom+FLineMarginBottom+FPaddingBottom+FBorderBottom;

  if Commit then
    PlaceLineNodes;
  ClearLineNodes;

  FLastLineBorderBoxBottom:=FLineBorderBoxBottom;
  FLastLineMarginBottom:=FLineMarginBottom;
end;

procedure TFLBlockFormattingContext.StartLine;
begin
  FFirstLineNodeIndex:=-1;
  FLastLineNodeIndex:=-1;
  FLineBorderBoxHeight:=0;
  FLineBorderBoxLeft:=FBorderLeft+FPaddingLeft;
  FLineBorderBoxRight:=FLineBorderBoxLeft;
  FLineMarginLeft:=0;
  FLineMarginTop:=0;
  FLineMarginRight:=0;
  FLineMarginBottom:=0;
end;

procedure TFLBlockFormattingContext.PlaceLineNodes;
var
  BFCNode: TFLBFCNode;
  ChildNode: TSimpleFresnelLayoutNode;
  ChildEl: TFresnelElement;
  i: Integer;
  ChildTop, ChildBottom: TFresnelLength;
begin
  for i:=0 to FLineNodes.Count-1 do
  begin
    BFCNode:=TFLBFCNode(FLineNodes[i]);
    ChildNode:=BFCNode.Node;
    ChildEl:=ChildNode.Element;

    ChildTop:=FLineBorderBoxTop-BFCNode.MarginTop;
    ChildBottom:=FLineBorderBoxTop
                +BFCNode.BorderTop+BFCNode.PaddingTop
                +BFCNode.Height
                +BFCNode.PaddingBottom+BFCNode.BorderBottom+BFCNode.MarginBottom;

    ChildEl.CSSComputedAttribute[fcaLeft]:=FloatToCSSStr(BFCNode.Left);
    ChildEl.CSSComputedAttribute[fcaRight]:=FloatToCSSStr(BFCNode.Right);
    ChildEl.CSSComputedAttribute[fcaWidth]:=FloatToCSSStr(BFCNode.Width);
    ChildEl.CSSComputedAttribute[fcaTop]:=FloatToCSSStr(ChildTop);
    ChildEl.CSSComputedAttribute[fcaBottom]:=FloatToCSSStr(ChildBottom);
    ChildEl.CSSComputedAttribute[fcaHeight]:=FloatToCSSStr(BFCNode.Height);

    debugln(['TFLBlockFormattingContext.PlaceLineNodes ',ChildEl.GetPath,
      ' Left=',ChildEl.CSSComputedAttribute[fcaLeft],
      ' Top=',ChildEl.CSSComputedAttribute[fcaTop],
      ' Width=',ChildEl.CSSComputedAttribute[fcaWidth],
      ' Height=',ChildEl.CSSComputedAttribute[fcaHeight],
      ' Right=',ChildEl.CSSComputedAttribute[fcaRight],
      ' Bottom=',ChildEl.CSSComputedAttribute[fcaBottom],
      '']);
  end;
end;

procedure TFLBlockFormattingContext.PlaceAbsoluteNode(
  ChildNode: TSimpleFresnelLayoutNode);
var
  ChildEl: TFresnelElement;
  ChildLeft, ChildRight, ChildTop, ChildBottom, ChildWidth,
    ChildHeight, ChildBorderLeft, ChildBorderRight,
    ChildBorderTop, ChildBorderBottom, ChildPaddingLeft,
    ChildPaddingRight, ChildPaddingTop, ChildPaddingBottom,
    ChildMinWidth, ChildMaxWidth, ChildMinHeight, ChildMaxHeight,
    ChildMarginLeft, ChildMarginRight, ChildMarginTop,
    ChildMarginBottom: TFresnelLength;
  ChildPrefBorderBox: TFresnelPoint;

  procedure ComputeChildWidth;
  begin
    if IsNan(ChildWidth) then
    begin
      ChildPrefBorderBox:=ChildNode.GetPreferredBorderBox_MaxWidth(GetViewport.MaxPreferredWidth,flnpsApplyMinMax);
      ChildWidth:=ChildPrefBorderBox.X;
    end else begin
      ChildMaxWidth:=ChildEl.GetComputedCSSLength(fcaMaxWidth,false,true);
      if not IsNan(ChildMaxWidth) then
        ChildWidth:=Min(ChildWidth,ChildMaxWidth);
      ChildMinWidth:=ChildNode.GetMinWidthBorderBox;
      ChildWidth:=Max(ChildWidth,ChildMinWidth);
    end;
  end;

  procedure ComputeChildHeight;
  begin
    if IsNan(ChildHeight) then
    begin
      if (ChildPrefBorderBox.X<0) or (ChildPrefBorderBox.X>ChildWidth) then
        ChildPrefBorderBox:=ChildNode.GetPreferredBorderBox_MaxWidth(ChildWidth,flnpsApplyMinMax);
      ChildHeight:=ChildPrefBorderBox.Y;
    end else begin
      ChildMaxHeight:=ChildEl.GetComputedCSSLength(fcaMaxHeight,false,true);
      if not IsNan(ChildMaxHeight) then
        ChildHeight:=Min(ChildHeight,ChildMaxHeight);
      ChildMinHeight:=ChildEl.GetComputedCSSLength(fcaMinHeight,false,true);
      if not IsNan(ChildMinHeight) then
        ChildHeight:=Min(ChildHeight,ChildMinHeight);
    end;
  end;

begin
  ChildEl:=ChildNode.Element;

  // left, top, right, bottom are marginbox
  ChildLeft:=ChildEl.GetComputedCSSLength(fcaLeft,false,true);
  ChildRight:=ChildEl.GetComputedCSSLength(fcaRight,false,true);
  ChildTop:=ChildEl.GetComputedCSSLength(fcaTop,false,true);
  ChildBottom:=ChildEl.GetComputedCSSLength(fcaBottom,false,true);

  ChildMarginLeft:=ChildEl.GetComputedCSSLength(fcaMarginLeft,false);
  ChildMarginRight:=ChildEl.GetComputedCSSLength(fcaMarginRight,false);
  ChildMarginTop:=ChildEl.GetComputedCSSLength(fcaMarginTop,false);
  ChildMarginBottom:=ChildEl.GetComputedCSSLength(fcaMarginBottom,false);

  ChildBorderLeft:=ChildEl.GetComputedCSSLength(fcaBorderLeftWidth,false);
  ChildBorderRight:=ChildEl.GetComputedCSSLength(fcaBorderRightWidth,false);
  ChildBorderTop:=ChildEl.GetComputedCSSLength(fcaBorderTopWidth,false);
  ChildBorderBottom:=ChildEl.GetComputedCSSLength(fcaBorderBottomWidth,false);

  ChildPaddingLeft:=ChildEl.GetComputedCSSLength(fcaPaddingLeft,false);
  ChildPaddingRight:=ChildEl.GetComputedCSSLength(fcaPaddingRight,false);
  ChildPaddingTop:=ChildEl.GetComputedCSSLength(fcaPaddingTop,false);
  ChildPaddingBottom:=ChildEl.GetComputedCSSLength(fcaPaddingBottom,false);

  // width, height are contentbox
  ChildWidth:=ChildEl.GetComputedCSSLength(fcaWidth,false,true);
  ChildHeight:=ChildEl.GetComputedCSSLength(fcaHeight,false,true);
  ChildPrefBorderBox.X:=-1;

  // either ChildLeft or ChildRight must be set
  if IsNan(ChildLeft) and IsNan(ChildRight) then
    ChildLeft:=0;
  if IsNan(ChildLeft) then
  begin
    // ChildRight is set
    ComputeChildWidth;
    ChildLeft:=ChildRight
               -ChildMarginRight-ChildBorderRight-ChildPaddingRight
               -ChildWidth
               -ChildPaddingLeft-ChildBorderLeft-ChildMarginLeft;
  end else begin
    if IsNan(ChildRight) then
      ComputeChildWidth
    else
      ChildWidth:=Max(0,ChildRight-ChildLeft);
    ChildRight:=ChildLeft
               +ChildMarginLeft+ChildBorderLeft+ChildPaddingLeft
               +ChildWidth
               +ChildPaddingRight+ChildBorderRight+ChildMarginRight;
  end;

  // either ChildTop or ChildBottom must be set
  if IsNan(ChildTop) and IsNan(ChildBottom) then
    ChildTop:=0;
  if IsNan(ChildTop) then
  begin
    // ChildBottom is set
    ComputeChildHeight;
    ChildTop:=ChildBottom
             -ChildMarginBottom-ChildBorderBottom-ChildPaddingBottom
             -ChildHeight
             -ChildPaddingTop-ChildBorderTop-ChildMarginTop;
  end else begin
    if IsNan(ChildBottom) then
      ComputeChildHeight
    else
      ChildHeight:=Max(0,ChildBottom-ChildTop);
    ChildBottom:=ChildTop
                +ChildMarginTop+ChildBorderTop+ChildPaddingTop
                +ChildHeight
                +ChildPaddingBottom+ChildBorderBottom+ChildMarginBottom;
  end;

  ChildEl.CSSComputedAttribute[fcaLeft]:=FloatToCSSStr(ChildLeft);
  ChildEl.CSSComputedAttribute[fcaRight]:=FloatToCSSStr(ChildRight);
  ChildEl.CSSComputedAttribute[fcaWidth]:=FloatToCSSStr(ChildWidth);
  ChildEl.CSSComputedAttribute[fcaTop]:=FloatToCSSStr(ChildTop);
  ChildEl.CSSComputedAttribute[fcaBottom]:=FloatToCSSStr(ChildBottom);
  ChildEl.CSSComputedAttribute[fcaHeight]:=FloatToCSSStr(ChildHeight);
  debugln(['TFLBlockFormattingContext.PlaceAbsoluteNode ',ChildEl.GetPath,
    ' Left=',ChildEl.CSSComputedAttribute[fcaLeft],
    ' Top=',ChildEl.CSSComputedAttribute[fcaTop],
    ' Width=',ChildEl.CSSComputedAttribute[fcaWidth],
    ' Height=',ChildEl.CSSComputedAttribute[fcaHeight],
    ' Right=',ChildEl.CSSComputedAttribute[fcaRight],
    ' Bottom=',ChildEl.CSSComputedAttribute[fcaBottom],
    '']);
end;

procedure TFLBlockFormattingContext.ClearLineNodes;
var
  i: Integer;
begin
  if FLineNodes=nil then
    exit;
  for i:=0 to FLineNodes.Count-1 do
     TObject(FLineNodes[i]).Free;
  FLineNodes.Clear;
end;

function TFLBlockFormattingContext.AddLineNode(
  ChildNode: TSimpleFresnelLayoutNode): TFLBFCNode;
begin
  Result:=TFLBFCNode.Create;
  if FLineNodes=nil then
    FLineNodes:=TFPList.Create;
  FLineNodes.Add(Result);
  Result.Node:=ChildNode;
end;

constructor TFLBlockFormattingContext.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FContentSize.X:=-1;
end;

destructor TFLBlockFormattingContext.Destroy;
begin
  ClearLineNodes;
  FreeAndNil(FLineNodes);
  inherited Destroy;
end;

function TFLBlockFormattingContext.GetMinWidthBorderBox: TFresnelLength;
var
  i: Integer;
  ChildNode: TSimpleFresnelLayoutNode;
  El, ChildEl: TFresnelElement;
  ChildMinWidth, MaxChildMinWidth, Margin: TFresnelLength;
begin
  El:=Node.Element;
  Result:=El.GetMinWidthBorderBox;
  MaxChildMinWidth:=0;
  for i:=0 to Node.NodeCount-1 do
  begin
    ChildNode:=TSimpleFresnelLayoutNode(Node.Nodes[i]);
    ChildMinWidth:=ChildNode.GetMinWidthBorderBox;
    ChildEl:=ChildNode.Element;
    Margin:=Max(0,ChildEl.GetComputedCSSLength(fcaMarginLeft,false));
    ChildMinWidth:=ChildMinWidth+Margin;
    Margin:=Max(0,ChildEl.GetComputedCSSLength(fcaMarginRight,false));
    ChildMinWidth:=ChildMinWidth+Margin;
    if MaxChildMinWidth<ChildMinWidth then
      MaxChildMinWidth:=ChildMinWidth;
  end;
  Result:=Result+MaxChildMinWidth;
end;

function TFLBlockFormattingContext.GetPreferredBorderBox_MaxWidth(
  MaxWidth: TFresnelLength): TFresnelPoint;
begin
  Result:=ComputeLayoutBorderBox(MaxWidth,false);
end;

procedure TFLBlockFormattingContext.Apply;
var
  MaxWidth: TFresnelLength;
begin
  Node.Element.WriteComputedAttributes('TFLBlockFormattingContext.Apply');

  MaxWidth:=Node.Element.GetComputedCSSLength(fcaWidth,false);
  ComputeLayoutBorderBox(MaxWidth,true);
end;

procedure TFLBlockFormattingContext.ComputedChildrenCSS;
var
  El: TFresnelElement;
begin
  inherited ComputedChildrenCSS;
  El:=Node.Element;

  FBorderLeft:=El.GetComputedCSSLength(fcaBorderLeft,false);
  FBorderTop:=El.GetComputedCSSLength(fcaBorderTop,false);
  FBorderRight:=El.GetComputedCSSLength(fcaBorderRight,false);
  FBorderBottom:=El.GetComputedCSSLength(fcaBorderBottom,false);
  FPaddingLeft:=El.GetComputedCSSLength(fcaPaddingLeft,false);
  FPaddingTop:=El.GetComputedCSSLength(fcaPaddingTop,false);
  FPaddingRight:=El.GetComputedCSSLength(fcaPaddingRight,false);
  FPaddingBottom:=El.GetComputedCSSLength(fcaPaddingBottom,false);
  FContentSize.X:=-1;
  FContentSize.Y:=-1;
end;

function TFLBlockFormattingContext.ComputeLayoutBorderBox(
  MaxWidth: TFresnelLength; Commit: boolean): TFresnelPoint;
var
  NodeIndex: Integer;
  ChildNode: TSimpleFresnelLayoutNode;
  ChildEl: TFresnelElement;
  aPosition, aDisplayOutside: String;
  IsInline: Boolean;
  NewChildRight, MaxChildRight, CurSpace, OldMarginBoxBottom,
    ChildMarginLeft, ChildMarginRight, ChildMarginTop, ChildMarginBottom,
    ChildMinWidth, ChildMaxWidth,
    ChildWidth, ChildHeight, ChildMinHeight, ChildMaxHeight,
    ChildBorderLeft, ChildBorderRight, ChildBorderTop, ChildBorderBottom,
    ChildPaddingLeft, ChildPaddingRight, ChildPaddingTop, ChildPaddingBottom,
    ChildLeft, ChildRight, ChildBorderBoxHeight: TFresnelLength;
  ChildPrefBorderBox: TFresnelPoint;

  procedure AddLineNodeCache;
  var
    N: TFLBFCNode;
  begin
    debugln(['AddLineNodeCache ',ChildEl.GetPath,' L=',ChildLeft,',R=',ChildRight,',W=',ChildWidth,',H=',ChildHeight]);
    N:=AddLineNode(ChildNode);
    N.BorderLeft:=ChildBorderLeft;
    N.BorderRight:=ChildBorderRight;
    N.BorderTop:=ChildBorderTop;
    N.BorderBottom:=ChildBorderBottom;
    N.PaddingLeft:=ChildPaddingLeft;
    N.PaddingRight:=ChildPaddingRight;
    N.PaddingTop:=ChildPaddingTop;
    N.PaddingBottom:=ChildPaddingBottom;
    N.MarginLeft:=ChildMarginLeft;
    N.MarginRight:=ChildMarginRight;
    N.MarginTop:=ChildMarginTop;
    N.MarginBottom:=ChildMarginBottom;
    N.Left:=ChildLeft;
    N.Right:=ChildRight;
    N.Width:=ChildWidth;
    N.Height:=ChildHeight;
  end;

begin
  if (not Commit) and (FContentSize.X>=0) and (FContentSizeMaxWidth=MaxWidth) then
    exit(FContentSize); // already computed, return last result

  Result:=default(TFresnelPoint);
  FContentSize:=default(TFresnelPoint);
  FContentSizeMaxWidth:=MaxWidth;
  // ToDo: fragments
  // ToDo: css float and clear
  // ToDo: css line-height
  // ToDo: fcaMarginBlockEnd, fcaMarginBlockStart, fcaMarginInlineEnd, fcaMarginInlineEnd

  MaxChildRight:=Max(0,MaxWidth-FBorderRight-FPaddingRight);
  FLastLineBorderBoxBottom:=FBorderTop+FPaddingTop;
  FLastLineMarginBottom:=0;

  // add elements to the line until full
  StartLine;
  NodeIndex:=0;
  while NodeIndex<Node.NodeCount do
  begin
    ChildNode:=TSimpleFresnelLayoutNode(Node.Nodes[NodeIndex]);
    ChildEl:=ChildNode.Element;
    //debugln(['TFLBlockFormattingContext.ComputeLayoutBorderBox ',ChildEl.Name]);

    // skip position: absolute and fixed
    aPosition:=ChildEl.CSSComputedAttribute[fcaPosition];
    case aPosition of
    '','static','relative','sticky': ;
    else
      if Commit then
        PlaceAbsoluteNode(ChildNode);
      inc(NodeIndex);
      continue;
    end;

    // display-outside inline or block
    aDisplayOutside:=ChildEl.CSSComputedAttribute[fcaDisplayOutside];
    case aDisplayOutside of
    '',
    'inline': IsInline:=true;
    else
      // block -> end line, put element on a line of its own
      IsInline:=false;
      EndLine(Commit);
      StartLine;
    end;

    ChildMarginLeft:=ChildEl.GetComputedCSSLength(fcaMarginLeft,false);
    ChildMarginRight:=ChildEl.GetComputedCSSLength(fcaMarginRight,false);
    ChildMarginTop:=ChildEl.GetComputedCSSLength(fcaMarginTop,false);
    ChildMarginBottom:=ChildEl.GetComputedCSSLength(fcaMarginBottom,false);

    ChildBorderLeft:=ChildEl.GetComputedCSSLength(fcaBorderLeftWidth,false);
    ChildBorderRight:=ChildEl.GetComputedCSSLength(fcaBorderRightWidth,false);
    ChildBorderTop:=ChildEl.GetComputedCSSLength(fcaBorderTopWidth,false);
    ChildBorderBottom:=ChildEl.GetComputedCSSLength(fcaBorderBottomWidth,false);

    ChildPaddingLeft:=ChildEl.GetComputedCSSLength(fcaPaddingLeft,false);
    ChildPaddingRight:=ChildEl.GetComputedCSSLength(fcaPaddingRight,false);
    ChildPaddingTop:=ChildEl.GetComputedCSSLength(fcaPaddingTop,false);
    ChildPaddingBottom:=ChildEl.GetComputedCSSLength(fcaPaddingBottom,false);

    CurSpace:=Max(0,MaxChildRight-FLineBorderBoxRight-Max(FLineMarginRight,ChildMarginLeft)-ChildMarginRight);

    // width, height are contentbox and can be NaN
    ChildWidth:=ChildEl.GetComputedCSSLength(fcaWidth,false,true);
    ChildHeight:=ChildEl.GetComputedCSSLength(fcaHeight,false,true);
    ChildMinWidth:=ChildEl.GetComputedCSSLength(fcaMinWidth,false,true);
    ChildMaxWidth:=ChildEl.GetComputedCSSLength(fcaMaxWidth,false,true);
    ChildMinHeight:=ChildEl.GetComputedCSSLength(fcaMinHeight,false,true);
    ChildMaxHeight:=ChildEl.GetComputedCSSLength(fcaMaxHeight,false,true);

    if (not IsNan(ChildMinWidth)) and (not IsNan(ChildMaxWidth)) and (ChildMinWidth>=ChildMaxWidth) then
      ChildWidth:=ChildMinWidth;
    if Commit and (not IsInline) and IsNan(ChildWidth) then
    begin
      // a block element expands the full line
      ChildWidth:=CurSpace-ChildBorderLeft-ChildPaddingLeft-ChildPaddingRight-ChildBorderRight;
    end;
    if not IsNan(ChildWidth) then
    begin
      if not IsNan(ChildMaxWidth) then
        ChildWidth:=Min(ChildWidth,ChildMaxWidth);
      if not IsNan(ChildMinWidth) then
        ChildWidth:=Max(ChildWidth,ChildMinWidth);
    end;

    if (not IsNan(ChildMinHeight)) and (not IsNan(ChildMaxHeight)) and (ChildMinHeight>=ChildMaxHeight) then
      ChildHeight:=ChildMinHeight
    else if not IsNan(ChildHeight) then
    begin
      if not IsNan(ChildMaxHeight) then
        ChildHeight:=Min(ChildHeight,ChildMaxHeight);
      if not IsNan(ChildMinHeight) then
        ChildHeight:=Max(ChildHeight,ChildMinHeight);
    end;

    if not IsNan(ChildWidth) and not IsNan(ChildHeight) then
    begin
      // fixed size
    end else begin
      // one or both sides are dynamic
      if IsNan(ChildWidth) then
      begin
        if not IsNan(ChildMaxWidth) then
          CurSpace:=Min(CurSpace,ChildMaxWidth);
        if not IsNan(ChildMinWidth) then
          CurSpace:=Max(CurSpace,ChildMinWidth);
      end else begin
        CurSpace:=ChildWidth;
      end;
      ChildPrefBorderBox:=ChildNode.GetPreferredBorderBox_MaxWidth(CurSpace,flnpsApplyMinMax);
      // apply fixed, min, max
      if not IsNan(ChildWidth) then
        ChildPrefBorderBox.X:=ChildWidth;
      if not IsNan(ChildMaxWidth) then
        ChildPrefBorderBox.X:=Min(ChildPrefBorderBox.X,ChildMaxWidth);
      if not IsNan(ChildMinWidth) then
        ChildPrefBorderBox.X:=Max(ChildPrefBorderBox.X,ChildMinWidth);
      if not IsNan(ChildHeight) then
        ChildPrefBorderBox.Y:=ChildHeight;
      if not IsNan(ChildMaxHeight) then
        ChildPrefBorderBox.Y:=Min(ChildPrefBorderBox.Y,ChildMaxHeight);
      if not IsNan(ChildMinHeight) then
        ChildPrefBorderBox.Y:=Max(ChildPrefBorderBox.Y,ChildMinHeight);
      ChildWidth:=ChildPrefBorderBox.X;
      ChildHeight:=ChildPrefBorderBox.Y;
    end;

    NewChildRight:=0;
    if (FFirstLineNodeIndex>=0) and IsInline then
    begin
      // check if inline element fits in line
      NewChildRight:=FLineBorderBoxRight
                     +Max(FLineMarginRight,ChildMarginLeft) // margin collapsing
                     +ChildBorderLeft+ChildPaddingLeft
                     +ChildWidth
                     +ChildPaddingRight+ChildBorderRight+ChildMarginRight;
      if NewChildRight>MaxChildRight then
      begin
        // element does not fit into the line -> next line
        EndLine(Commit);
        StartLine;
      end;
    end;

    ChildBorderBoxHeight:=ChildBorderTop+ChildPaddingTop+ChildHeight+ChildPaddingBottom+ChildBorderBottom;
    //debugln(['TFLBlockFormattingContext.ComputeLayoutBorderBox ',ChildEl.Name,' ',ChildBorderBoxHeight,':=BT ',ChildBorderTop,'+PT ',ChildPaddingTop,'+H ',ChildHeight,'+PB ',ChildPaddingBottom,'+BB ',ChildBorderBottom]);
    if FFirstLineNodeIndex<0 then
    begin
      // first element in line
      FFirstLineNodeIndex:=NodeIndex;
      FLastLineNodeIndex:=NodeIndex;

      FLineMarginLeft:=ChildMarginLeft;
      FLineMarginRight:=ChildMarginRight;
      FLineMarginTop:=ChildMarginTop;
      FLineMarginBottom:=ChildMarginBottom;

      // todo: div adds margin to left/top, span only to left)
      ChildLeft:=FBorderLeft+FPaddingLeft;
      ChildRight:=ChildLeft
                 +ChildMarginLeft+ChildBorderLeft+ChildPaddingLeft
                 +ChildWidth
                 +ChildPaddingRight+ChildBorderRight+ChildMarginRight;

      FLineBorderBoxLeft:=ChildLeft+ChildMarginLeft;
      FLineBorderBoxRight:=ChildRight-ChildMarginRight;
      FLineBorderBoxHeight:=ChildBorderBoxHeight;

      AddLineNodeCache;

      if (ChildRight>=MaxChildRight) or not IsInline then
      begin
        // first element already fills the max width
        EndLine(Commit);
        StartLine;
      end;
    end else begin
      // append element to line
      FLastLineNodeIndex:=NodeIndex;

      ChildLeft:=FLineBorderBoxRight
                 +Max(FLineMarginRight,ChildMarginLeft) // margin collapsing
                 -ChildMarginLeft;
      ChildRight:=NewChildRight;

      FLineBorderBoxRight:=ChildRight-ChildMarginRight;
      FLineMarginRight:=ChildMarginRight;

      // align element at the top of the line
      FLineMarginTop:=Max(FLineMarginTop,ChildMarginTop);
      OldMarginBoxBottom:=FLineBorderBoxHeight+FLineMarginBottom;
      FLineBorderBoxHeight:=Max(FLineBorderBoxHeight,ChildBorderBoxHeight);
      if OldMarginBoxBottom<ChildBorderBoxHeight+ChildMarginBottom then
      begin
        FLineMarginBottom:=ChildBorderBoxHeight+ChildMarginBottom-FLineBorderBoxHeight;
      end;

      AddLineNodeCache;
    end;

    inc(NodeIndex);
  end;
  EndLine(Commit);

  Result:=FContentSize;
end;

{ TSimpleFresnelLayoutNode }

destructor TSimpleFresnelLayoutNode.Destroy;
begin
  FreeAndNil(Layouter);
  BlockContainer:=nil;
  inherited Destroy;
end;

function TSimpleFresnelLayoutNode.GetMinWidthBorderBox: TFresnelLength;
begin
  if Layouter<>nil then
    Result:=Layouter.GetMinWidthBorderBox
  else
    Result:=Element.GetMinWidthBorderBox;
end;

function TSimpleFresnelLayoutNode.GetPreferredBorderBox_MaxWidth(
  MaxWidth: TFresnelLength; const Flags: TFLNPreferredSizeFlags): TFresnelPoint;

  function GetLimit(Attr: TFresnelCSSAttribute; out aLimit: TFresnelLength): boolean;
  begin
    aLimit:=Element.GetComputedCSSLength(Attr,false,true);
    Result:=not IsNan(aLimit);
  end;

var
  Limit: TFresnelLength;
begin
  if Layouter<>nil then
    Result:=Layouter.GetPreferredBorderBox_MaxWidth(MaxWidth)
  else
    Result:=Element.GetPreferredBorderBox_MaxWidth(MaxWidth);
  // apply max before min
  if (flnpsApplyMaxWidth in Flags) and GetLimit(fcaMaxWidth,Limit) then
    Result.X:=Min(Result.X,Limit);
  if (flnpsApplyMinWidth in Flags) and GetLimit(fcaMinWidth,Limit) then
    Result.X:=Max(Result.X,Limit);
  if (flnpsApplyMaxHeight in Flags) and GetLimit(fcaMaxHeight,Limit) then
    Result.Y:=Min(Result.Y,Limit);
  if (flnpsApplyMinHeight in Flags) and GetLimit(fcaMinHeight,Limit) then
    Result.Y:=Max(Result.Y,Limit);
end;

{ TSimpleFresnelLayouter }

procedure TSimpleFresnelLayouter.SetViewport(const AValue: TFresnelViewport);
var
  OldLNode: TFresnelLayoutNode;
begin
  if FViewport=AValue then Exit;
  if FViewport<>nil then
    FViewport.Layouter:=nil;
  FViewport:=AValue;
  if FViewport<>nil then
  begin
    FViewport.Layouter:=Self;
    if FViewport.LayoutNode<>nil then
    begin
      OldLNode:=FViewport.LayoutNode;
      FViewport.LayoutNode:=nil;
      OldLNode.Free;
    end;
    CreateLayoutNode(FViewport);
  end;
end;

procedure TSimpleFresnelLayouter.ErrorLayout(const ID: int64; const Msg: string);
var
  s: String;
begin
  s:='['+IntToStr(ID)+'] '+Msg;
  DebugLn(['Error: TFresnelLayouter.ErrorLayout ',s]);
  raise EFresnelLayout.Create(s);
end;

procedure TSimpleFresnelLayouter.Layout(Node: TSimpleFresnelLayoutNode);
var
  i: Integer;
begin
  if Node.SkipLayout then exit;

  if Node.Layouter<>nil then
    Node.Layouter.Apply;

  for i:=0 to Node.NodeCount-1 do
    Layout(TSimpleFresnelLayoutNode(Node.Nodes[i]));

  // sort for z-index
  SortStackingContext(Node);
end;

function TSimpleFresnelLayouter.CreateLayoutNode(El: TFresnelElement
  ): TSimpleFresnelLayoutNode;
begin
  if El.LayoutNode<>nil then
    Result:=TSimpleFresnelLayoutNode(El.LayoutNode)
  else begin
    Result:=TSimpleFresnelLayoutNode.Create(nil);
    El.LayoutNode:=Result;
    Result.Element:=El;
  end;
end;

constructor TSimpleFresnelLayouter.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

procedure TSimpleFresnelLayouter.Apply(aViewport: TFresnelViewport);
var
  aDisplayInside, aPosition, aDisplayOutside, aDisplayBox: String;
  VPNode: TSimpleFresnelLayoutNode;
begin
  Viewport:=aViewport;
  if Viewport=nil then
    exit;

  // sanity checks
  if not (Viewport.LayoutNode is TSimpleFresnelLayoutNode) then
    ErrorLayout(20221031154042,'TFresnelLayouter.Apply expected viewport.LayoutNode is TSimpleFresnelLayoutNode');
  VPNode:=TSimpleFresnelLayoutNode(Viewport.LayoutNode);
  if Viewport.NodeCount=0 then
    exit; // nothing to do

  aDisplayBox:=Viewport.CSSComputedAttribute[fcaDisplayBox];
  if aDisplayBox<>'' then
    ErrorLayout(20221031184139,'TFresnelLayouter.Apply expected viewport.displaybox=, but found "'+aDisplayBox+'"');
  aDisplayOutside:=Viewport.CSSComputedAttribute[fcaDisplayOutside];
  if aDisplayOutside<>'block' then
    ErrorLayout(20221031182815,'TFresnelLayouter.Apply expected viewport.displayoutside=block, but found "'+aDisplayOutside+'"');
  aDisplayInside:=Viewport.CSSComputedAttribute[fcaDisplayInside];
  if aDisplayInside<>'flow-root' then
    ErrorLayout(20221018142350,'TFresnelLayouter.Apply expected viewport.displayinside=flow-root, but found "'+aDisplayInside+'"');
  aPosition:=Viewport.CSSComputedAttribute[fcaPosition];
  if aPosition<>'absolute' then
    ErrorLayout(20221031153911,'TFresnelLayouter.Apply expected viewport.position=absolute, but found "'+aPosition+'"');

  Layout(VPNode);
end;

function TSimpleFresnelLayouter.NeedBlockFormattingContext(El: TFresnelElement
  ): boolean;
// BFC
// contain internal and external floats
// surpress margin collapsing
var
  aPosition, aOverflow, aFloat, aDisplayBox, aDisplayInside,
    aDisplayOutside: String;
begin
  if El=nil then
    exit(false);
  if El.Parent=nil then
    exit(true);

  aPosition:=El.CSSComputedAttribute[fcaPosition];
  case aPosition of
  'absolute',
  'fixed': exit(true);
  end;

  aDisplayBox:=El.CSSComputedAttribute[fcaDisplayBox];
  if aDisplayBox='none' then
    exit(false);

  aDisplayOutside:=El.CSSComputedAttribute[fcaDisplayOutside];
  case aDisplayOutside of
  'block': exit(true);
  end;

  aDisplayInside:=El.CSSComputedAttribute[fcaDisplayInside];
  case aDisplayInside of
  'flow-root',
  'table',
  'flex',
  'grid',
  'ruby': exit(true);
  end;

  aOverflow:=El.CSSComputedAttribute[fcaOverflow];
  case aOverflow of
  'visible',
  'clip': exit(true);
  end;

  aFloat:=El.CSSComputedAttribute[fcaFloat];
  case aFloat of
  '', 'none': ;
  else
    exit(true);
  end;

  // contain: layout, content, paint
  // flex items
  // grid items
  // multicol containers
  // column-span

  Result:=false;
end;

function TSimpleFresnelLayouter.GetBlockContainer(El: TFresnelElement
  ): TFresnelElement;
var
  aValue, aPosition: String;
  Node: TSimpleFresnelLayoutNode;
begin
  Node:=TSimpleFresnelLayoutNode(El.LayoutNode);
  Result:=Node.BlockContainer;
  if Result<>nil then exit;

  aPosition:=El.CSSComputedAttribute[fcaPosition];
  case aPosition of
  'absolute':
    begin
      // the containing block is nearest ancestor element that has a position value other than static
      Result:=El.Parent;
      while (Result<>nil) do
      begin
        aValue:=Result.CSSComputedAttribute[fcaPosition];
        if (aValue<>'static') and (aValue<>'') then
          exit;
        // Note:check for transform<>'none'
        Result:=Result.Parent;
      end;
      Result:=Viewport;
    end;
  'fixed':
    begin
      // viewport
      Result:=Viewport;
    end;
  else
    // static, relative, sticky
    // the containing block is the nearest ancestor element that is either a
    // block container (such as an inline-block, block, or list-item element)
    // or establishes a formatting context (such as a table container,
    // flex container, grid container, or the block container itself)
    Result:=El.Parent;
    while (Result<>nil) do
    begin
      if TSimpleFresnelLayoutNode(Result.LayoutNode).Layouter<>nil then
        exit;
      Result:=Result.Parent;
    end;
    Result:=Viewport;
  end;
end;

procedure TSimpleFresnelLayouter.UpdateLayouter(El: TFresnelElement;
  LNode: TSimpleFresnelLayoutNode);
var
  aDisplayInside: String;
  LayouterClass: TFLNodeLayouterClass;
begin
  LayouterClass:=nil;
  if (not LNode.SkipLayout) and (El.NodeCount>0) then
  begin
    aDisplayInside:=El.CSSComputedAttribute[fcaDisplayInside];
    if aDisplayInside='grid' then
      LayouterClass:=TFLGridLayouter
    else if aDisplayInside='flex' then
      LayouterClass:=TFLFlexLayouter
    else if NeedBlockFormattingContext(El) then
      LayouterClass:=TFLBlockFormattingContext;
  end;
  if (LayouterClass<>nil) then
  begin
    if (LNode.Layouter<>nil) and (LNode.Layouter.ClassType<>LayouterClass) then
      FreeAndNil(LNode.Layouter);
    if LNode.Layouter=nil then
    begin
      LNode.Layouter:=LayouterClass.Create(nil);
      LNode.Layouter.Node:=LNode;
    end;
  end else begin
    if (LNode.Layouter<>nil) then
      FreeAndNil(LNode.Layouter);
  end;
end;

procedure TSimpleFresnelLayouter.UpdateLayoutParent(El: TFresnelElement;
  LNode: TSimpleFresnelLayoutNode);
var
  ZIndexStr, aPosition: String;
  ZIndexInt, {%H-}Code: Integer;
  NewParent: TFresnelElement;
  ParentLNode: TSimpleFresnelLayoutNode;
begin
  debugln(['TSimpleFresnelLayouter.UpdateLayoutParent ',El.GetPath]);
  if LNode.SkipLayout or (El.Parent=nil) then
    LNode.Parent:=nil
  else begin
    aPosition:=El.CSSComputedAttribute[fcaPosition];
    case aPosition of
    'absolute','fixed','relative','sticky':
      begin
        ZIndexStr:=El.CSSComputedAttribute[fcaZIndex];
        if (ZIndexStr='') or (ZIndexStr='auto') then
          LNode.ZIndex:=0.0
        else begin
          ZIndexInt:=0;
          val(ZIndexStr,ZIndexInt,Code);
          LNode.ZIndex:=ZIndexInt;
        end;
        LNode.ZIndex:=LNode.ZIndex+0.1;
      end;
    else
      LNode.ZIndex:=0;
    end;

    NewParent:=LNode.BlockContainer;
    while (NewParent<>nil) and (TSimpleFresnelLayoutNode(NewParent.LayoutNode).Layouter=nil) do
      NewParent:=NewParent.Parent;
    if NewParent=nil then
      LNode.Parent:=nil
    else begin
      ParentLNode:=TSimpleFresnelLayoutNode(NewParent.LayoutNode);
      LNode.Parent:=ParentLNode;
      if ParentLNode.Layouter=nil then
        ParentLNode.SubParent:=true;
    end;
  end;
end;

function TSimpleFresnelLayouter.GetPixPerUnit(El: TFresnelElement;
  anUnit: TFresnelCSSUnit; IsHorizontal: boolean): TFresnelLength;
var
  B: TFresnelLength;
  aContainer: TFresnelElement;
begin
  Result:=1;
  case anUnit of
    fcuPercent:
      begin
        aContainer:=GetBlockContainer(El);
        if aContainer<>nil then
        begin
          if IsHorizontal then
          begin
            Result:=aContainer.GetComputedCSSLength(fcaWidth,false);
            B:=aContainer.GetComputedCSSLength(fcaPaddingLeft,false);
            Result:=Result+B;
            B:=aContainer.GetComputedCSSLength(fcaPaddingRight,false);
            Result:=Result+B;
          end
          else begin
            Result:=aContainer.GetComputedCSSLength(fcaHeight,false);
            exit(0);
            B:=aContainer.GetComputedCSSLength(fcaPaddingTop,false);
            Result:=Result+B;
            B:=aContainer.GetComputedCSSLength(fcaPaddingBottom,false);
            Result:=Result+B;
          end;
        end else begin
          if IsHorizontal then
            Result:=Viewport.Width
          else
            Result:=Viewport.Height;
        end;
        Result:=Result/100;
      end;
    fcu_cm: Result:=Viewport.DPI[IsHorizontal]/2.54;
    fcu_mm: Result:=Viewport.DPI[IsHorizontal]/25.4;
    fcu_ch,
    fcu_em,
    fcu_ex:
      begin
        // relative to font of element
        Result:=El.GetComputedCSSLength(fcaFontSize,true);
        // ToDo
        case anUnit of
        fcu_ch: Result:=Result/2;
        fcu_em: ;
        fcu_ex: ;
        end;
      end;
    fcu_in: Result:=Viewport.DPI[IsHorizontal];
    fcu_px: Result:=1.0;
    fcu_pt: Result:=Viewport.DPI[IsHorizontal]/72; // 1pt = 1/72 of 1in
    fcu_pc: Result:=Viewport.DPI[IsHorizontal]/6; // 1pc = 12 pt
    fcu_rem:
      // relative to font-size of the root element
      Result:=GetPixPerUnit(Viewport,fcu_em,IsHorizontal);
    fcu_vw,
    fcu_vh,
    fcu_vmax,
    fcu_vmin:
      begin
        case anUnit of
        fcu_vw: Result:=Viewport.Width/100; // relative to 1% of the width of the viewport
        fcu_vh: Result:=Viewport.Height/100; // relative to 1% of the height of the viewport
        fcu_vmax: // relative to 1% of viewport's larger dimension
          if Viewport.Width>Viewport.Height then
            Result:=Viewport.Width/100
          else
            Result:=Viewport.Height/100;
        fcu_vmin: // relative to 1% of viewport's smaller dimension
          if Viewport.Width>Viewport.Height then
            Result:=Viewport.Height/100
          else
            Result:=Viewport.Width/100;
        end;
      end;
  end;
end;

procedure TSimpleFresnelLayouter.ConvertCSSBorderWidthToPix(
  El: TFresnelElement; Attr: TFresnelCSSAttribute);
var
  aWidth: String;
begin
  aWidth:=El.CSSComputedAttribute[Attr];
  case aWidth of
  'thin': El.CSSComputedAttribute[Attr]:='0.5';
  'medium': El.CSSComputedAttribute[Attr]:='1';
  'thick': El.CSSComputedAttribute[Attr]:='2';
  end;
end;

procedure TSimpleFresnelLayouter.ConvertCSSValueToPixel(El: TFresnelElement;
  Attr: TFresnelCSSAttribute; IsHorizontal: boolean);
var
  aValue: String;
  StartP, p: PChar;
  aNumber: TFresnelLength;
begin
  aValue:=El.CSSComputedAttribute[Attr];
  if (AValue='auto') or (aValue='') then
    exit;
  StartP:=PChar(AValue);
  p:=StartP;
  if p^='-' then inc(p);
  while p^ in ['0'..'9'] do inc(p);
  if p^='.' then
  begin
    // float
    inc(p);
    while p^ in ['0'..'9'] do inc(p);
  end;
  if (p^ in ['e','E']) and (p[1] in ['-','0'..'9']) then
  begin
    // exponent
    inc(p);
    if p^='-' then
      inc(p);
    while p^ in ['0'..'9'] do inc(p);
  end;
  if not CSSStrToFloat(copy(aValue,1,p-StartP),aNumber) then
  begin
    El.CSSInvalidValueWarning(20221017003716,Attr,aValue);
    exit;
  end;

  // convert aNumber to pixels
  case p^ of
  '%': aNumber:=aNumber*GetPixPerUnit(El,fcuPercent,IsHorizontal);
  'c':
    case p[1] of
    'm': // cm  centimeters
      aNumber:=aNumber*GetPixPerUnit(El,fcu_cm,IsHorizontal);
    'h': // ch  relative to the width of the "0" (zero)
      aNumber:=aNumber*GetPixPerUnit(El,fcu_ch,IsHorizontal); // ToDo
    end;
  'e':
    case p[1] of
    'm': // em  relative to the font-size of the element
      aNumber:=aNumber*GetPixPerUnit(El,fcu_em,IsHorizontal);
    'x': // ex  relative to the x-height of the current font
      aNumber:=aNumber*GetPixPerUnit(El,fcu_ex,IsHorizontal);
    end;
  'i':
    if p[1]='n' then
      // in  inches
      aNumber:=aNumber*GetPixPerUnit(El,fcu_cm,IsHorizontal);
  'm':
    if p[1]='m' then
      // mm  centimeters
      aNumber:=aNumber*GetPixPerUnit(El,fcu_mm,IsHorizontal);
  'p':
    case p[1] of
    'x': ; // px  pixels
    't': // pt  points (1pt = 1/72 of 1in)
      aNumber:=aNumber*GetPixPerUnit(El,fcu_pt,IsHorizontal);
    'c': // pc  picas (1pc = 12 pt)
      aNumber:=aNumber*GetPixPerUnit(El,fcu_pc,IsHorizontal);
    end;
  'r':
    if (p[1]='e') and (p[2]='m') then
      // rem  relative to font-size of the root element
      aNumber:=aNumber*GetPixPerUnit(El,fcu_rem,IsHorizontal);
  'v':
    case p[1] of
    'w': // vw  relative to 1% of the width of the viewport
      aNumber:=aNumber*GetPixPerUnit(El,fcu_vw,IsHorizontal);
    'h': // vh  relative to 1% of the height of the viewport
      aNumber:=aNumber*GetPixPerUnit(El,fcu_vh,IsHorizontal);
    'm':
      case p[2] of
      'a': if p[3]='x' then  // vmax  relative to 1% of viewport's smaller dimension
        aNumber:=aNumber*GetPixPerUnit(El,fcu_vmax,IsHorizontal);
      'i': if p[3]='n' then  // vmin  relative to 1% of viewport's larger dimension
        aNumber:=aNumber*GetPixPerUnit(El,fcu_vmin,IsHorizontal);
      end;
    end;
  end;

  El.CSSComputedAttribute[Attr]:=FloatToCSSStr(aNumber);
  //writeln('TFresnelElement.ConvertCSSValueToPixel ',Attr,' ',FCSSComputed[Attr]);
end;

procedure TSimpleFresnelLayouter.ComputeCSS(El: TFresnelElement);
var
  Attr: TFresnelCSSAttribute;
  LNode, ParentLNode: TSimpleFresnelLayoutNode;
  aDisplayBox, aVisibility: String;
  {%H-}Code: integer;
begin
  writeln('TSimpleFresnelLayouter.ComputeCSS ',El.GetPath);
  // every node gets a layout ndoe
  LNode:=CreateLayoutNode(El);

  // inherit flags
  if El.Parent<>nil then
  begin
    ParentLNode:=TSimpleFresnelLayoutNode(El.Parent.LayoutNode);
    if ParentLNode.SkipLayout then
      LNode.SkipLayout:=true;
    if ParentLNode.SkipRendering then
      LNode.SkipRendering:=true;
  end
  else
    ParentLNode:=nil;

  // display-box
  aDisplayBox:=El.CSSComputedAttribute[fcaDisplayBox];
  if aDisplayBox='none' then
  begin
    LNode.SkipLayout:=true;
    LNode.SkipRendering:=true;
  end;

  // layouter
  UpdateLayouter(El,LNode);

  // visibility
  aVisibility:=El.CSSComputedAttribute[fcaVisibility];
  case aVisibility of
  'hidden','collapse':
    LNode.SkipRendering:=true;
  end;

  // block container
  LNode.BlockContainer:=GetBlockContainer(El);
  writeln('TSimpleFresnelLayouter.ComputeCSS ',El.Name,' BlockContainer=',DbgSName(LNode.BlockContainer));

  // LayoutParent
  UpdateLayoutParent(El,LNode);

  // convert non-pixel units to pixels
  ConvertCSSBorderWidthToPix(El,fcaBorderLeftWidth);
  ConvertCSSBorderWidthToPix(El,fcaBorderRightWidth);
  ConvertCSSBorderWidthToPix(El,fcaBorderTopWidth);
  ConvertCSSBorderWidthToPix(El,fcaBorderBottomWidth);

  for Attr in [
      fcaLeft,
      fcaRight,
      fcaWidth,
      fcaBorderLeftWidth,
      fcaBorderRightWidth,
      fcaMarginTop, // all margins are computed from the width
      fcaMarginBottom,
      fcaMarginLeft,
      fcaMarginRight,
      fcaMarginBlockEnd,
      fcaMarginBlockStart,
      fcaMarginInlineEnd,
      fcaMarginInlineStart,
      fcaMinWidth,
      fcaMaxWidth,
      fcaPaddingLeft,
      fcaPaddingRight,
      fcaPaddingTop,  // all padding are computed from the width
      fcaPaddingBottom] do
    ConvertCSSValueToPixel(El,Attr,true);
  for Attr in [
      fcaTop,
      fcaBottom,
      fcaHeight,
      fcaBorderTopWidth,
      fcaBorderBottomWidth,
      fcaLineHeight,
      fcaMinHeight,
      fcaMaxHeight] do
    ConvertCSSValueToPixel(El,Attr,false);
end;

procedure TSimpleFresnelLayouter.ComputedChildrenCSS(El: TFresnelElement);
var
  LNode: TSimpleFresnelLayoutNode;
begin
  LNode:=TSimpleFresnelLayoutNode(El.LayoutNode);
  if LNode.Layouter<>nil then
    LNode.Layouter.ComputedChildrenCSS;
end;

procedure TSimpleFresnelLayouter.SortStackingContext(LNode: TSimpleFresnelLayoutNode
  );

  function IsSorted: boolean;
  var
    NextNode, Node: TSimpleFresnelLayoutNode;
    i: Integer;
  begin
    if LNode.NodeCount<=1 then
      exit(true);
    i:=LNode.NodeCount-1;
    NextNode:=TSimpleFresnelLayoutNode(LNode.Nodes[i]);
    dec(i);
    repeat
      Node:=TSimpleFresnelLayoutNode(LNode.Nodes[i]);
      if (Node.ZIndex<NextNode.ZIndex) then
        // ok
      else if (Node.ZIndex>NextNode.ZIndex) then
        exit(false) // need sorting
      else if Node.Element.DOMIndex>NextNode.Element.DOMIndex then
        exit(false); // need sorting
      NextNode:=Node;
      dec(i);
    until i<0;
    Result:=true;
  end;

begin
  if IsSorted then exit;
  LNode.SortNodes(@CompareLayoutNodesZIndexDomIndex,nil);
  {$IFDEF VerboseFresnelLayouter}
  if not IsSorted then
    raise Exception.Create('20221031180116 TSimpleFresnelLayouter.SortLayoutNodes');
  {$ENDIF}
end;

procedure TSimpleFresnelLayouter.WriteLayoutTree;

  procedure WriteNode(const Prefix: string; Node: TSimpleFresnelLayoutNode);
  var
    i: Integer;
    El: TFresnelElement;
  begin
    El:=Node.Element;
    write(Prefix,El.Name,' NodeCount=',Node.NodeCount,' display-outside=',El.CSSComputedAttribute[fcaDisplayOutside],' display-inside=',El.CSSComputedAttribute[fcaDisplayInside],' position=',El.CSSComputedAttribute[fcaPosition],' z-index=',El.CSSComputedAttribute[fcaZIndex]);
    if Node.Layouter<>nil then
      write(' layouter=',Node.Layouter.ClassName);
    writeln;
    for i:=0 to Node.NodeCount-1 do
      WriteNode(Prefix+'  ',TSimpleFresnelLayoutNode(Node.Nodes[i]));
  end;

begin
  if Viewport=nil then
  begin
    writeln('TSimpleFresnelLayouter.WriteLayoutTree Viewport=nil');
    exit;
  end;
  if Viewport.LayoutNode=nil then
  begin
    writeln('TSimpleFresnelLayouter.WriteLayoutTree Viewport.LayoutNode=nil');
    exit;
  end;
  writeln('TSimpleFresnelLayouter.WriteLayoutTree BEGIN======================');
  WriteNode('',TSimpleFresnelLayoutNode(Viewport.LayoutNode));
  writeln('TSimpleFresnelLayouter.WriteLayoutTree END========================');
end;

end.

