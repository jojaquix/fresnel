unit utcbaseevents;

{$mode objfpc}
{$H+}
{$modeswitch functionreferences}
{$modeswitch nestedprocvars}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, basevents;

Const
  MaxEvents = 5;

type

  TEvent1 = Class(TAbstractEvent)
    class function EventName: TEventName; override;
  end;

  { TEvent2 }

  TEvent2 = Class(TAbstractEvent)
    class function EventName: TEventName; override;
  end;

  { TEvent3 }

  TEvent3 = Class(TAbstractEvent)
    class function EventName: TEventName; override;
  end;

  { TMyRegistry }

  TMyRegistry = class(TEventRegistry)
   class Function DefaultIDOffset: TEventID; override;
  end;

  { TCEventsRegistry }
  TCBaseEvents = class(TTestCase)
  Private
    FRegistry : TEventRegistry;
    FRegs : Array[1..3] of TEventID;
  Protected
    procedure Register(event : integer);
    procedure RegisterID(event : Integer; aID : Integer = -1);
    procedure Register1;
    procedure Register2;
    procedure Register2As1;
    procedure Register2As11;
    procedure Register3;
    Procedure RegisterAll;
    Procedure RegisterAllGlobally;
    Procedure EnableOffset;
    procedure SetUp; override;
    procedure TearDown; override;
    property Registry : TEventRegistry Read FRegistry;
  end;

  TCEventsRegistry = class(TCBaseEvents)
  protected
    procedure FindNonExistent;
    procedure FindNonExistentClass;
    procedure FindNonExistentClassByID;
    procedure FindNonExistentID;
  published
    procedure TestHookUp;
    procedure TestRegister;
    procedure TestRegisterDuplicate;
    procedure TestRegisterOffset;
    procedure TestRegisterWithID;
    procedure TestRegisterWithIDDUplicate;
    procedure TestRegisterWithIDOutOfRange;
    procedure TestFindEventID;
    procedure TestGetEventID;
    procedure TestFindEventName;
    procedure TestGetEventName;
    procedure TestFindEventClass;
    procedure TestGetEventClass;
    procedure TestFindEventClassByID;
    procedure TestGetEventClassByID;
    procedure TestUnregisterEventClass;
    procedure TestUnregisterEventID;
    procedure TestUnregisterEventName;
    Procedure TestClear;
    Procedure TestRegisterGLobally;
  end;


  { TCEventsDispatcher }
  THandlerType = (htObject,htProc,htRef);

  TCEventsDispatcher = class(TCBaseEvents)
  private
    Class var FHandlerCallCount : Array[THandlerType] of Integer;
    Class var FHandlerCallEvent : Array[1..MaxEvents,THandlerType] of TAbstractEvent;
    Class var FExpectedEvent : TAbstractEvent;

    class procedure RegisterEvent(aType: THandlerType; aEvent: TAbstractEvent);
    class procedure AssertCalled(const Msg: String; aType: THandlerType;
      aEvent: TAbstractEvent; aIndex, aCount: Integer);
  private
    FDispatcher: TEventDispatcher;
    FRHandler:TEventHandlerRef;
    FRHandler2:TEventHandlerRef;
    FEvents : Array[1..3] of TAbstractEvent;
    FSecondEvent : TAbstractEvent;

    Procedure RegisterEvent2P;
    Procedure RegisterEvent2R;
    Procedure RegisterEvent2O;
  protected
    Procedure SetUp; override;
    Procedure TearDown; override;
    Function CreateEvent(aID : Integer) : TAbstractEvent;
    Procedure EventHandlerO(aEvent : TAbstractEvent);
    Procedure EventHandlerO2(aEvent : TAbstractEvent);
    function RegisterHandlerO(aEventName: String): TEventHandlerItem;
    function RegisterHandlerO2(aEventName: String): TEventHandlerItem;
    function RegisterHandlerP(aEventName: String): TEventHandlerItem;
    function RegisterHandlerP2(aEventName: String): TEventHandlerItem;
    function RegisterHandlerR(aEventName: String): TEventHandlerItem;
    function RegisterHandlerR2(aEventName: String): TEventHandlerItem;
    Property Dispatcher : TEventDispatcher Read FDispatcher;
  Published
    Procedure TestHookup;
    Procedure TestRegisterHandlerO;
    Procedure TestRegisterHandlerR;
    Procedure TestRegisterHandlerP;
    Procedure TestRegisterHandlerOUnknown;
    Procedure TestRegisterHandlerRUnknown;
    Procedure TestRegisterHandlerPUnknown;
    Procedure TestUnRegisterHandlerO;
    Procedure TestUnRegisterHandlerR;
    Procedure TestUnRegisterHandlerP;
    Procedure TestUnRegisterHandlerOName;
    Procedure TestUnRegisterHandlerRName;
    Procedure TestUnRegisterHandlerPName;
    Procedure TestUnRegisterHandlerOUnknownEvent;
    Procedure TestUnRegisterHandlerRUnknownEvent;
    Procedure TestUnRegisterHandlerPUnknownEvent;
    Procedure TestUnRegisterHandlerOUnknownHandler;
    Procedure TestUnRegisterHandlerRUnknownHandler;
    Procedure TestUnRegisterHandlerPUnknownHandler;
    Procedure TestUnRegisterHandlerOAllName;
    Procedure TestUnRegisterHandlerRAllName;
    Procedure TestUnRegisterHandlerPAllName;
    Procedure TestUnRegisterHandlerMixedAllName;
    Procedure TestUnRegisterHandlerOAllHandler;
    Procedure TestUnRegisterHandlerRAllHandler;
    Procedure TestUnRegisterHandlerPAllHandler;
    Procedure TestCreateEventByName;
    Procedure TestCreateEventByID;
    Procedure TestDispatchEvent;
    Procedure TestDispatchEventProc;
    Procedure TestDispatchEventRef;
    Procedure TestDispatchEvent2Handlers;
    Procedure TestDispatchEvent2MixedHandlers;
    Procedure TestDispatchEventInEvent;
  end;

implementation

{ TCEventsDispatcher }

procedure TCEventsDispatcher.SetUp;

var
  H : THandlerType;
  I : Integer;

begin
  inherited SetUp;
  FRHandler:=Nil;
  FDispatcher:=TEventDispatcher.Create(Self);
  FDispatcher.Registry:=Self.Registry;
  For H in THandlerType do
    begin
    FHandlerCallCount[H]:=0;
    For I:=1 to MaxEvents do
    FHandlerCallEVent[i,H]:=Nil;
    end;
end;

procedure TCEventsDispatcher.TearDown;

var
  I : Integer;
begin
  FRHandler:=Nil;
  FreeAndNil(FDispatcher);
  for I:=1 to 3 do
    FreeAndNil(FEvents[i]);
  inherited TearDown;
end;

function TCEventsDispatcher.CreateEvent(aID: Integer): TAbstractEvent;
begin
  if Assigned(FEvents[aID]) then
    Fail('Event %d already created',[aID]);
  FEvents[aID]:=Dispatcher.CreateEvent(Self,aID);
  Result:=FEvents[aID];
end;

class procedure TCEventsDispatcher.RegisterEvent(aType: THandlerType;
  aEvent: TAbstractEvent);

begin
  Inc(FHandlerCallCount[aType]);
  if FHandlerCallCount[aType]>MaxEvents then
    Fail('Max number of recursive events reached');
  FHandlerCallEvent[FHandlerCallCount[aType],aType]:=aEvent;
  if (FExpectedEvent<>Nil) then
    AssertSame('Correct event object registered',FExpectedEvent,aEvent);
end;

class procedure TCEventsDispatcher.AssertCalled(const Msg: String;
  aType: THandlerType; aEvent: TAbstractEvent; aIndex, aCount: Integer);
begin
  AssertEquals(Msg+'Correct handler count',aCount,FHandlerCallCount[aType]);
  AssertSame(Msg+' Correct event passed',aEvent,FHandlerCallEvent[aIndex,aType]);
end;

procedure TCEventsDispatcher.RegisterEvent2P;
begin
  RegisterHandlerP('event2');
end;

procedure TCEventsDispatcher.RegisterEvent2R;
begin
  RegisterHandlerR('event2');
end;

procedure TCEventsDispatcher.RegisterEvent2O;
begin
  RegisterHandlerO('event2');
end;

procedure TCEventsDispatcher.EventHandlerO(aEvent: TAbstractEvent);
begin
  RegisterEvent(htObject,aEvent);
  if Assigned(FSecondEvent) then
    begin
    FExpectedEvent:=FSecondEVent;
    Dispatcher.DispatchEvent(FSecondEvent);
    end;
end;

procedure TCEventsDispatcher.EventHandlerO2(aEvent: TAbstractEvent);
begin
  RegisterEvent(htObject,aEvent);
end;

function TCEventsDispatcher.RegisterHandlerO(aEventName: String): TEventHandlerItem;
begin
  Result:=Dispatcher.RegisterHandler(@EventHandlerO,aEventName);
end;

function TCEventsDispatcher.RegisterHandlerO2(aEventName: String
  ): TEventHandlerItem;
begin
  Result:=Dispatcher.RegisterHandler(@EventHandlerO2,aEventName);
end;

Procedure EventHandlerP(aEvent : TAbstractEVent);

begin
  TCEventsDispatcher.RegisterEvent(htProc,aEvent);
end;

Procedure EventHandlerP2(aEvent : TAbstractEVent);

begin
  TCEventsDispatcher.RegisterEvent(htProc,aEvent);
end;

function TCEventsDispatcher.RegisterHandlerP(aEventName: String): TEventHandlerItem;
begin
  Result:=Dispatcher.RegisterHandler(@EventHandlerP,aEventName);
end;

function TCEventsDispatcher.RegisterHandlerP2(aEventName: String
  ): TEventHandlerItem;
begin
  Result:=Dispatcher.RegisterHandler(@EventHandlerP2,aEventName);
end;

function TCEventsDispatcher.RegisterHandlerR(aEventName: String) : TEventHandlerItem;

  Procedure EventHandlerR(aEvent : TAbstractEVent);

  begin
    RegisterEvent(htRef,aEvent);
  end;

begin
  FRHandler:=@EventHandlerR;
  Result:=Dispatcher.RegisterHandler(FRHandler,aEventName);
end;

function TCEventsDispatcher.RegisterHandlerR2(aEventName: String
  ): TEventHandlerItem;

  Procedure EventHandlerR2(aEvent : TAbstractEVent);

  begin
    RegisterEvent(htRef,aEvent);
  end;

begin
  FRHandler2:=@EventHandlerR2;
  Result:=Dispatcher.RegisterHandler(FRHandler,aEventName);
end;

procedure TCEventsDispatcher.TestHookup;
begin
  AssertNotNull('Dispatcher',Dispatcher);
  AssertEquals('No handlers',0,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestRegisterHandlerO;

Var
  Itm : TEventHandlerItem;

begin
  RegisterAll;
  Itm:=RegisterHandlerO('event1');
  AssertNotNull('Register returns item',Itm);
  AssertEquals('Event ID',1,Itm.EventID);
  AssertEquals('Event name','event1',Itm.EventName);
  AssertEquals('Count',1,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestRegisterHandlerR;
Var
  Itm : TEventHandlerItem;

begin
  RegisterAll;
  Itm:=RegisterHandlerR('event1');
  AssertNotNull('Register returns item',Itm);
  AssertEquals('Event ID',1,Itm.EventID);
  AssertEquals('Event name','event1',Itm.EventName);
  AssertEquals('Count',1,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestRegisterHandlerP;
Var
  Itm : TEventHandlerItem;

begin
  RegisterAll;
  Itm:=RegisterHandlerP('event1');
  AssertNotNull('Register returns item',Itm);
  AssertEquals('Event ID',1,Itm.EventID);
  AssertEquals('Event name','event1',Itm.EventName);
  AssertEquals('Count',1,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestRegisterHandlerOUnknown;
begin
  Register1;
  AssertException('Not known',EEvents,@RegisterEvent2O,'Unknown event name: event2');
end;

procedure TCEventsDispatcher.TestRegisterHandlerRUnknown;
begin
  Register1;
  AssertException('Not known',EEvents,@RegisterEvent2R,'Unknown event name: event2');
end;

procedure TCEventsDispatcher.TestRegisterHandlerPUnknown;
begin
  Register1;
  AssertException('Not known',EEvents,@RegisterEvent2P,'Unknown event name: event2');
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerO;

Var
  Itm : TEventHandlerItem;

begin
  Register1;
  Itm:=RegisterHandlerO('event1');
  AssertNotNull('Register returns item',Itm);
  AssertEquals('Dispatcher count',1,Dispatcher.Count);
  Dispatcher.UnregisterHandler(Itm);
  AssertEquals('Dispatcher count',0,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerR;
Var
  Itm : TEventHandlerItem;

begin
  Register1;
  Itm:=RegisterHandlerR('event1');
  AssertNotNull('Register returns item',Itm) ;
  AssertEquals('Dispatcher count',1,Dispatcher.Count);
  Dispatcher.UnregisterHandler(Itm);
  AssertEquals('Dispatcher count',0,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerP;

Var
  Itm : TEventHandlerItem;

begin
  Register1;
  Itm:=RegisterHandlerP('event1');
  AssertNotNull('Register returns item',Itm) ;
  AssertEquals('Dispatcher count',1,Dispatcher.Count);
  Dispatcher.UnregisterHandler(Itm);
  AssertEquals('Dispatcher count',0,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerOName;

Var
  Itm : TEventHandlerItem;

begin
  Register1;
  Itm:=RegisterHandlerO('event1');
  AssertNotNull('Register returns item',Itm);

  AssertEquals('Dispatcher count',1,Dispatcher.Count);
  Dispatcher.UnregisterHandler(@EventHandlerO,'event1');
  AssertEquals('Dispatcher count',0,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerRName;
Var
  Itm : TEventHandlerItem;

begin
  Register1;
  Itm:=RegisterHandlerR('event1');
  AssertNotNull('Register returns item',Itm);
  AssertEquals('Dispatcher count',1,Dispatcher.Count);
  Dispatcher.UnregisterHandler(FRHandler,'event1');
  AssertEquals('Dispatcher count',0,Dispatcher.Count);

end;

procedure TCEventsDispatcher.TestUnRegisterHandlerPName;

Var
  Itm : TEventHandlerItem;

begin
  Register1;
  Itm:=RegisterHandlerP('event1');
  AssertNotNull('Register returns item',Itm);
  AssertEquals('Dispatcher count',1,Dispatcher.Count);
  Dispatcher.UnregisterHandler(@EventHandlerP,'event1');
  AssertEquals('Dispatcher count',0,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerOUnknownEvent;

Var
  Itm : TEventHandlerItem;

begin
  Register1;
  Register2;
  Itm:=RegisterHandlerO('event1');
  AssertNotNull('Register returns item',Itm);
  AssertEquals('Dispatcher count',1,Dispatcher.Count);
  Dispatcher.UnregisterHandler(@EventHandlerO,'event2');
  AssertEquals('Dispatcher count',1,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerRUnknownEvent;
Var
  Itm : TEventHandlerItem;

begin
  Register1;
  Register2;
  Itm:=RegisterHandlerR('event1');
  AssertNotNull('Register returns item',Itm);

  AssertEquals('Dispatcher count',1,Dispatcher.Count);
  Dispatcher.UnregisterHandler(FRHandler,'event2');
  AssertEquals('Dispatcher count',1,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerPUnknownEvent;


begin
  Register1;
  Register2;
  RegisterHandlerP('event1');
  AssertEquals('Dispatcher count before',1,Dispatcher.Count);
  Dispatcher.UnregisterHandler(@EventHandlerP,'event2');
  AssertEquals('Dispatcher count after',1,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerOUnknownHandler;

begin
  Register1;
  Register2;
  RegisterHandlerO('event1');
  RegisterHandlerO2('event2');
  AssertEquals('Dispatcher count before',2,Dispatcher.Count);
  Dispatcher.UnregisterHandler(@EventHandlerO2,'event1');
  AssertEquals('Dispatcher count after',2,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerRUnknownHandler;

begin
  Register1;
  Register2;
  RegisterHandlerR('event1');
  RegisterHandlerR2('event2');
  AssertEquals('Dispatcher count before',2,Dispatcher.Count);
  Dispatcher.UnregisterHandler(FRHandler2,'event1');
  AssertEquals('Dispatcher count after',2,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerPUnknownHandler;
Var
  Itm : TEventHandlerItem;

begin
  Register1;
  Register2;
  Itm:=RegisterHandlerP('event1');
  AssertNotNull('Register returns item',Itm);
  Itm:=RegisterHandlerP2('event2');
  AssertNotNull('Register 2 returns item',Itm);
  AssertEquals('Dispatcher count before',2,Dispatcher.Count);
  Dispatcher.UnregisterHandler(@EventHandlerP2,'event1');
  AssertEquals('Dispatcher count after',2,Dispatcher.Count);


end;

procedure TCEventsDispatcher.TestUnRegisterHandlerOAllName;

begin
  Register1;
  Register2;
  RegisterHandlerP('event1');
  RegisterHandlerP2('event1');
  AssertEquals('Dispatcher count before',2,Dispatcher.Count);
  Dispatcher.UnregisterHandler('event1');
  AssertEquals('Dispatcher count after',0,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerRAllName;
begin
  Register1;
  Register2;
  RegisterHandlerR('event1');
  RegisterHandlerR2('event1');
  AssertEquals('Dispatcher count before',2,Dispatcher.Count);
  Dispatcher.UnregisterHandler('event1');
  AssertEquals('Dispatcher count after',0,Dispatcher.Count);

end;

procedure TCEventsDispatcher.TestUnRegisterHandlerPAllName;
begin
  Register1;
  Register2;
  RegisterHandlerP('event1');
  RegisterHandlerP2('event1');
  AssertEquals('Dispatcher count before',2,Dispatcher.Count);
  Dispatcher.UnregisterHandler('event1');
  AssertEquals('Dispatcher count after',0,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerMixedAllName;
begin
  Register1;
  Register2;
  RegisterHandlerP('event1');
  RegisterHandlerO2('event1');
  AssertEquals('Dispatcher count before',2,Dispatcher.Count);
  Dispatcher.UnregisterHandler('event1');
  AssertEquals('Dispatcher count after',0,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerOAllHandler;
begin
  Register1;
  Register2;
  RegisterHandlerO('event1');
  RegisterHandlerO('event2');
  AssertEquals('Dispatcher count before',2,Dispatcher.Count);
  Dispatcher.UnregisterHandler(@EventHandlerO);
  AssertEquals('Dispatcher count after',0,Dispatcher.Count);

end;

procedure TCEventsDispatcher.TestUnRegisterHandlerRAllHandler;
begin
  Register1;
  Register2;
  RegisterHandlerR('event1');
  Dispatcher.RegisterHandler(FRHandler,'event2');
  AssertEquals('Dispatcher count before',2,Dispatcher.Count);
  Dispatcher.UnregisterHandler(FRHandler);
  AssertEquals('Dispatcher count after',0,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestUnRegisterHandlerPAllHandler;
begin
  Register1;
  Register2;
  RegisterHandlerP('event1');
  RegisterHandlerP('event2');
  AssertEquals('Dispatcher count before',2,Dispatcher.Count);
  Dispatcher.UnregisterHandler(@EventHandlerP);
  AssertEquals('Dispatcher count after',0,Dispatcher.Count);
end;

procedure TCEventsDispatcher.TestCreateEventByName;
var
  E : TAbstractEvent;
begin
  Register1;
  E:=Dispatcher.CreateEvent(Self,'event1');
  AssertEquals('Correct class',TEvent1,E.ClassType);
  AssertSame('Correct sender',Self,E.Sender);
  AssertEquals('Event ID',1,E.EventID);
  E.Free;
end;

procedure TCEventsDispatcher.TestCreateEventByID;
var
  E : TAbstractEvent;
begin
  Register1;
  E:=Dispatcher.CreateEvent(Self,1);
  AssertEquals('Correct class',TEvent1,E.ClassType);
  AssertSame('Correct sender',Self,E.Sender);
  E.Free;
end;

procedure TCEventsDispatcher.TestDispatchEvent;

Var
  Evt : TAbstractEvent;
  aCount : Integer;

begin
  Register1;
  Register2;
  Evt:=CreateEvent(1);
  RegisterHandlerO('event1');
  RegisterHandlerO2('event2');
  FExpectedEvent:=Evt;
  aCOunt:=Dispatcher.DispatchEvent(Evt);
  AssertEquals('Count',1,aCount);
  AssertCalled('Event handler called',htObject,Evt,1,1);
end;

procedure TCEventsDispatcher.TestDispatchEventProc;

Var
  Evt : TAbstractEvent;
  aCount : integer;

begin
  Register1;
  Register2;
  Evt:=CreateEvent(1);
  RegisterHandlerP('event1');
  RegisterHandlerP2('event2');
  FExpectedEvent:=Evt;
  aCount:=Dispatcher.DispatchEvent(Evt);
  AssertEquals('Count',1,aCount);
  AssertCalled('Event handler called',htProc,Evt,1,1);
end;

procedure TCEventsDispatcher.TestDispatchEventRef;

Var
  Evt : TAbstractEvent;
  aCount : integer;

begin
  Register1;
  Register2;
  Evt:=CreateEvent(1);
  RegisterHandlerR('event1');
  RegisterHandlerR2('event2');
  FExpectedEvent:=Evt;
  aCount:=Dispatcher.DispatchEvent(Evt);
  AssertEquals('Count',1,aCount);
  AssertEquals('Count',1,aCount);
  AssertCalled('Event handler called',htRef,Evt,1,1);

end;

procedure TCEventsDispatcher.TestDispatchEvent2Handlers;
Var
  Evt : TAbstractEvent;
  aCount : integer;

begin
  Register1;
  Register2;
  Evt:=CreateEvent(1);
  RegisterHandlerO('event1');
  RegisterHandlerO2('event1');
  FExpectedEvent:=Evt;
  aCount:=Dispatcher.DispatchEvent(Evt);
  AssertEquals('Count',2,aCount);

  AssertCalled('Event handler called',htObject,Evt,1,2);
  AssertCalled('Event handler called',htObject,Evt,2,2);
end;

procedure TCEventsDispatcher.TestDispatchEvent2MixedHandlers;
Var
  Evt : TAbstractEvent;
  aCount :Integer;

begin
  Register1;
  Register2;
  Evt:=CreateEvent(1);
  RegisterHandlerO('event1');
  RegisterHandlerP('event1');
  FExpectedEvent:=Evt;
  aCount:=Dispatcher.DispatchEvent(Evt);
  AssertEquals('Count',2,aCount);
  AssertCalled('Event handler called',htObject,Evt,1,1);
  AssertCalled('Event handler called',htProc,Evt,1,1);

end;

procedure TCEventsDispatcher.TestDispatchEventInEvent;

Var
  Evt,Evt2 : TAbstractEvent;
  aCount : Integer;

begin
  Register1;
  Register2;
  Evt:=CreateEvent(1);
  Evt2:=CreateEvent(2);
  RegisterHandlerO('event1');
  RegisterHandlerO2('event2');
  FExpectedEvent:=Evt;
  FSecondEvent:=Evt2;
  aCount:=Dispatcher.DispatchEvent(Evt);
  AssertEquals('Event handler count',1,aCount);
  AssertCalled('Event handler 1 called',htObject,Evt,1,2);
  AssertCalled('Event handler 2 called',htObject,Evt2,2,2);
end;

{ TMyRegistry }

class function TMyRegistry.DefaultIDOffset: TEventID;
begin
  Result:=10;
end;

{ TEvent3 }

class function TEvent3.EventName: TEventName;
begin
  Result:='event3';
end;

{ TEvent2 }

class function TEvent2.EventName: TEventName;
begin
  Result:='event2';
end;

{ TEvent1 }

class function TEvent1.EventName: TEventName;
begin
  Result:='event1';
end;

procedure TCBaseEvents.Register(event: integer);
begin
  case event of
  1 : FRegs[1]:=Registry.RegisterEvent(TEvent1);
  2 : FRegs[2]:=Registry.RegisterEvent(TEvent2);
  3 : FRegs[3]:=Registry.RegisterEvent(TEvent3);
  end;
end;

procedure TCBaseEvents.RegisterID(event: Integer; aID: Integer);
begin
  if aID=-1 then aID:=event;
  case event of
    1 : FRegs[1]:=Registry.RegisterEventWithID(aID,TEvent1);
    2 : FRegs[2]:=Registry.RegisterEventWithID(aID,TEvent2);
    3 : FRegs[3]:=Registry.RegisterEventWithID(aID,TEvent3);
  end;
end;

procedure TCBaseEvents.Register1;
begin
  Register(1);
end;

procedure TCBaseEvents.Register2;
begin
  Register(2);
end;

procedure TCBaseEvents.Register2As1;
begin
  RegisterID(2,1);
end;

procedure TCBaseEvents.Register2As11;
begin
  RegisterID(2,11);
end;

procedure TCBaseEvents.Register3;
begin
  Register(3);
end;

procedure TCBaseEvents.RegisterAll;
begin
  Register1;
  Register2;
  Register3;
end;

procedure TCBaseEvents.RegisterAllGlobally;
begin
  FRegs[1]:=TEvent1.Register;
  FRegs[2]:=TEvent2.Register;
  FRegs[3]:=TEvent3.Register;
end;

procedure TCBaseEvents.EnableOffset;
begin
  FreeAndNil(Fregistry);
  FRegistry:=TMyRegistry.Create;
end;

procedure TCBaseEvents.SetUp;
begin
  FRegistry:=TEventRegistry.Create;
end;

procedure TCBaseEvents.TearDown;
begin
  FreeAndNil(FRegistry);
  TEventRegistry.Instance.Clear;
end;

procedure TCEventsRegistry.TestHookUp;
begin
  AssertNotNull(FRegistry);
  AssertEquals('No events',0,Registry.GetRegisteredEventCount);
end;

procedure TCEventsRegistry.TestRegister;
begin
  Register(1);
  AssertEquals('Event1',1,FRegs[1]);
  Register(2);
  AssertEquals('Event2',2,FRegs[2]);
  Register(3);
  AssertEquals('Event3',3,FRegs[3]);
end;

procedure TCEventsRegistry.TestRegisterDuplicate;
begin
  Register1;
  AssertException('Cannot register same name twice',EEvents,@Register1);
end;

procedure TCEventsRegistry.TestRegisterOffset;
begin
  EnableOffset;
  Register(1);
  AssertEquals('Event1',11,FRegs[1]);
  Register(2);
  AssertEquals('Event2',12,FRegs[2]);
  Register(3);
  AssertEquals('Event3',13,FRegs[3]);
end;

procedure TCEventsRegistry.TestRegisterWithID;
begin
  EnableOffset;
  RegisterID(1,3);
  AssertEquals('Event1',3,FRegs[1]);
end;

procedure TCEventsRegistry.TestRegisterWithIDDUplicate;
begin
  EnableOffset;
  RegisterID(1,1);
  AssertException('Duplicate with Event1',EEvents,@Register2As1);

end;

procedure TCEventsRegistry.TestRegisterWithIDOutOfRange;
begin
  EnableOffset;
  RegisterID(1,1);
  AssertException('ID out of allowed range',EEvents,@Register2as11)
end;

procedure TCEventsRegistry.TestFindEventID;
begin
  RegisterAll;
  AssertEquals('Event 1',FRegs[1],Registry.FindEventID('event1'));
  AssertEquals('Event 2',FRegs[2],Registry.FindEventID('event2'));
  AssertEquals('Event 3',FRegs[3],Registry.FindEventID('event3'));
  AssertEquals('Nonexisting event',0,Registry.FindEventID('event4'));

end;

procedure TCEventsRegistry.TestGetEventID;
begin
  RegisterAll;
  AssertEquals('Event 1',FRegs[1],Registry.GetEventID('event1'));
  AssertEquals('Event 2',FRegs[2],Registry.GetEventID('event2'));
  AssertEquals('Event 3',FRegs[3],Registry.GetEventID('event3'));
  AssertException('Nonexisting event',EEvents,@FindNonExistent);

end;

procedure TCEventsRegistry.TestFindEventName;
begin
  RegisterAll;
  AssertEquals('Event 1','event1',Registry.GetEventName(FRegs[1]));
  AssertEquals('Event 2','event2',Registry.GetEventName(FRegs[2]));
  AssertEquals('Event 3','event3',Registry.GetEventName(FRegs[3]));
  AssertException('Nonexisting event',EEvents,@FindNonExistentID);
end;

procedure TCEventsRegistry.TestGetEventName;
begin
  RegisterAll;
  AssertEquals('Event 1','event1',Registry.FindEventName(FRegs[1]));
  AssertEquals('Event 2','event2',Registry.FindEventName(FRegs[2]));
  AssertEquals('Event 3','event3',Registry.FIndEventName(FRegs[3]));
  AssertEquals('Nonexisting event','',Registry.FIndEventName(4));
end;

procedure TCEventsRegistry.TestFindEventClass;
begin
  RegisterAll;
  AssertEquals('Event 1',TEvent1,Registry.FindEventClass('event1'));
  AssertEquals('Event 2',TEvent2,Registry.FindEventClass('event2'));
  AssertEquals('Event 3',TEvent3,Registry.FindEventClass('event3'));
  AssertNull('Nonexisting event',Registry.FindEventClass('event4'));

end;

procedure TCEventsRegistry.TestGetEventClass;
begin
  RegisterAll;
  AssertEquals('Event 1',TEvent1,Registry.GetEventClass('event1'));
  AssertEquals('Event 2',TEvent2,Registry.GetEventClass('event2'));
  AssertEquals('Event 3',TEvent3,Registry.GetEventClass('event3'));
  AssertException('Nonexisting event',EEvents,@FindNonExistentClass);
end;

procedure TCEventsRegistry.TestFindEventClassByID;
begin
  RegisterAll;
  AssertEquals('Event 1',TEvent1,Registry.FindEventClass(FRegs[1]));
  AssertEquals('Event 2',TEvent2,Registry.FindEventClass(FRegs[2]));
  AssertEquals('Event 3',TEvent3,Registry.FindEventClass(FRegs[3]));
  AssertNull('Nonexisting event',Registry.FindEventClass(122));

end;

procedure TCEventsRegistry.TestGetEventClassByID;
begin
  RegisterAll;
  AssertEquals('Event 1',TEvent1,Registry.GetEventClass(FRegs[1]));
  AssertEquals('Event 2',TEvent2,Registry.GetEventClass(FRegs[2]));
  AssertEquals('Event 3',TEvent3,Registry.GetEventClass(FRegs[3]));
  AssertException('Nonexisting event',EEvents,@FindNonExistentClassByID);

end;

procedure TCEventsRegistry.TestUnregisterEventClass;
begin
  RegisterAll;
  Registry.UnRegisterEvent(TEVent1);
  AssertEquals('Not found',0,Registry.FindEventID('event1'));
  Register1;
end;

procedure TCEventsRegistry.TestUnregisterEventID;
begin
  RegisterAll;
  Registry.UnRegisterEvent(FRegs[1]);
  AssertEquals('Not found',0,Registry.FindEventID('event1'));
  Register1;
end;

procedure TCEventsRegistry.TestUnregisterEventName;
begin
  RegisterAll;
  Registry.UnRegisterEvent('event1');
  AssertEquals('Not found',0,Registry.FindEventID('event1'));
  Register1;
end;

procedure TCEventsRegistry.TestClear;
begin
  RegisterAll;
  Registry.Clear;
  AssertNull('Nonexisting event1',Registry.FindEventClass(1));
  AssertNull('Nonexisting event2',Registry.FindEventClass(2));
  AssertNull('Nonexisting event3',Registry.FindEventClass(3));
  TestRegister;
end;

procedure TCEventsRegistry.TestRegisterGLobally;
begin
  RegisterAllGlobally;
  AssertEquals('Event 1',TEvent1,TEventRegistry.Instance.FindEventClass(FRegs[1]));
  AssertEquals('Event 2',TEvent2,TEventRegistry.Instance.FindEventClass(FRegs[2]));
  AssertEquals('Event 3',TEvent3,TEventRegistry.Instance.FindEventClass(FRegs[3]));
end;

procedure TCEventsRegistry.FindNonExistent;

begin
  Registry.GetEventID('event4');
end;

procedure TCEventsRegistry.FindNonExistentClass;
begin
  Registry.GetEventClass('event4');
end;

procedure TCEventsRegistry.FindNonExistentClassByID;
begin
  Registry.GetEventClass(4);
end;

procedure TCEventsRegistry.FindNonExistentID;
begin
  Registry.GetEventName(4);
end;

initialization
  RegisterTests([TCEventsRegistry,TCEventsDispatcher]);
end.

