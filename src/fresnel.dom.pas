{
 *****************************************************************************
  This file is part of Fresnel.

  See the file COPYING.modifiedLGPL.txt, included in this distribution,
  for details about the license.
 *****************************************************************************

ToDo:
- speed up GetCSSIndex
- speed up GetCSSDepth
- speed up GetCSSNextOfType
- speed up GetCSSPreviousOfType
- @font-face
- @media all
- @media only screen and (max-width: 711px)
- @media screen and (-webkit-min-device-pixel-ratio: 0)
}
unit Fresnel.DOM;

{$mode ObjFPC}{$H+}
{$WARN 6060 off} // Case statement does not handle all possible cases
{$ModeSwitch AdvancedRecords}

interface

uses
  Classes, SysUtils, Math, Types, FPImage, sortbase,
  fpCSSResolver, fpCSSTree, fpCSSParser, fcl.events, fresnel.events,
  LazLoggerBase;

type
  { EFresnel }

  EFresnel = class(Exception)
  end;

  TFresnelLength = double;
  TArray4FresnelLength =  array[0..3] of TFresnelLength;

  { TFresnelPoint }

  TFresnelPoint = packed record
    X, Y: TFresnelLength;
    class function PointInCircle(const aPoint, aCenter: TFresnelPoint; const aRadius: TFresnelLength): Boolean; static; inline;
    class function Zero: TFresnelPoint; static; inline;
    class operator + (const p1, p2: TFresnelPoint): TFresnelPoint;
    class operator - (const p1, p2: TFresnelPoint): TFresnelPoint;
    class operator <> (const p1, p2: TFresnelPoint): Boolean;
    class operator = (const p1, p2: TFresnelPoint): Boolean;
    function Add(const p: TFresnelPoint): TFresnelPoint;
    function Angle(const p: TFresnelPoint): TFresnelLength;
    function Distance(const p: TFresnelPoint): TFresnelLength;
    function IsZero: Boolean;
    function Subtract(const p: TFresnelPoint): TFresnelPoint;
    procedure Offset(const dx,dy: TFresnelLength);
    procedure Offset(const p: TFresnelPoint);
    procedure SetLocation(const ax, ay: TFresnelLength);
    procedure SetLocation(const p: TFresnelPoint);
  end;

  { TFresnelRect }

  TFresnelRect = packed record
    procedure Clear;
    constructor Create(const ALeft, ATop, ARight, ABottom: TFresnelLength);
    class operator = (const L, R: TFresnelRect): Boolean;
    class operator <> (const L, R: TFresnelRect): Boolean;
    class operator + (const L, R: TFresnelRect): TFresnelRect; // union
    class operator * (const L, R: TFresnelRect): TFresnelRect; // intersection
    class function Empty: TFresnelRect; static;
    procedure NormalizeRect;
    function IsEmpty: Boolean;
    function Contains(const P: TFresnelPoint): Boolean;
    function Contains(const R: TFresnelRect): Boolean;
    function Contains(const ax, ay: TFresnelLength): Boolean;
    function IntersectsWith(const R: TFresnelRect): Boolean;
    class function Intersect(const R1, R2: TFresnelRect): TFresnelRect; static;
    procedure Intersect(const R: TFresnelRect);
    class function Union(const R1, R2: TFresnelRect): TFresnelRect; static;
    procedure Union(const R: TFresnelRect);
    class function Union(const Points: array of TFresnelPoint): TFresnelRect; static;
    procedure Offset(const DX, DY: TFresnelLength);
    procedure Offset(const DP: TFresnelPoint);
    procedure SetLocation(const X, Y: TFresnelLength);
    procedure SetLocation(const P: TFresnelPoint);
    procedure Inflate(const DX, DY: TFresnelLength);
    procedure Inflate(const DL, DT, DR, DB: TFresnelLength);
    function CenterPoint: TFresnelPoint;
  public
    case Longint of
      0: (Left, Top, Right, Bottom: TFresnelLength);
      1: (TopLeft, BottomRight: TFresnelPoint);
      2: (Vector: TArray4FresnelLength);
  end;

  { EFresnelFont }

  EFresnelFont = class(EFresnel)
  end;

const
  MaxFresnelLength = TFresnelLength(high(longint));
  FresnelDefaultDPI = 96;

type
  // CSS attributes of the TFresnelElement class
  TFresnelCSSAttribute = (
    fcaDisplay,
    fcaDisplayBox,
    fcaDisplayInside,
    fcaDisplayOutside,
    fcaPosition,
    fcaOverflow, // x y
    fcaOverflowX, // visible|hidden|clip|scroll|auto
    fcaOverflowY,
    fcaZIndex,
    fcaClear,
    fcaDirection,
    fcaLeft,
    fcaTop,
    fcaRight,
    fcaBottom,
    fcaWidth,
    fcaHeight,
    fcaBorder, // shorthand
    fcaBorderLeft, // shorthand
    fcaBorderRight, // shorthand
    fcaBorderTop, // shorthand
    fcaBorderBottom, // shorthand
    fcaBorderWidth, // shorthand
    fcaBorderLeftWidth,
    fcaBorderRightWidth,
    fcaBorderTopWidth,
    fcaBorderBottomWidth,
    fcaBorderColor,
    fcaBoxSizing,
    fcaFloat,
    fcaFont, // shorthand
    fcaFontFamily,
    fcaFontFeatureSettings,
    fcaFontKerning, // auto|normal|none
    fcaFontSize,  // medium|xx-small|x-small|small|large|x-large|xx-large|smaller|larger|LengthUnit|%
    fcaFontStyle, // normal|italic|oblique
    fcaFontWeight, // normal|bold|bolder|lighter|number
    fcaFontVariant,
    fcaLineHeight,
    fcaMargin, // shorthand
    fcaMarginLeft,
    fcaMarginTop,
    fcaMarginRight,
    fcaMarginBottom,
    fcaMarginBlock, // shorthand
    fcaMarginBlockEnd,
    fcaMarginBlockStart,
    fcaMarginInline,
    fcaMarginInlineEnd,
    fcaMarginInlineStart,
    fcaMinWidth,
    fcaMaxWidth,
    fcaMinHeight,
    fcaMaxHeight,
    fcaOpacity,
    fcaPadding, // shorthand
    fcaPaddingLeft,
    fcaPaddingTop,
    fcaPaddingRight,
    fcaPaddingBottom,
    fcaVisibility,
    fcaBackgroundColor,
    fcaColor
    );
  TFresnelCSSAttributes = set of TFresnelCSSAttribute;

const
  FresnelCSSAttributeNames: array[TFresnelCSSAttribute] of string = (
    // case sensitive!
    'display',
    'display-box',
    'display-inside',
    'display-outside',
    'position',
    'overflow',
    'overflow-x',
    'overflow-y',
    'z-index',
    'clear',
    'direction',
    'left',
    'top',
    'right',
    'bottom',
    'width',
    'height',
    'border',
    'border-left',
    'border-right',
    'border-top',
    'border-bottom',
    'border-width',
    'border-left-width',
    'border-right-width',
    'border-top-width',
    'border-bottom-width',
    'border-color',
    'box-sizing',
    'float',
    'font',
    'font-family',
    'font-feature-settings',
    'font-kerning',
    'font-size',
    'font-style',
    'font-weight',
    'font-variant',
    'line-height',
    'margin',
    'margin-left',
    'margin-top',
    'margin-right',
    'margin-bottom',
    'margin-block',
    'margin-block-end',
    'margin-block-start',
    'margin-inline',
    'margin-inline-end',
    'margin-inline-start',
    'min-width',
    'max-width',
    'min-height',
    'max-height',
    'opacity',
    'padding',
    'padding-left',
    'padding-top',
    'padding-right',
    'padding-bottom',
    'visibility',
    'background-color',
    'color'
    );

type
  TFresnelCSSUnit = (
    fcuPercent, // % relative to 1% of the containing block's width/height
    fcu_cm, // centimeters
    fcu_mm, // milimeters
    fcu_ch, // relative to the width of the "0" (zero)
    fcu_em, // relative to the font-size of the element
    fcu_ex, // relative to the x-height of the current font
    fcu_in, // inches
    fcu_px, // pixels
    fcu_pt, // points (1pt = 1/72 of 1in)
    fcu_pc, // picas (1pc = 12 pt)
    fcu_rem,// relative to font-size of the root element
    fcu_vw, // relative to 1% of the width of the viewport
    fcu_vh, // relative to 1% of the height of the viewport
    fcu_vmax,// relative to 1% of viewport's larger dimension
    fcu_vmin // relative to 1% of viewport's smaller dimension
    );
  TFresnelCSSUnits = set of TFresnelCSSUnit;

type
  TFresnelCSSPseudo = (
    fcpaLang
    );
  TFresnelCSSPseudoAttributes = set of TFresnelCSSPseudo;

const
  FresnelCSSPseudoNames: array[TFresnelCSSPseudo] of string = (
    ':visible'
    );

type
  TFresnelViewport = class;
  TFresnelElement = class;

  { TFresnelLayoutNode }

  TFresnelLayoutNode = class(TComponent)
  private
    FElement: TFresnelElement;
    FParent: TFresnelLayoutNode;
    FNodes: TFPList; // list of TFresnelLayoutNode
    function GetNodeCount: integer;
    function GetNodes(Index: integer): TFresnelLayoutNode;
    procedure SetElement(const AValue: TFresnelElement);
    procedure SetParent(const AValue: TFresnelLayoutNode);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function GetRoot: TFresnelLayoutNode;
    procedure SortNodes(const Compare: TListSortComparer_Context; Context: Pointer); virtual;
    property Parent: TFresnelLayoutNode read FParent write SetParent;
    property Element: TFresnelElement read FElement write SetElement;
    property NodeCount: integer read GetNodeCount;
    property Nodes[Index: integer]: TFresnelLayoutNode read GetNodes;
  end;
  TFresnelLayoutElDataClass = class of TFresnelLayoutNode;

  { TFresnelLayouter }

  TFresnelLayouter = class(TComponent)
  public
    procedure ComputeCSS(El: TFresnelElement); virtual; abstract;
    procedure ComputedChildrenCSS(El: TFresnelElement); virtual; abstract;
  end;
  TFresnelLayouterClass = class of TFresnelLayouter;

  IFresnelFont = interface
    ['{6B53C662-5598-419B-996B-7E839271B64E}']
    function GetFamily: string;
    function GetKerning: string;
    function GetSize: string;
    function GetStyle: string;
    function GetVariant: string;
    function GetWeight: string;
    function TextSize(const aText: string): TFresnelPoint;
    function TextSizeMaxWidth(const aText: string; MaxWidth: TFresnelLength): TFresnelPoint;
    function GetTool: TObject;
  end;

  TFresnelFontDesc = record
    Family: string;
    Kerning: string;
    Size: string;
    Style: string;
    Variant_: string;
    Weight: string;
  end;
  PFresnelFontDesc = ^TFresnelFontDesc;

  TFresnelLengthCheck = (
    flcNoNegative,
    flcNoPercentage,
    flcInteger
    );
  TFresnelLengthChecks = set of TFresnelLengthCheck;

  { TFresnelElement }

  TFresnelElement = class(TComponent, ICSSNode, IFPObserver)
  private
    function GetCSSPseudo(Pseudo: TFresnelCSSPseudo): string;
    function GetNodeCount: integer;
    function GetNodes(Index: integer): TFresnelElement;
    function GetCSSElAttribute(Attr: TFresnelCSSAttribute): string;
    function GetCSSElComputedAttribute(Attr: TFresnelCSSAttribute): string;
    procedure SetCSSElComputedAttribute(Attr: TFresnelCSSAttribute; AValue: string);
    function GetCSSRenderedAttribute(Attr: TFresnelCSSAttribute): string;
    procedure SetCSSRenderedAttribute(Attr: TFresnelCSSAttribute; const AValue: string
      );
  private
    FDOMIndex: integer;
    FFont: IFresnelFont;
    FLayoutNode: TFresnelLayoutNode;
    FFontDesc: TFresnelFontDesc;
    FFontDescValid: boolean;
    FRendered: boolean;
    FRenderedBorderBox: TFresnelRect;
    FRenderedContentBox: TFresnelRect;
    // Todo: change to dictionary to reduce mem footprint
    FStandardEvents : Array[0..MaxFresnelEvents] of TEventHandlerItem;
    FEventDispatcher : TFresnelEventDispatcher;
    function GetEventHandler(AIndex: Integer): TFresnelEventHandler;
    procedure SetEventHandler(AIndex: Integer; AValue: TFresnelEventHandler);
 class var
    // Stuff for registering CSS numerical IDs
    FCSSNumericalIDs: array[TCSSNumericalIDKind] of TCSSNumericalIDs;
    FCSSIDToName: array[TCSSNumericalIDKind] of TCSSStringDynArray;
    FCSSIDToNameCount: array[TCSSNumericalIDKind] of integer;
    FFresnelElementTypeID: TCSSNumericalID;
    FFresnelElementBaseAttrID: TCSSNumericalID;
    FFresnelElementBasePseudoID: TCSSNumericalID;
  protected
    FCSSAttributes: array[TFresnelCSSAttribute] of string;
    FCSSClasses: TStrings;
    FCSSComputed: array[TFresnelCSSAttribute] of string;
    FCSSRendered: array[TFresnelCSSAttribute] of string;
    FChildren: TFPList; // list of TFresnelElement
    FParent: TFresnelElement;
    FStyle: string;
    FStyleElements: TCSSElement;
    FCSSPosElement: TCSSElement;
    procedure SetCSSElAttribute(Attr: TFresnelCSSAttribute; const AValue: string); virtual;
    function CheckCSSClear(const AValue: string): boolean; virtual;
    function CheckOrSetCSSDisplay(const AValue: string; Check: boolean): boolean; virtual;
    function CheckCSSDisplayBox(const AValue: string): boolean; virtual;
    function CheckCSSDisplayInside(const AValue: string): boolean; virtual;
    function CheckCSSDisplayOutside(const AValue: string): boolean; virtual;
    function CheckCSSPosition(const AValue: string): boolean; virtual;
    function CheckOrSetCSSOverflow(const AValue: string; Check: boolean): boolean; virtual;
    function CheckCSSOverflowX(const AValue: string): boolean; virtual;
    function CheckCSSOverflowY(const AValue: string): boolean; virtual;
    function CheckCSSZIndex(const AValue: string): boolean; virtual;
    function CheckCSSDirection(const AValue: string): boolean; virtual;
    function CheckOrSetCSSBorder(const AValue: string; Check: boolean): boolean; virtual;
    function CheckOrSetCSSBorderLeft(const AValue: string; Check: boolean): boolean; virtual;
    function CheckOrSetCSSBorderTop(const AValue: string; Check: boolean): boolean; virtual;
    function CheckOrSetCSSBorderRight(const AValue: string; Check: boolean): boolean; virtual;
    function CheckOrSetCSSBorderBottom(const AValue: string; Check: boolean): boolean; virtual;
    function CheckOrSetCSSBorderWidth(const AValue: string; Check: boolean): boolean; virtual;
    function CheckCSSBorderXWidth(Attr: TFresnelCSSAttribute; const AValue: string): boolean; virtual;
    function CheckCSSBorderColor(const AValue: string): boolean; virtual;
    function CheckCSSBoxSizing(const AValue: string): boolean; virtual;
    function CheckCSSFloat(const AValue: string): boolean; virtual;
    function CheckCSSLineHeight(const AValue: string): boolean; virtual;
    function CheckOrSetCSSMargin(const AValue: string; Check: boolean): boolean; virtual;
    function CheckOrSetCSSMarginBlock(const AValue: string; Check: boolean): boolean; virtual;
    function CheckOrSetCSSMarginInline(const AValue: string; Check: boolean): boolean; virtual;
    function CheckCSSMinMaxWidthHeight(Attr: TFresnelCSSAttribute; const AValue: string): boolean; virtual;
    function CheckCSSOpacity(const AValue: string): boolean; virtual;
    function CheckOrSetCSSFont(const AValue: string; Check: boolean): boolean; virtual;
    function CheckCSSFontKerning(const AValue: string): boolean; virtual;
    function CheckCSSFontSize(const AValue: string): boolean; virtual;
    function CheckCSSFontStyle(const AValue: string): boolean; virtual;
    function CheckCSSFontWeight(const AValue: string): boolean; virtual;
    function CheckCSSFontVariant(const AValue: string): boolean; virtual;
    function CheckOrSetCSSPadding(const AValue: string; Check: boolean): boolean; virtual;
    function CheckCSSVisibility(const AValue: string): boolean; virtual;
    function CheckCSSBackgroundColor(const AValue: string): boolean; virtual;
    function CheckCSSColor(const AValue: string): boolean; virtual;
    function GetComputedCSSValue(AttrID: TCSSNumericalID): TCSSString;
    procedure SetComputedCSSValue(AttrID: TCSSNumericalID; const Value: TCSSString);
    procedure SetCSSClasses(const AValue: TStrings);
    procedure SetParent(const AValue: TFresnelElement);
    procedure SetStyle(const AValue: string);
    procedure SetStyleElements(const AValue: TCSSElement);
    class constructor InitFresnelElementClass;
    class destructor FinalFresnelElementClass;
    class function RegisterCSSType(const aName: string): TCSSNumericalID;
    class function RegisterCSSAttr(const aName: string): TCSSNumericalID;
    class function RegisterCSSPseudo(const aName: string): TCSSNumericalID;
    procedure InitCSSResolver(aResolver: TCSSResolver); virtual;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    function CSSReadNextValue(const aValue: string; var p: integer): string;
    function CheckCSSLength(Attr: TFresnelCSSAttribute; const AValue: string; const Checks: TFresnelLengthChecks = []): boolean; virtual;
    procedure ComputeCSSAttribute(Attr: TFresnelCSSAttribute); virtual;
    function GetDPI(IsHorizontal: boolean): TFresnelLength; virtual;
    function GetViewport: TFresnelViewport; virtual;
    function ElementAttrToAttrId(Attr: TFresnelCSSAttribute): TCSSNumericalID;
    function GetFont: IFresnelFont; virtual;
    procedure FPOObservedChanged(ASender: TObject; Operation: TFPObservedOperation; Data: Pointer); virtual;
  protected
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    procedure SetParentComponent(Value: TComponent); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;
    function GetParentComponent: TComponent; override;
    function HasParent: Boolean; override;
    function GetRoot: TFresnelElement;
    function GetPath: string; virtual;
    function AcceptChildrenAtDesignTime: boolean; virtual;
    procedure DomChanged; virtual;
    property Parent: TFresnelElement read FParent write SetParent;
    property NodeCount: integer read GetNodeCount;
    property Nodes[Index: integer]: TFresnelElement read GetNodes; default;
    // CSS
    class function CSSTypeID: TCSSNumericalID; virtual;
    class function CSSTypeName: TCSSString; virtual;
    procedure ClearCSSValues; virtual;
    function GetCSSAttribute(const AttrID: TCSSNumericalID): TCSSString; virtual;
    function GetCSSAttributeClass: TCSSString; virtual;
    function GetCSSChild(const anIndex: integer): ICSSNode; virtual;
    function GetCSSChildCount: integer; virtual;
    function GetCSSDepth: integer; virtual;
    function GetCSSEmpty: boolean; virtual;
    function GetCSSID: TCSSString; virtual;
    function GetCSSIndex: integer; virtual;
    function GetCSSNextOfType: ICSSNode; virtual;
    function GetCSSNextSibling: ICSSNode; virtual;
    function GetCSSParent: ICSSNode; virtual;
    function GetCSSPreviousOfType: ICSSNode; virtual;
    function GetCSSPreviousSibling: ICSSNode; virtual;
    function GetCSSPseudo(const AttrID: TCSSNumericalID): TCSSString; virtual;
    function GetCSSTypeID: TCSSNumericalID; virtual;
    function GetCSSTypeName: TCSSString; virtual;
    function HasCSSAttribute(const AttrID: TCSSNumericalID): boolean; virtual;
    function HasCSSClass(const aClassName: TCSSString): boolean; virtual;
    function HasCSSPseudo(const AttrID: TCSSNumericalID): boolean; virtual;
    procedure SetCSSValue(AttrID: TCSSNumericalID; Value: TCSSElement); virtual;
    function CheckCSSValue(AttrID: TCSSNumericalID; Value: TCSSElement
      ): boolean; virtual;
    function GetCSSInitialAttribute(const AttrID: TCSSNumericalID): TCSSString; virtual;
    function GetCSSInheritAttribute(const AttrID: TCSSNumericalID): TCSSString; virtual;
    procedure ComputeCSS; virtual;
    procedure CSSWarning(const ID: int64; Msg: string); virtual;
    procedure CSSInvalidValueWarning(const ID: int64; Attr: TFresnelCSSAttribute; const aValue: string); virtual;
    // CSS classes and inline style
    property StyleElements: TCSSElement read FStyleElements write SetStyleElements;
    // CSS attributes
    function GetComputedCSSLength(Attr: TFresnelCSSAttribute; UseInherited: boolean; UseNaNOnFail: boolean = false): TFresnelLength; virtual; // on fail returns NaN
    function GetComputedCSString(Attr: TFresnelCSSAttribute; UseInherited: boolean): string; virtual;
    procedure WriteComputedAttributes(Title: string);
    property CSSAttribute[Attr: TFresnelCSSAttribute]: string read GetCSSElAttribute write SetCSSElAttribute;
    property CSSComputedAttribute[Attr: TFresnelCSSAttribute]: string read GetCSSElComputedAttribute write SetCSSElComputedAttribute;
    property CSSPseudo[Pseudo: TFresnelCSSPseudo]: string read GetCSSPseudo;
    class property FresnelElementBaseAttrID: TCSSNumericalID read FFresnelElementBaseAttrID;
    class property FresnelElementBasePseudoID: TCSSNumericalID read FFresnelElementBasePseudoID;
    // layouter
    function GetMaxWidthIntrinsicContentBox: TFresnelLength; virtual; // this element, excluding children, ignoring min-width
    function GetMaxWidthContentBox: TFresnelLength; virtual; // this element, excluding children
    function GetMaxWidthBorderBox: TFresnelLength; virtual; // this element, excluding children
    function GetMinWidthIntrinsicContentBox: TFresnelLength; virtual; // this element, excluding children, ignoring min-width
    function GetMinWidthContentBox: TFresnelLength; virtual; // this element, excluding children
    function GetMinWidthBorderBox: TFresnelLength; virtual; // this element, excluding children
    function GetPreferredContentBox_MaxWidth(MaxWidth: TFresnelLength): TFresnelPoint; virtual; // this element, excluding children
    function GetPreferredBorderBox_MaxWidth(MaxWidth: TFresnelLength): TFresnelPoint; virtual; // this element, excluding children
    property DOMIndex: integer read FDOMIndex write FDOMIndex;
    property LayoutNode: TFresnelLayoutNode read FLayoutNode write FLayoutNode;
    // renderer
    procedure UpdateRenderedAttributes; virtual;
    function GetBoundingClientRect: TFresnelRect; virtual;
    function GetRenderedCSSLength(Attr: TFresnelCSSAttribute; UseInherited: boolean; UseNaNOnFail: boolean = false): TFresnelLength; virtual; // on fail returns NaN
    function GetRenderedCSString(Attr: TFresnelCSSAttribute; UseInherited: boolean): string; virtual;
    property Rendered: boolean read FRendered write FRendered;
    property RenderedBorderBox: TFresnelRect read FRenderedBorderBox write FRenderedBorderBox; // relative to layout parent
    property RenderedContentBox: TFresnelRect read FRenderedContentBox write FRenderedContentBox; // relative to layout parent
    property CSSRenderedAttribute[Attr: TFresnelCSSAttribute]: string read GetCSSRenderedAttribute write SetCSSRenderedAttribute;
    // Events
    Procedure AddEventListener(aID : TEventID; aHandler : TFresnelEventHandler);
    Procedure AddEventListener(Const aName: TEventName; aHandler : TFresnelEventHandler);
    Property EventDispatcher : TFresnelEventDispatcher Read FEventDispatcher;
    // font
    property Font: IFresnelFont read GetFont write FFont;
  published
    // ToDo
    property CSSClasses: TStrings read FCSSClasses write SetCSSClasses;
    property Style: string read FStyle write SetStyle;
    Property OnClick : TFresnelEventHandler Index evtClick Read GetEventHandler Write SetEventHandler;
  end;
  TFresnelElementClass = class of TFresnelElement;
  TFresnelElementArray = array of TFresnelElement;

  TFresnelViewportLength = (
    vlFontMinSize,
    vlDPIHorizontal,
    vlDPIVertical,
    vlHorizontalScrollbarWidth,
    vlVerticalScrollbarWidth
    );
  TFresnelViewportLengths = set of TFresnelViewportLength;

  { TFresnelFontEngine }

  TFresnelFontEngine = class(TComponent)
  public
    function Allocate(const Desc: TFresnelFontDesc): IFresnelFont; virtual; abstract;
  end;

  { TFresnelViewport }

  TFresnelViewport = class(TFresnelElement)
  private
    FCSSResolver: TCSSResolver;
    FFontEngine: TFresnelFontEngine;
    FLayouter: TFresnelLayouter;
    FOnDomChanged: TNotifyEvent;
    FStylesheetElements: TCSSElement;
    FStylesheet: TStrings;
    FDPI: array[boolean] of TFresnelLength;
    FFontMinSize: TFresnelLength;
    FScrollbarWidth: array[boolean] of TFresnelLength;
    FHeight: TFresnelLength;
    FWidth: TFresnelLength;
    FMaxPreferredWidth: TFresnelLength;
    procedure CSSResolverLog(Sender: TObject; Entry: TCSSResolverLogEntry);
  protected
    function GetDPI(IsHorizontal: boolean): TFresnelLength; override;
    function GetHeight: TFresnelLength; virtual;
    function GetMaxPreferredWidth: TFresnelLength; virtual;
    function GetScrollbarWidth(IsHorizontal: boolean): TFresnelLength; virtual;
    function GetVPLength(l: TFresnelViewportLength): TFresnelLength; virtual;
    function GetWidth: TFresnelLength; virtual;
    procedure FPOObservedChanged(ASender: TObject; {%H-}Operation: TFPObservedOperation; {%H-}Data: Pointer); override;
    procedure InitCSSResolver(aResolver: TCSSResolver); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure SetCSSElAttribute(Attr: TFresnelCSSAttribute; const AValue: string
      ); override;
    procedure SetDPI(IsHorizontal: boolean; const AValue: TFresnelLength);
    procedure SetFontMinSize(const AValue: TFresnelLength); virtual;
    procedure SetHeight(AValue: TFresnelLength); virtual;
    procedure SetMaxPreferredWidth(const AValue: TFresnelLength); virtual;
    procedure SetScrollbarWidth(IsHorizontal: boolean;
      const AValue: TFresnelLength); virtual;
    procedure SetStylesheet(AValue: TStrings); virtual;
    procedure SetVPLength(l: TFresnelViewportLength;
      const AValue: TFresnelLength); virtual;
    procedure SetWidth(AValue: TFresnelLength); virtual;
    procedure StylesheetChanged; virtual;
    procedure UpdateStylesheetElements; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ApplyCSS; virtual;
    procedure ClearCSSValues; override;
    procedure DomChanged; override;
    function AllocateFont(const Desc: TFresnelFontDesc): IFresnelFont; virtual;
    function GetElementAt(const x, y: TFresnelLength): TFresnelElement; virtual;
    property DPI[IsHorizontal: boolean]: TFresnelLength read GetDPI write SetDPI;
    property FontMinSize: TFresnelLength read FFontMinSize write SetFontMinSize;
    property ScrollbarWidth[IsHorizontal: boolean]: TFresnelLength read GetScrollbarWidth write SetScrollbarWidth;
    property VPLength[l: TFresnelViewportLength]: TFresnelLength read GetVPLength write SetVPLength;
    property CSSResolver: TCSSResolver read FCSSResolver;
    property Stylesheet: TStrings read FStylesheet write SetStylesheet;
    property StylesheetElements: TCSSElement read FStylesheetElements;
    property Width: TFresnelLength read GetWidth write SetWidth;
    property Height: TFresnelLength read GetHeight write SetHeight;
    property MaxPreferredWidth: TFresnelLength read GetMaxPreferredWidth write SetMaxPreferredWidth;
    property Layouter: TFresnelLayouter read FLayouter write FLayouter;
    property FontEngine: TFresnelFontEngine read FFontEngine write FFontEngine;
    property OnDomChanged: TNotifyEvent read FOnDomChanged write FOnDomChanged;
  end;

  IFresnelStreamRoot = interface
    ['{A53F44CF-3BFB-42F6-A711-8C555E835E7F}']
    function GetViewport: TFresnelViewport;
  end;

const
  FresnelCSSFormatSettings: TFormatSettings = (
    CurrencyFormat: 1;
    NegCurrFormat: 5;
    ThousandSeparator: ',';
    DecimalSeparator: '.';
    CurrencyDecimals: 2;
    DateSeparator: '-';
    TimeSeparator: ':';
    ListSeparator: ',';
    CurrencyString: '$';
    ShortDateFormat: 'd/m/y';
    LongDateFormat: 'dd" "mmmm" "yyyy';
    TimeAMString: 'AM';
    TimePMString: 'PM';
    ShortTimeFormat: 'hh:nn';
    LongTimeFormat: 'hh:nn:ss';
    ShortMonthNames: ('Jan','Feb','Mar','Apr','May','Jun',
                      'Jul','Aug','Sep','Oct','Nov','Dec');
    LongMonthNames: ('January','February','March','April','May','June',
                     'July','August','September','October','November','December');
    ShortDayNames: ('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
    LongDayNames:  ('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
    TwoDigitYearCenturyWindow: 50;
  );

function FloatToCSSStr(const f: TFresnelLength): string;
function CSSStrToFloat(const s: string; out l: TFresnelLength): boolean;
function CompareFresnelPoint(const A, B: TFresnelPoint): integer;
function CompareFresnelRect(const A, B: TFresnelRect): integer;

function FPColorToCSS(const c: TFPColor): string;
function CSSToFPColor(const s: string; out c: TFPColor): boolean;

function dbgs(const p: TFresnelPoint): string;
function dbgs(const r: TFresnelRect): string;

implementation

function FloatToCSSStr(const f: TFresnelLength): string;
begin
  Result:=FloatToStr(f,FresnelCSSFormatSettings);
end;

function CSSStrToFloat(const s: string; out l: TFresnelLength): boolean;
var
  Code: Integer;
begin
  Code:=0;
  l:=0;
  val(s,l,Code);
  if Code<>0 then exit(false);
  if IsNan(l) or (l>MaxFresnelLength) or (l<-MaxFresnelLength) then
    Result:=false
  else
    Result:=true;
end;

function CompareFresnelPoint(const A, B: TFresnelPoint): integer;
begin
  if A.X>B.X then
    Result:=1
  else if A.X<B.X then
    Result:=-1
  else if A.Y>B.Y then
    Result:=1
  else if A.Y<B.Y then
    Result:=-1
  else
    Result:=0;
end;

function CompareFresnelRect(const A, B: TFresnelRect): integer;
begin
  if A.Left>B.Left then
    Result:=1
  else if A.Left<B.Left then
    Result:=-1
  else if A.Top>B.Top then
    Result:=1
  else if A.Top<B.Top then
    Result:=-1
  else if A.Right>B.Right then
    Result:=1
  else if A.Right<B.Right then
    Result:=-1
  else if A.Bottom>B.Bottom then
    Result:=1
  else if A.Bottom<B.Bottom then
    Result:=-1
  else
    Result:=0;
end;

function FPColorToCSS(const c: TFPColor): string;
const
  hex: array[0..15] of char = '0123456789abcdef';
var
  br, bg, bb, ba: Byte;
begin
  if (lo(c.Red)=hi(c.Red))
      and (lo(c.Green)=hi(c.Green))
      and (lo(c.Blue)=hi(c.Blue))
      and (lo(c.Alpha)=hi(c.Alpha)) then
  begin
    br:=lo(c.Red);
    bg:=lo(c.Green);
    bb:=lo(c.Blue);
    ba:=lo(c.Alpha);
    if (br and 15 = br shr 4)
        and (bg and 15 = bg shr 4)
        and (bb and 15 = bb shr 4)
        and (ba and 15 = ba shr 4) then
    begin
      Result:='#'+hex[br and 15]+hex[bg and 15]+hex[bb and 15];
      if c.Alpha<>alphaOpaque then
        Result:=Result+hex[ba and 15];
    end else begin
      Result:='#'+hex[br shr 4]+hex[br and 15]
                 +hex[bg shr 4]+hex[bg and 15]
                 +hex[bb shr 4]+hex[bb and 15];
      if c.Alpha<>alphaOpaque then
        Result:=Result+hex[ba shr 4]+hex[ba and 15];
    end;
  end else begin
    Result:='#'+HexStr(c.Red,4)+HexStr(c.Green,4)+HexStr(c.Blue,4);
    if c.Alpha<>alphaOpaque then
      Result:=Result+HexStr(c.Alpha,4);
  end;
end;

function CSSToFPColor(const s: string; out c: TFPColor): boolean;

  function ReadColor(var p: PChar; Count: byte): word;
  var
    i: Integer;
    ch: Char;
    v: word;
  begin
    Result:=0;
    for i:=1 to Count do
    begin
      ch:=p^;
      case ch of
      '0'..'9': v:=ord(ch)-ord('0');
      'a'..'f': v:=ord(ch)-ord('a')+10;
      'A'..'F': v:=ord(ch)-ord('A')+10;
      end;
      Result:=Result shl 4+v;
      inc(p);
    end;
  end;

var
  HexPerColor: byte;
  p, StartP: PChar;
  HasAlpha: Boolean;
begin
  Result := False;
  c.Red := 0;
  c.Green := 0;
  c.Blue := 0;
  c.Alpha := alphaOpaque;
  if s='' then
    exit;
  p:=@s[1];
  if p^='#' then
  begin
    inc(p);
    StartP:=p;
    repeat
      case p^ of
      #0:
        if p-StartP+1=length(s) then
          break
        else
          exit;
      '0'..'9','a'..'f','A'..'F': inc(p);
      else
        exit;
      end;
    until false;
    p:=StartP;
    case length(s) of
    4:
      begin
        HexPerColor:=1;
        HasAlpha:=false;
      end;
    5:
      begin
        HexPerColor:=1;
        HasAlpha:=true;
      end;
    7:
      begin
        HexPerColor:=2;
        HasAlpha:=false;
      end;
    9:
      begin
        HexPerColor:=2;
        HasAlpha:=true;
      end;
    13:
      begin
        HexPerColor:=4;
        HasAlpha:=false;
      end;
    17:
      begin
        HexPerColor:=4;
        HasAlpha:=true;
      end;
    else
      exit;
    end;
    c.Red:=ReadColor(p,HexPerColor);
    c.Green:=ReadColor(p,HexPerColor);
    c.Blue:=ReadColor(p,HexPerColor);
    if HasAlpha then
      c.Alpha:=ReadColor(p,HexPerColor);
  end else begin
    case LowerCase(s) of
      'aqua'   : c := colAqua;
      'black'  : c := colBlack;
      'blue'   : c := colBlue;
      'fuchsia': c := colFuchsia;
      'gray'   : c := colGray;
      'green'  : c := colGreen;
      'lime'   : c := colLime;
      'maroon' : c := colMaroon;
      'navy'   : c := colNavy;
      'olive'  : c := colOlive;
      'purple' : c := colPurple;
      'red'    : c := colRed;
      'silver' : c := colSilver;
      'teal'   : c := colTeal;
      'white'  : c := colWhite;
      'yellow' : c := colYellow;
    else
      exit;
    end;
  end;
  Result:=true;
end;

function dbgs(const p: TFresnelPoint): string;
begin
  Result:=FloatToStr(p.X)+','+FloatToStr(p.Y);
end;

function dbgs(const r: TFresnelRect): string;
begin
  Result:=FloatToStr(r.Left)+','+FloatToStr(r.Top)+','+FloatToStr(r.Right)+','+FloatToStr(r.Bottom);
end;

{ TFresnelLayoutNode }

procedure TFresnelLayoutNode.SetElement(const AValue: TFresnelElement);
begin
  if FElement=AValue then Exit;
  if FElement<>nil then
    FElement.FLayoutNode:=nil;
  FElement:=AValue;
  if FElement<>nil then
    FElement.FLayoutNode:=Self;
end;

function TFresnelLayoutNode.GetNodeCount: integer;
begin
  if FNodes<>nil then
    Result:=FNodes.Count
  else
    Result:=0;
end;

function TFresnelLayoutNode.GetNodes(Index: integer): TFresnelLayoutNode;
begin
  Result:=TFresnelLayoutNode(FNodes[Index]);
end;

procedure TFresnelLayoutNode.SetParent(const AValue: TFresnelLayoutNode);
begin
  if FParent=AValue then Exit;
  if FParent<>nil then
  begin
    if FParent.FNodes<>nil then
      FParent.FNodes.Remove(Self);
  end;
  FParent:=AValue;
  if FParent<>nil then
  begin
    if FParent.FNodes=nil then
      FParent.FNodes:=TFPList.Create;
    FParent.FNodes.Add(Self);
  end;
end;

constructor TFresnelLayoutNode.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TFresnelLayoutNode.Destroy;
var
  i: Integer;
begin
  for i:=NodeCount-1 downto 0 do
    Nodes[i].FParent:=nil;
  FreeAndNil(FNodes);
  Parent:=nil;
  inherited Destroy;
end;

function TFresnelLayoutNode.GetRoot: TFresnelLayoutNode;
begin
  Result:=Self;
  while Result.Parent<>nil do
    Result:=Result.Parent;
end;

procedure TFresnelLayoutNode.SortNodes(
  const Compare: TListSortComparer_Context; Context: Pointer);
begin
  FNodes.Sort(Compare,Context,SortBase.DefaultSortingAlgorithm);
end;

{ TFresnelPoint }

class function TFresnelPoint.Zero: TFresnelPoint;
begin
  Result.x := 0.0;
  Result.y := 0.0;
end;

function TFresnelPoint.Add(const p: TFresnelPoint): TFresnelPoint;
begin
  Result.x := X+p.X;
  Result.y := Y+p.Y;
end;

function TFresnelPoint.Distance(const p: TFresnelPoint): TFresnelLength;
begin
  Result := Sqrt(Sqr(p.X-X)+Sqr(p.Y-Y));
end;

function TFresnelPoint.IsZero: Boolean;
begin
  Result:=SameValue(X,0) and SameValue(y,0);
end;

function TFresnelPoint.Subtract(const p: TFresnelPoint): TFresnelPoint;
begin
  Result.x := X-p.X;
  Result.y := Y-p.Y;
end;

procedure TFresnelPoint.SetLocation(const p: TFresnelPoint);
begin
  X:=p.X;
  Y:=p.Y;
end;

procedure TFresnelPoint.SetLocation(const ax, ay: TFresnelLength);
begin
  X:=ax;
  Y:=ay;
end;

procedure TFresnelPoint.Offset(const p: TFresnelPoint);
begin
  X:=X+p.X;
  Y:=Y+p.Y;
end;

procedure TFresnelPoint.Offset(const dx, dy: TFresnelLength);
begin
  X:=X+dX;
  Y:=Y+dY;
end;

function TFresnelPoint.Angle(const p: TFresnelPoint): TFresnelLength;

  function ArcTan2(const y,x: TFresnelLength): TFresnelLength;
    begin
      if x=0 then
        begin
          if y=0 then
            Result:=0.0
          else if y>0 then
            Result:=pi/2
          else
            Result:=-pi/2;
        end
      else
        begin
          Result:=ArcTan(y/x);
          if x<0 then
            if y<0 then
              Result:=Result-pi
            else
              Result:=Result+pi;
        end;
    end;

begin
  Result:=ArcTan2(Y-p.Y,X-p.X);
end;

class function TFresnelPoint.PointInCircle(const aPoint,
  aCenter: TFresnelPoint; const aRadius: TFresnelLength): Boolean;
begin
  Result := aPoint.Distance(aCenter) <= aRadius;
end;

class operator TFresnelPoint.=(const p1, p2: TFresnelPoint): Boolean;
begin
  Result:=SameValue(p1.X,p2.X) and SameValue(p1.Y,p2.Y);
end;

class operator TFresnelPoint.<>(const p1, p2: TFresnelPoint): Boolean;
begin
  Result:=(not SameValue(p1.X,p2.X)) or (not SameValue(p1.Y,p2.Y));
end;

class operator TFresnelPoint.+(const p1, p2: TFresnelPoint): TFresnelPoint;
begin
  Result.X:=p1.X+p2.X;
  Result.Y:=p1.Y+p2.Y;
end;

class operator TFresnelPoint.-(const p1, p2: TFresnelPoint): TFresnelPoint;
begin
  Result.X:=p1.X-p2.X;
  Result.Y:=p1.Y-p2.Y;
end;

{ TFresnelRect }

procedure TFresnelRect.Clear;
begin
  Left:=0.0;
  Top:=0.0;
  Right:=0.0;
  Bottom:=0.0;
end;

constructor TFresnelRect.Create(const ALeft, ATop, ARight,
  ABottom: TFresnelLength);
begin
  Left := ALeft;
  Top := ATop;
  Right := ARight;
  Bottom := ABottom;
end;

class operator TFresnelRect.=(const L, R: TFresnelRect): Boolean;
begin
  Result := SameValue(L.Left,R.Left) and SameValue(L.Right,R.Right)
        and SameValue(L.Top,R.Top) and SameValue(L.Bottom,R.Bottom);
end;

class operator TFresnelRect.<>(const L, R: TFresnelRect): Boolean;
begin
  Result := (not SameValue(L.Left,R.Left)) and (not SameValue(L.Right,R.Right))
        and (not SameValue(L.Top,R.Top)) and (not SameValue(L.Bottom,R.Bottom));
end;

class operator TFresnelRect.+(const L, R: TFresnelRect): TFresnelRect;
begin
  Result := TFresnelRect.Union(L, R);
end;

class operator TFresnelRect.*(const L, R: TFresnelRect): TFresnelRect;
begin
  Result := TFresnelRect.Intersect(L, R);
end;

class function TFresnelRect.Empty: TFresnelRect;
begin
  Result := TFresnelRect.Create(0,0,0,0);
end;

procedure TFresnelRect.NormalizeRect;
var
  h: TFresnelLength;
begin
  if Top>Bottom then
  begin
    h := Top;
    Top := Bottom;
    Bottom := h;
  end;
  if Left>Right then
  begin
    h := Left;
    Left := Right;
    Right := h;
  end
end;

function TFresnelRect.IsEmpty: Boolean;
begin
  Result := (Right <= Left) or (Bottom <= Top);
end;

function TFresnelRect.Contains(const P: TFresnelPoint): Boolean;
begin
  Result := (Left <= P.X) and (P.X < Right) and (Top <= P.Y) and (P.Y < Bottom);
end;

function TFresnelRect.Contains(const R: TFresnelRect): Boolean;
begin
  Result := (Left <= R.Left) and (R.Right <= Right) and (Top <= R.Top) and (R.Bottom <= Bottom);
end;

function TFresnelRect.Contains(const ax, ay: TFresnelLength): Boolean;
begin
  Result := (Left <= ax) and (ax < Right) and (Top <= ay) and (ay < Bottom);
end;

function TFresnelRect.IntersectsWith(const R: TFresnelRect): Boolean;
begin
  Result := (Left < R.Right) and (R.Left < Right) and (Top < R.Bottom) and (R.Top < Bottom);
end;

class function TFresnelRect.Intersect(const R1, R2: TFresnelRect): TFresnelRect;
begin
  Result.Left:=Max(R1.Left,R2.Left);
  Result.Right:=Min(R1.Right,R2.Right);
  Result.Top:=Max(R1.Top,R2.Top);
  Result.Bottom:=Min(R1.Bottom,R2.Bottom);
  if Result.IsEmpty then
    FillByte(Result,SizeOf(TFresnelRect),0);
end;

procedure TFresnelRect.Intersect(const R: TFresnelRect);
begin
  if Left<R.Left then Left:=R.Left;
  if Right>R.Right then Right:=R.Right;
  if Top<R.Top then Top:=R.Top;
  if Bottom>R.Bottom then Bottom:=R.Bottom;
  if IsEmpty then
    FillByte(Self,SizeOf(TFresnelRect),0);
end;

class function TFresnelRect.Union(const R1, R2: TFresnelRect): TFresnelRect;
begin
  Result.Left:=Min(R1.Left,R2.Left);
  Result.Right:=Max(R1.Right,R2.Right);
  Result.Top:=Min(R1.Top,R2.Top);
  Result.Bottom:=Max(R1.Bottom,R2.Bottom);
  if Result.IsEmpty then
    FillByte(Result,SizeOf(TFresnelRect),0);
end;

procedure TFresnelRect.Union(const R: TFresnelRect);
begin
  if Left>R.Left then Left:=R.Left;
  if Right<R.Right then Right:=R.Right;
  if Top>R.Top then Top:=R.Top;
  if Bottom<R.Bottom then Bottom:=R.Bottom;
  if IsEmpty then
    FillByte(Self,SizeOf(TFresnelRect),0);
end;

class function TFresnelRect.Union(const Points: array of TFresnelPoint
  ): TFresnelRect;
var
  i: Integer;
begin
  if Length(Points) > 0 then
  begin
    Result.TopLeft := Points[Low(Points)];
    Result.BottomRight := Points[Low(Points)];

    for i := Low(Points)+1 to High(Points) do
    begin
      if Points[i].X < Result.Left then Result.Left := Points[i].X;
      if Points[i].X > Result.Right then Result.Right := Points[i].X;
      if Points[i].Y < Result.Top then Result.Top := Points[i].Y;
      if Points[i].Y > Result.Bottom then Result.Bottom := Points[i].Y;
    end;
  end else
    Result := Empty;
end;

procedure TFresnelRect.Offset(const DX, DY: TFresnelLength);
begin
  Left:=Left+DX;
  Top:=Top+DY;
  Right:=Right+DX;
  Bottom:=Bottom+DY;
end;

procedure TFresnelRect.Offset(const DP: TFresnelPoint);
begin
  Left:=Left+DP.X;
  Top:=Top+DP.Y;
  Right:=Right+DP.X;
  Bottom:=Bottom+DP.Y;
end;

procedure TFresnelRect.SetLocation(const X, Y: TFresnelLength);
begin
  Offset(X-Left, Y-Top);
end;

procedure TFresnelRect.SetLocation(const P: TFresnelPoint);
begin
  Offset(P.X-Left, P.Y-Top);
end;

procedure TFresnelRect.Inflate(const DX, DY: TFresnelLength);
begin
  Left:=Left-DX;
  Top:=Top-DY;
  Right:=Right+DX;
  Bottom:=Bottom+DY;
end;

procedure TFresnelRect.Inflate(const DL, DT, DR, DB: TFresnelLength);
begin
  Left:=Left-DL;
  Top:=Top-DT;
  Right:=Right+DR;
  Bottom:=Bottom+DB;
end;

function TFresnelRect.CenterPoint: TFresnelPoint;
begin
  Result.X := (Left+Right)/2;
  Result.Y := (Top+Bottom)/2;
end;

{ TFresnelViewport }

procedure TFresnelViewport.CSSResolverLog(Sender: TObject;
  Entry: TCSSResolverLogEntry);
begin
  debugln(['TFresnelViewport.CSSResolverLog ','['+IntToStr(Entry.ID)+'] '+Entry.Msg+' at '+CSSResolver.GetElPos(Entry.PosEl)]);
end;

function TFresnelViewport.GetDPI(IsHorizontal: boolean): TFresnelLength;
begin
  Result:=FDPI[IsHorizontal];
end;

function TFresnelViewport.GetScrollbarWidth(IsHorizontal: boolean
  ): TFresnelLength;
begin
  Result:=FScrollbarWidth[IsHorizontal];
end;

function TFresnelViewport.GetHeight: TFresnelLength;
begin
  Result:=FHeight;
end;

function TFresnelViewport.GetMaxPreferredWidth: TFresnelLength;
begin
  Result:=FMaxPreferredWidth;
end;

function TFresnelViewport.GetVPLength(l: TFresnelViewportLength
  ): TFresnelLength;
begin
  Result:=0;
  case l of
    vlFontMinSize: Result:=FFontMinSize;
    vlDPIHorizontal: Result:=FDPI[true];
    vlDPIVertical: Result:=FDPI[false];
    vlHorizontalScrollbarWidth: Result:=FScrollbarWidth[true];
    vlVerticalScrollbarWidth: Result:=FScrollbarWidth[false];
  end;
end;

function TFresnelViewport.GetWidth: TFresnelLength;
begin
  Result:=FWidth;
end;

procedure TFresnelViewport.SetDPI(IsHorizontal: boolean;
  const AValue: TFresnelLength);
begin
  if FDPI[IsHorizontal]=AValue then exit;
  FDPI[IsHorizontal]:=AValue;
end;

procedure TFresnelViewport.SetCSSElAttribute(Attr: TFresnelCSSAttribute;
  const AValue: string);
begin
  // css cannot alter any viewport attributes
  if Attr=fcaDisplay then ;
  if AValue='' then ;
end;

procedure TFresnelViewport.StylesheetChanged;
begin
  UpdateStylesheetElements;
end;

procedure TFresnelViewport.UpdateStylesheetElements;
var
  ss: TStringStream;
  aParser: TCSSParser;
  NewStyleElements: TCSSElement;
begin
  //if FStylesheetElements<>nil then exit;
  aParser:=nil;
  ss:=TStringStream.Create(Stylesheet.Text);
  try
    aParser:=TCSSParser.Create(ss);
    NewStyleElements:=nil;
    try
      NewStyleElements:=aParser.Parse;
    except
      on CSSE: ECSSException do begin
        debugln(['TFresnelViewport.UpdateStylesheetElements ',Name,':',ClassName,' ',CSSE.Message]);
        exit;
      end;
      on FresnelE: EFresnel do begin
        debugln(['TFresnelViewport.UpdateStylesheetElements ',Name,':',ClassName,' ',FresnelE.Message]);
        exit;
      end;
    end;
    if (FStylesheetElements=nil) then
    begin
      if NewStyleElements=nil then exit;
    end else begin
      if (NewStyleElements<>nil)
          and FStylesheetElements.Equals(NewStyleElements) then exit;
    end;
    FreeAndNil(FStylesheetElements);
    FStylesheetElements:=NewStyleElements;
  finally
    aParser.Free;
  end;
  DomChanged;
end;

procedure TFresnelViewport.FPOObservedChanged(ASender: TObject;
  Operation: TFPObservedOperation; Data: Pointer);
begin
  if ASender=FStylesheet then
    StylesheetChanged;
  inherited FPOObservedChanged(ASender,Operation,Data);
end;

procedure TFresnelViewport.InitCSSResolver(aResolver: TCSSResolver);
begin
  inherited InitCSSResolver(aResolver);
  if CSSResolver.StyleCount=0 then
    CSSResolver.AddStyle(StylesheetElements)
  else
    CSSResolver.Styles[0]:=StylesheetElements;
end;

procedure TFresnelViewport.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if AComponent=FLayouter then
      FLayouter:=nil
    else if AComponent=FCSSResolver then
      FCSSResolver:=nil;
  end;
end;

procedure TFresnelViewport.SetFontMinSize(const AValue: TFresnelLength);
begin
  if FFontMinSize=AValue then Exit;
  FFontMinSize:=AValue;
end;

procedure TFresnelViewport.SetHeight(AValue: TFresnelLength);
begin
  if FHeight=AValue then exit;
  FHeight:=AValue;
end;

procedure TFresnelViewport.SetMaxPreferredWidth(const AValue: TFresnelLength);
begin
  if AValue=FMaxPreferredWidth then exit;
  FMaxPreferredWidth:=AValue;
end;

procedure TFresnelViewport.SetScrollbarWidth(IsHorizontal: boolean;
  const AValue: TFresnelLength);
begin
  if FScrollbarWidth[IsHorizontal]=AValue then exit;
  FScrollbarWidth[IsHorizontal]:=AValue;
end;

procedure TFresnelViewport.SetStylesheet(AValue: TStrings);
begin
  if AValue=nil then exit;
  if FStylesheet=AValue then Exit;
  if FStylesheet.Equals(AValue) then Exit;
  FStylesheet.Assign(AValue);
  FreeAndNil(FStylesheetElements);
end;

procedure TFresnelViewport.SetVPLength(l: TFresnelViewportLength;
  const AValue: TFresnelLength);
begin
  case l of
    vlFontMinSize: FontMinSize:=AValue;
    vlDPIHorizontal: DPI[true]:=AValue;
    vlDPIVertical: DPI[false]:=AValue;
    vlHorizontalScrollbarWidth: ScrollbarWidth[true]:=AValue;
    vlVerticalScrollbarWidth: ScrollbarWidth[false]:=AValue;
  end;
end;

procedure TFresnelViewport.SetWidth(AValue: TFresnelLength);
begin
  if FWidth=AValue then exit;
  FWidth:=AValue;
end;

destructor TFresnelViewport.Destroy;
begin
  FreeAndNil(FStylesheetElements);
  FreeAndNil(FStylesheet);
  FreeAndNil(FCSSResolver);
  inherited Destroy;
end;

procedure TFresnelViewport.ApplyCSS;
var
  CurDomIndex: integer;

  procedure Traverse(El: TFresnelElement);
  var
    i: Integer;
  begin
    //writeln('Traverse ',El.ClassName,' CSSTypeName=',El.CSSTypeName);
    El.DOMIndex:=CurDomIndex;
    inc(CurDomIndex);
    if El<>Self then
    begin
      CSSResolver.Compute(El,El.StyleElements);
      El.ComputeCSS;
    end;
    Layouter.ComputeCSS(El);
    for i:=0 to El.NodeCount-1 do
      Traverse(El.Nodes[i]);
    Layouter.ComputedChildrenCSS(El);
  end;

begin
  if NodeCount>1 then
    CSSWarning(20221018152912,'TFresnelViewport.ApplyCSS NodeCount='+IntToStr(NodeCount));
  ClearCSSValues;
  UpdateStylesheetElements;
  InitCSSResolver(CSSResolver);
  //writeln('TFresnelViewport.ApplyCSS ',StylesheetElements.ClassName);
  try
    CurDomIndex:=0;
    Traverse(Self);
  finally
    CSSResolver.ClearStyleCustomData;
  end;
end;

procedure TFresnelViewport.ClearCSSValues;
begin
  inherited ClearCSSValues;
  FCSSComputed[fcaDisplayBox]:='';
  FCSSComputed[fcaDisplayOutside]:='block';
  FCSSComputed[fcaDisplayInside]:='flow-root';
  FCSSComputed[fcaPosition]:='absolute';
  FCSSComputed[fcaWidth]:=FloatToCSSStr(Width);
  FCSSComputed[fcaHeight]:=FloatToCSSStr(Height);
  FCSSComputed[fcaFontFamily]:='arial';
  FCSSComputed[fcaFontSize]:='12';
  FCSSComputed[fcaColor]:='#000';
end;

procedure TFresnelViewport.DomChanged;
begin
  if Assigned(OnDomChanged) then
    OnDomChanged(Self);
end;

function TFresnelViewport.AllocateFont(const Desc: TFresnelFontDesc
  ): IFresnelFont;
begin
  if FFontEngine<>nil then
    Result:=FFontEngine.Allocate(Desc)
  else
    raise EFresnelFont.Create('TFresnelViewport.AllocateFont no FontEngine');
end;

function TFresnelViewport.GetElementAt(const x, y: TFresnelLength
  ): TFresnelElement;

  function Check(Node: TFresnelLayoutNode; const dx, dy: TFresnelLength): TFresnelElement;
  var
    El: TFresnelElement;
    i: Integer;
    BorderBox: TFresnelRect;
    aContentOffset: TFresnelPoint;
  begin
    Result:=nil;
    if Node=nil then exit;
    El:=Node.Element;
    if not El.Rendered then exit;
    if Node.NodeCount>0 then begin
      aContentOffset:=El.RenderedContentBox.TopLeft;
      for i:=Node.NodeCount-1 downto 0 do
      begin
        Result:=Check(Node.Nodes[i],dx+aContentOffset.X,dy+aContentOffset.Y);
        if Result<>nil then exit;
      end;
    end;
    BorderBox:=El.RenderedBorderBox;
    if BorderBox.Contains(x-dx,y-dy) then
      Result:=El
    else
      Result:=nil;
  end;

begin
  Result:=Check(LayoutNode,0,0);
end;

constructor TFresnelViewport.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FWidth:=800;
  FMaxPreferredWidth:=100000;
  FHeight:=600;
  FDPI[false]:=FresnelDefaultDPI;
  FDPI[true]:=FresnelDefaultDPI;
  FFontMinSize:=6;
  FScrollbarWidth[False]:=12;
  FScrollbarWidth[true]:=12;

  FCSSResolver:=TCSSResolver.Create(nil);
  FCSSResolver.OwnsStyle:=false;
  FCSSResolver.OnLog:=@CSSResolverLog;
  FStylesheet:=TStringList.Create(false);
  FStylesheet.FPOAttachObserver(Self);
end;

{ TFresnelElement }

function TFresnelElement.GetNodeCount: integer;
begin
  Result:=FChildren.Count;
end;

function TFresnelElement.GetCSSElComputedAttribute(Attr: TFresnelCSSAttribute
  ): string;
begin
  Result:=FCSSComputed[Attr];
end;

procedure TFresnelElement.SetCSSElComputedAttribute(Attr: TFresnelCSSAttribute;
  AValue: string);
begin
  FCSSComputed[Attr]:=AValue;
end;

function TFresnelElement.GetCSSRenderedAttribute(Attr: TFresnelCSSAttribute
  ): string;
begin
  Result:=FCSSRendered[Attr];
end;

procedure TFresnelElement.SetCSSRenderedAttribute(Attr: TFresnelCSSAttribute;
  const AValue: string);
begin
  FCSSRendered[Attr]:=AValue;
end;

function TFresnelElement.GetEventHandler(AIndex: Integer): TFresnelEventHandler;
begin
  Result:=nil;
  If Assigned(FStandardEvents[aIndex]) then
    Result:=TObjectEventHandlerItem(FStandardEvents[aIndex]).EventHandler;
end;

procedure TFresnelElement.SetEventHandler(AIndex: Integer; AValue: TFresnelEventHandler);
begin
  If Assigned(FStandardEvents[aIndex]) then
    TObjectEventHandlerItem(FStandardEvents[aIndex]).EventHandler:=aValue
  else
    FStandardEvents[aIndex]:=FEventDispatcher.RegisterHandler(aValue,aIndex);
end;

procedure TFresnelElement.SetCSSClasses(const AValue: TStrings);
begin
  if FCSSClasses=AValue then Exit;
  if AValue=nil then
  begin
    if FCSSClasses.Count=0 then exit;
    FCSSClasses.Clear;
  end else begin
    if FCSSClasses.Equals(AValue) then exit;
    FCSSClasses.Assign(AValue);
    FCSSClasses.Delimiter:=' ';
  end;
  DomChanged;
end;

function TFresnelElement.GetCSSPseudo(
  Pseudo: TFresnelCSSPseudo): string;
begin
  Result:='';
  case Pseudo of
  fcpaLang: ;
  end;
end;

function TFresnelElement.GetNodes(Index: integer): TFresnelElement;
begin
  Result:=TFresnelElement(FChildren[Index]);
end;

function TFresnelElement.GetCSSElAttribute(Attr: TFresnelCSSAttribute): string;
begin
  Result:=FCSSAttributes[Attr];
end;

procedure TFresnelElement.SetCSSElAttribute(Attr: TFresnelCSSAttribute;
  const AValue: string);
begin
  writeln('TFresnelElement.SetCSSAttribute ',Name,' ',Attr,' ',AValue);
  if FCSSAttributes[Attr]=AValue then exit;
  FCSSAttributes[Attr]:=AValue;
  case AValue of
  'inherit','initial','unset': exit;
  end;

  // set shorthand attributes
  case Attr of
  fcaDisplay: CheckOrSetCSSDisplay(AValue,false);
  fcaOverflow: CheckOrSetCSSOverflow(AValue,false);
  fcaBorder: CheckOrSetCSSBorder(AValue,false);
  fcaBorderLeft: CheckOrSetCSSBorderLeft(AValue,false);
  fcaBorderTop: CheckOrSetCSSBorderTop(AValue,false);
  fcaBorderRight: CheckOrSetCSSBorderRight(AValue,false);
  fcaBorderBottom: CheckOrSetCSSBorderBottom(AValue,false);
  fcaBorderWidth: CheckOrSetCSSBorderWidth(AValue,false);
  fcaFont: CheckOrSetCSSFont(AValue,false);
  fcaMargin: CheckOrSetCSSMargin(AValue,false);
  fcaMarginBlock: CheckOrSetCSSMarginBlock(AValue,false);
  fcaMarginInline: CheckOrSetCSSMarginInline(AValue,false);
  fcaPadding: CheckOrSetCSSPadding(AValue,false);
  end;
end;

function TFresnelElement.CheckCSSClear(const AValue: string): boolean;
begin
  case AValue of
  'none',
  'left',
  'right',
  'both',
  'inline-start', // ltr left
  'inline-end':  // ltr right
    exit(true);
  else
    CSSInvalidValueWarning(20221031133557,fcaClear,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckOrSetCSSDisplay(const AValue: string; Check: boolean
  ): boolean;
var
  p: integer;
  DispOutside, DispInside: String;
begin
  p:=Pos(' ',AValue);
  if p>0 then
  begin
    p:=1;
    DispOutside:=CSSReadNextValue(AValue,p);
    DispInside:=CSSReadNextValue(AValue,p);
    if Check then
    begin
      if not CheckCSSDisplayOutside(DispOutside) then
        exit(false);
      if not CheckCSSDisplayInside(DispInside) then
        exit(false);
    end else begin
      SetCSSElAttribute(fcaDisplayOutside,DispOutside);
      SetCSSElAttribute(fcaDisplayInside,DispInside);
    end;
  end else begin
    case AValue of
    'none',
    'contents':
      if not Check then
      begin
        SetCSSElAttribute(fcaDisplayBox,AValue);
        SetCSSElAttribute(fcaDisplayInside,'');
        SetCSSElAttribute(fcaDisplayOutside,'');
      end;
    'block',
    'inline':
      if not Check then
      begin
        SetCSSElAttribute(fcaDisplayBox,'');
        SetCSSElAttribute(fcaDisplayOutside,AValue);
        SetCSSElAttribute(fcaDisplayInside,'flow');
      end;
    'flow',
    'flow-root',
    'flex',
    'grid':
      if not Check then
      begin
        SetCSSElAttribute(fcaDisplayBox,'');
        SetCSSElAttribute(fcaDisplayOutside,'');
        SetCSSElAttribute(fcaDisplayInside,AValue);
      end;
    'inline-block':
      if not Check then
      begin
        SetCSSElAttribute(fcaDisplayBox,'');
        SetCSSElAttribute(fcaDisplayOutside,'inline');
        SetCSSElAttribute(fcaDisplayInside,'flow-root');
      end;
    else
      p:=Pos('-',AValue);
      if p>0 then
      begin
        DispOutside:=LeftStr(AValue,p-1);
        DispInside:=copy(AValue,p+1,length(AValue));
        if Check then
        begin
          if not CheckCSSDisplayOutside(DispOutside) then
            exit(false);
          if not CheckCSSDisplayInside(DispInside) then
            exit(false);
        end else begin
          SetCSSElAttribute(fcaDisplayBox,'');
          SetCSSElAttribute(fcaDisplayOutside,DispOutside);
          SetCSSElAttribute(fcaDisplayInside,DispInside);
        end;
      end else begin
        CSSInvalidValueWarning(20221016214635,fcaDisplay,AValue);
        exit(false);
      end;
    end;
  end;
  Result:=true;
end;

function TFresnelElement.CheckCSSDisplayBox(const AValue: string): boolean;
begin
  case AValue of
  'none',
  'contents':
    exit(true);
  else
    CSSInvalidValueWarning(20221031085548,fcaDisplayBox,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckCSSDisplayInside(const AValue: string): boolean;
begin
  case AValue of
  'flow',
  'flow-root',
  'flex',
  'grid':
    exit(true);
  else
    CSSInvalidValueWarning(20221031085111,fcaDisplayInside,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckCSSDisplayOutside(const AValue: string): boolean;
begin
  case AValue of
  'block',
  'inline':
  //'run-in'
    exit(true);
  else
    CSSInvalidValueWarning(20221031085119,fcaDisplayOutside,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckCSSPosition(const AValue: string
  ): boolean;
begin
  case AValue of
  'static',
  'relative',
  'absolute',
  'fixed',
  'sticky': Result:=true;
  else
    CSSInvalidValueWarning(20221016214813,fcaPosition,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckOrSetCSSOverflow(const AValue: string; Check: boolean
  ): boolean;
var
  p: Integer;
  X, Y: String;
begin
  // x, y
  p:=1;
  X:=CSSReadNextValue(AValue,p);
  Y:=CSSReadNextValue(AValue,p);
  if Check then
  begin
    if not CheckCSSOverflowX(X) then
      exit(false);
    if (Y<>'') and not CheckCSSOverflowY(Y) then
      exit(false);
  end else begin
    if Y='' then Y:=X;
    SetCSSElAttribute(fcaOverflowX,X);
    SetCSSElAttribute(fcaOverflowY,Y);
  end;
  Result:=true;
end;

function TFresnelElement.CheckCSSOverflowX(const AValue: string
  ): boolean;
begin
  case AValue of
  'visible',
  'hidden',
  'clip',
  'scroll',
  'auto': Result:=true;
  else
    CSSInvalidValueWarning(20221016214951,fcaOverflowX,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckCSSOverflowY(const AValue: string
  ): boolean;
begin
  case AValue of
  'visible',
  'hidden',
  'clip',
  'scroll',
  'auto': Result:=true;
  else
    CSSInvalidValueWarning(20221016215008,fcaOverflowY,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckCSSZIndex(const AValue: string): boolean;
var
  V, Code: Integer;
begin
  if AValue='auto' then
    exit(true);
  V:=0;
  Code:=0;
  val(AValue,V,Code);
  if (Code>0) or (V<low(ShortInt)) or (V>high(ShortInt)) then
  begin
    CSSInvalidValueWarning(20221016215854,fcaZIndex,AValue);
    Result:=false;
  end else
    Result:=true;
  if V=0 then ;
end;

function TFresnelElement.CheckCSSDirection(const AValue: string
  ): boolean;
begin
  case AValue of
  'lrt','rtl': Result:=true;
  else
    CSSInvalidValueWarning(20221019094537,fcaDirection,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckOrSetCSSBorder(const AValue: string;
  Check: boolean): boolean;
var
  p: Integer;
  aWidth: String;
begin
  // width, style, color
  p:=1;
  aWidth:=CSSReadNextValue(AValue,p);
  //aStyle:=CSSReadNextValue(AValue,p);
  //aColor:=CSSReadNextValue(AValue,p);
  if Check then
  begin
    Result:=CheckCSSBorderXWidth(fcaBorderLeftWidth,aWidth);
  end else begin
    SetCSSElAttribute(fcaBorderLeftWidth,aWidth);
    SetCSSElAttribute(fcaBorderTopWidth,aWidth);
    SetCSSElAttribute(fcaBorderRightWidth,aWidth);
    SetCSSElAttribute(fcaBorderBottomWidth,aWidth);
    Result:=true;
  end;
end;

function TFresnelElement.CheckOrSetCSSBorderLeft(const AValue: string;
  Check: boolean): boolean;
var
  p: Integer;
  aWidth: String;
begin
  // width, style, color
  p:=1;
  aWidth:=CSSReadNextValue(AValue,p);
  //aStyle:=CSSReadNextValue(AValue,p);
  //aColor:=CSSReadNextValue(AValue,p);
  if Check then
  begin
    Result:=CheckCSSBorderXWidth(fcaBorderLeftWidth,aWidth);
  end else
  begin
    SetCSSElAttribute(fcaBorderLeftWidth,aWidth);
  end;
end;

function TFresnelElement.CheckOrSetCSSBorderTop(const AValue: string;
  Check: boolean): boolean;
var
  p: Integer;
  aWidth: String;
begin
  // width, style, color
  p:=1;
  aWidth:=CSSReadNextValue(AValue,p);
  //aStyle:=CSSReadNextValue(AValue,p);
  //aColor:=CSSReadNextValue(AValue,p);
  if Check then
  begin
    Result:=CheckCSSBorderXWidth(fcaBorderTopWidth,aWidth);
  end else
  begin
    SetCSSElAttribute(fcaBorderTopWidth,aWidth);
  end;
end;

function TFresnelElement.CheckOrSetCSSBorderRight(const AValue: string;
  Check: boolean): boolean;
var
  p: Integer;
  aWidth: String;
begin
  // width, style, color
  p:=1;
  aWidth:=CSSReadNextValue(AValue,p);
  //aStyle:=CSSReadNextValue(AValue,p);
  //aColor:=CSSReadNextValue(AValue,p);
  if Check then
  begin
    Result:=CheckCSSBorderXWidth(fcaBorderRightWidth,aWidth);
  end else
  begin
    SetCSSElAttribute(fcaBorderRightWidth,aWidth);
  end;
end;

function TFresnelElement.CheckOrSetCSSBorderBottom(const AValue: string;
  Check: boolean): boolean;
var
  p: Integer;
  aWidth: String;
begin
  // width, style, color
  p:=1;
  aWidth:=CSSReadNextValue(AValue,p);
  //aStyle:=CSSReadNextValue(AValue,p);
  //aColor:=CSSReadNextValue(AValue,p);
  if Check then
  begin
    Result:=CheckCSSBorderXWidth(fcaBorderBottomWidth,aWidth);
  end else
  begin
    SetCSSElAttribute(fcaBorderBottomWidth,aWidth);
  end;
end;

function TFresnelElement.CheckOrSetCSSBorderWidth(const AValue: string;
  Check: boolean): boolean;
var
  p: Integer;
  aTop, aRight, aBottom, aLeft: String;
begin
  p:=1;
  aTop:=CSSReadNextValue(AValue,p);
  aRight:=CSSReadNextValue(AValue,p);
  aBottom:=CSSReadNextValue(AValue,p);
  aLeft:=CSSReadNextValue(AValue,p);
  if Check then
  begin
    if not CheckCSSBorderXWidth(fcaBorderTopWidth,aTop) then
      exit(false);
    if (aRight<>'') and not CheckCSSBorderXWidth(fcaBorderRightWidth,aRight) then
      exit(false);
    if (aBottom<>'') and not CheckCSSBorderXWidth(fcaBorderBottomWidth,aBottom) then
      exit(false);
    if (aLeft<>'') and not CheckCSSBorderXWidth(fcaBorderLeftWidth,aLeft) then
      exit(false);
  end else begin
    if aRight='' then
      aRight:=aTop;
    if aBottom='' then
      aBottom:=aTop;
    if aLeft='' then
      aLeft:=aRight;
    SetCSSElAttribute(fcaBorderLeftWidth,aLeft);
    SetCSSElAttribute(fcaBorderTopWidth,aTop);
    SetCSSElAttribute(fcaBorderRightWidth,aRight);
    SetCSSElAttribute(fcaBorderBottomWidth,aBottom);
  end;
  Result:=true;
end;

function TFresnelElement.CheckCSSBorderXWidth(Attr: TFresnelCSSAttribute;
  const AValue: string): boolean;
begin
  case AValue of
  'thin',
  'medium',
  'thick': Result:=true;
  else
    Result:=CheckCSSLength(Attr,AValue,[flcNoNegative]);
  end
end;

function TFresnelElement.CheckCSSBorderColor(const AValue: string): boolean;
var
  aColor: TFPColor;
begin
  Result:=CSSToFPColor(AValue,aColor);
end;

function TFresnelElement.CheckCSSBoxSizing(const AValue: string): boolean;
begin
  case AValue of
  // ToDo 'border-box',
  'content-box': Result:=true;
  else
    CSSInvalidValueWarning(20221019123227,fcaBoxSizing,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckCSSFloat(const AValue: string): boolean;
begin
  case AValue of
  'none',
  'left',
  'right',
  'inline-start', // ltr depending left
  'inline-end': // ltr depending right
    Result:=true;
  else
    CSSInvalidValueWarning(20221024134429,fcaFloat,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckCSSLineHeight(const AValue: string): boolean;
begin
  if AValue='normal' then
    Result:=true
  else
    Result:=CheckCSSLength(fcaLineHeight,AValue,[flcNoNegative]);
end;

function TFresnelElement.CheckOrSetCSSMargin(const AValue: string;
  Check: boolean): boolean;
var
  p: Integer;
  aTop, aRight, aBottom, aLeft: String;
begin
  p:=1;
  aTop:=CSSReadNextValue(AValue,p);
  aRight:=CSSReadNextValue(AValue,p);
  aBottom:=CSSReadNextValue(AValue,p);
  aLeft:=CSSReadNextValue(AValue,p);
  if Check then
  begin
    if not CheckCSSLength(fcaMarginTop,aTop) then
      exit(false);
    if (aRight<>'') and not CheckCSSLength(fcaMarginRight,aRight) then
      exit(false);
    if (aBottom<>'') and not CheckCSSLength(fcaMarginRight,aBottom) then
      exit(false);
    if (aLeft<>'') and not CheckCSSLength(fcaMarginLeft,aLeft) then
      exit(false);
  end else begin
    if aRight='' then
      aRight:=aTop;
    if aBottom='' then
      aBottom:=aTop;
    if aLeft='' then
      aLeft:=aRight;
    SetCSSElAttribute(fcaMarginLeft,aLeft);
    SetCSSElAttribute(fcaMarginTop,aTop);
    SetCSSElAttribute(fcaMarginRight,aRight);
    SetCSSElAttribute(fcaMarginBottom,aBottom);
  end;
  Result:=true;
end;

function TFresnelElement.CheckOrSetCSSMarginBlock(const AValue: string;
  Check: boolean): boolean;
var
  p: Integer;
  aStart, aEnd: String;
begin
  p:=1;
  aStart:=CSSReadNextValue(AValue,p);
  aEnd:=CSSReadNextValue(AValue,p);
  if Check then
  begin
     if not CheckCSSLength(fcaMarginBlockStart,aStart) then
       exit(false);
    if (aEnd<>'') and not CheckCSSLength(fcaMarginBlockEnd,aEnd) then
      exit(false);
  end else begin
    if aEnd='' then
      aEnd:=aStart;
    SetCSSElAttribute(fcaMarginBlockStart,aStart);
    SetCSSElAttribute(fcaMarginBlockEnd,aEnd);
  end;
  Result:=true;
end;

function TFresnelElement.CheckOrSetCSSMarginInline(const AValue: string;
  Check: boolean): boolean;
var
  p: Integer;
  aStart, aEnd: String;
begin
  p:=1;
  aStart:=CSSReadNextValue(AValue,p);
  aEnd:=CSSReadNextValue(AValue,p);
  if Check then
  begin
    if not CheckCSSLength(fcaMarginInlineStart,aStart) then
      exit(false);
    if (aEnd<>'') and not CheckCSSLength(fcaMarginInlineEnd,aEnd) then
      exit(false);
  end else begin
    if aEnd='' then aEnd:=aStart;
    SetCSSElAttribute(fcaMarginInlineStart,aStart);
    SetCSSElAttribute(fcaMarginInlineEnd,aEnd);
  end;
  Result:=true;
end;

function TFresnelElement.CheckCSSMinMaxWidthHeight(Attr: TFresnelCSSAttribute;
  const AValue: string): boolean;
begin
  case AValue of
  'auto',
  'max-content',
  'min-content': Result:=true;
  else
    // ToDo fit-content()
    Result:=CheckCSSLength(Attr,AValue,[flcNoNegative]);
  end;
end;

function TFresnelElement.CheckCSSOpacity(const AValue: string
  ): boolean;
var
  l: SizeInt;
  Opacity: TFresnelLength;
  Code: integer;
begin
  // float or float%
  if AValue='' then
    exit(true);
  Result:=false;
  l:=length(AValue);
  if AValue[l]='%' then
  begin
    Code:=0;
    val(LeftStr(AValue,l-1),Opacity,Code);
    if Code>0 then
    begin
      CSSInvalidValueWarning(20221019112449,fcaOpacity,AValue);
      exit;
    end;
    if (Opacity<0) or (Opacity>100.0) then
    begin
      CSSInvalidValueWarning(20221019112449,fcaOpacity,AValue);
      exit;
    end;
  end else begin
    Code:=0;
    val(AValue,Opacity,Code);
    if Code>0 then
    begin
      CSSInvalidValueWarning(20221019112600,fcaOpacity,AValue);
      exit;
    end;
    if (Opacity<0) or (Opacity>1.0) then
    begin
      CSSInvalidValueWarning(20221019112607,fcaOpacity,AValue);
      exit;
    end;
  end;
  Result:=true;
end;

function TFresnelElement.CheckOrSetCSSFont(const AValue: string; Check: boolean
  ): boolean;
var
  p, Code, Weight: Integer;
  s, LineHeight: String;
  SlashP: SizeInt;
begin
  p:=1;
  s:=CSSReadNextValue(AValue,p);
  case s of
  'normal','italic','oblique':
    begin
      // first value is font-style
      if not Check then
        SetCSSElAttribute(fcaFontStyle,s);
      s:=CSSReadNextValue(AValue,p);
    end;
  end;
  case s of
  'normal','small-caps':
    begin
      // second value is font-variant
      if not Check then
        SetCSSElAttribute(fcaFontVariant,s);
      s:=CSSReadNextValue(AValue,p);
    end;
  end;
  case s of
  'normal','bold','bolder','lighter':
    begin
      // third value is font-weight
      if not Check then
        SetCSSElAttribute(fcaFontWeight,s);
      s:=CSSReadNextValue(AValue,p);
    end;
  '0'..'9':
    begin
      Weight:=0;
      Code:=0;
      val(s,Weight,Code);
      if (Code>0) or (Weight<=0) then
      begin
        if Check then
          CSSWarning(20221016183759,'font expected font-weight, but got "'+s+'"');
        exit(false);
      end;
      if not Check then
        SetCSSElAttribute(fcaFontWeight,s);
      s:=CSSReadNextValue(AValue,p);
    end;
  end;

  SlashP:=Pos('/',s);
  if SlashP>0 then
  begin
    // font-size/line-height
    LineHeight:=copy(s,SlashP+1,length(s));
    s:=LeftStr(s,SlashP-1);
  end else
    LineHeight:='';

  case s of
  'medium','xx-small','x-small','small','large','x-large','xx-large','smaller','larger':
    begin
      // value is font-size
      if not Check then
        SetCSSElAttribute(fcaFontSize,s);
    end;
  '0'..'9':
    begin
      Weight:=0;
      Code:=0;
      val(s,Weight,Code);
      if Code>0 then
      begin
        if Check then
          CSSWarning(20221016184818,'font expected font-size, but got "'+s+'"');
        exit(false);
      end;
      if not Check then
        SetCSSElAttribute(fcaFontSize,s);
    end;
  else
    if Check then
      CSSWarning(20221016184934,'font expected font-size, but got "'+s+'"');
    exit(false);
  end;
  if (LineHeight<>'') and not Check then
    SetCSSElAttribute(fcaLineHeight,LineHeight);

  // rest is font-family
  while (p<=length(AValue)) and (aValue[p] in [' ',#9,#10,#13]) do inc(p);

  if not Check then
    SetCSSElAttribute(fcaFontFamily,copy(AValue,p,length(AValue)));
  Result:=true;
end;

function TFresnelElement.CheckCSSFontKerning(const AValue: string
  ): boolean;
begin
  case AValue of
  'auto','normal','none': Result:=true;
  else
    CSSInvalidValueWarning(20221016233244,fcaFontKerning,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckCSSFontSize(const AValue: string
  ): boolean;
begin
  case AValue of
  'medium',
  'xx-small',
  'x-small',
  'small',
  'large',
  'x-large',
  'xx-large',
  'smaller',
  'larger',
  'math': Result:=true;
  else
    Result:=CheckCSSLength(fcaFontSize,AValue,[flcNoNegative]);
  end;
end;

function TFresnelElement.CheckCSSFontStyle(const AValue: string
  ): boolean;
begin
  case AValue of
  'normal',
  'italic',
  'oblique': Result:=true;
  else
    CSSInvalidValueWarning(20221016234702,fcaFontStyle,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckCSSFontWeight(const AValue: string
  ): boolean;
begin
  case AValue of
  'normal',
  'bold',
  'lighter',
  'bolder',
  '100',
  '200',
  '300',
  '400',
  '500',
  '600',
  '700',
  '800',
  '900': Result:=true;
  else
    CSSInvalidValueWarning(20221016235036,fcaFontWeight,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckCSSFontVariant(const AValue: string
  ): boolean;
begin
  case AValue of
  'normal',
  'small-caps',
  'none': Result:=true;
  else
    CSSInvalidValueWarning(20221016235254,fcaFontVariant,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckOrSetCSSPadding(const AValue: string;
  Check: boolean): boolean;
var
  p: Integer;
  aTop, aRight, aBottom, aLeft: String;
begin
  p:=1;
  aTop:=CSSReadNextValue(AValue,p);
  aRight:=CSSReadNextValue(AValue,p);
  aBottom:=CSSReadNextValue(AValue,p);
  aLeft:=CSSReadNextValue(AValue,p);
  if Check then
  begin
    if not CheckCSSLength(fcaPaddingTop,aTop,[flcNoNegative]) then
      exit(false);
    if (aRight<>'') and not CheckCSSLength(fcaPaddingRight,aRight,[flcNoNegative]) then
      exit(false);
    if (aBottom<>'') and not CheckCSSLength(fcaPaddingBottom,aBottom,[flcNoNegative]) then
      exit(false);
    if (aLeft<>'') and not CheckCSSLength(fcaPaddingLeft,aBottom,[flcNoNegative]) then
      exit(false);
  end else begin
    if aRight='' then
      aRight:=aTop;
    if aBottom='' then
      aBottom:=aTop;
    if aLeft='' then
      aLeft:=aRight;
    SetCSSElAttribute(fcaPaddingLeft,aLeft);
    SetCSSElAttribute(fcaPaddingTop,aTop);
    SetCSSElAttribute(fcaPaddingRight,aRight);
    SetCSSElAttribute(fcaPaddingBottom,aBottom);
  end;
  Result:=true;
end;

function TFresnelElement.CheckCSSVisibility(const AValue: string): boolean;
begin
  case AValue of
  'visible',
  'hidden',
  'collapse': Result:=true;
  else
    CSSInvalidValueWarning(20221031131529,fcaVisibility,AValue);
    Result:=false;
  end;
end;

function TFresnelElement.CheckCSSBackgroundColor(const AValue: string): boolean;
var
  aColor: TFPColor;
begin
  // ToDo: rgb(r,g,b), rgba(r,g,b,a)
  // ToDo: hsl(), hsla()
  // ToDo: transparent
  // ToDo: currentcolor
  Result:=CSSToFPColor(AValue,aColor);
end;

function TFresnelElement.CheckCSSColor(const AValue: string): boolean;
var
  aColor: TFPColor;
begin
  // ToDo: rgb(r,g,b), rgba(r,g,b,a), ...
  Result:=CSSToFPColor(AValue,aColor);
end;

function TFresnelElement.GetComputedCSSValue(AttrID: TCSSNumericalID
  ): TCSSString;
var
  Attr: TFresnelCSSAttribute;
begin
  if (AttrID<FFresnelElementBaseAttrID) or (AttrID>ord(High(TFresnelCSSAttribute))+FFresnelElementBaseAttrID) then
    exit('');
  Attr:=TFresnelCSSAttribute(AttrID-FFresnelElementBaseAttrID);
  Result:=FCSSComputed[Attr];
end;

procedure TFresnelElement.SetComputedCSSValue(AttrID: TCSSNumericalID;
  const Value: TCSSString);
var
  Attr: TFresnelCSSAttribute;
begin
  if (AttrID<FFresnelElementBaseAttrID) or (AttrID>ord(High(TFresnelCSSAttribute))+FFresnelElementBaseAttrID) then
    exit;
  Attr:=TFresnelCSSAttribute(AttrID-FFresnelElementBaseAttrID);
  FCSSComputed[Attr]:=Value;
end;

procedure TFresnelElement.SetParent(const AValue: TFresnelElement);
begin
  if FParent=AValue then Exit;
  if AValue=Self then
    raise Exception.Create('cycle');

  if FParent<>nil then
  begin
    FParent.FChildren.Remove(Self);
    FParent.DomChanged;
  end;
  FParent:=AValue;
  if FParent<>nil then
  begin
    FParent.FChildren.Add(Self);
    FreeNotification(FParent);
    FParent.DomChanged;
  end;
end;

procedure TFresnelElement.SetStyle(const AValue: string);
var
  ss: TStringStream;
  aParser: TCSSParser;
  NewStyleElements: TCSSElement;
begin
  if FStyle=AValue then Exit;
  FStyle:=AValue;
  aParser:=nil;
  ss:=TStringStream.Create(Style);
  try
    NewStyleElements:=nil;
    aParser:=TCSSParser.Create(ss);
    try
      NewStyleElements:=aParser.ParseInline;
    except
      on CSSE: ECSSException do begin
        debugln(['TFresnelElement.SetStyle ',Name,':',ClassName,' ',CSSE.Message]);
        exit;
      end;
      on FresnelE: EFresnel do begin
        debugln(['TFresnelElement.SetStyle ',Name,':',ClassName,' ',FresnelE.Message]);
        exit;
      end;
    end;
    if (FStyleElements=nil) then
    begin
      if NewStyleElements=nil then exit;
    end else begin
      if (NewStyleElements<>nil)
          and FStyleElements.Equals(NewStyleElements) then exit;
    end;
    FreeAndNil(FStyleElements);
    FStyleElements:=NewStyleElements;
  finally
    aParser.Free;
  end;
  DomChanged;
end;

procedure TFresnelElement.SetStyleElements(const AValue: TCSSElement);
begin
  if FStyleElements=AValue then Exit;
  FreeAndNil(FStyleElements);
  FStyleElements:=AValue;
  DomChanged;
end;

class function TFresnelElement.RegisterCSSType(const aName: string
  ): TCSSNumericalID;
var
  Old: LongInt;
  Map: TCSSNumericalIDs;
begin
  Map:=FCSSNumericalIDs[nikType];
  Old:=Map[aName];
  if Old>0 then
    raise Exception.Create('TFresnelElement.RegisterCSSIDType type duplicate "'+aName+'"');
  Result:=CSSLastTypeID+100+Map.Count;
  //writeln('TFresnelElement.RegisterCSSType ',ClassName,' aName=',aName,' Result=',Result);
  Map[aName]:=Result;
end;

class function TFresnelElement.RegisterCSSAttr(const aName: string
  ): TCSSNumericalID;
var
  Old: LongInt;
  Map: TCSSNumericalIDs;
begin
  Map:=FCSSNumericalIDs[nikAttribute];
  Old:=Map[aName];
  if Old>0 then
    raise Exception.Create('TFresnelElement.RegisterCSSAttr type duplicate "'+aName+'"');
  Result:=CSSLastAttributeID+100+Map.Count;
  Map[aName]:=Result;
end;

class function TFresnelElement.RegisterCSSPseudo(const aName: string
  ): TCSSNumericalID;
var
  Old: LongInt;
  Map: TCSSNumericalIDs;
begin
  Map:=FCSSNumericalIDs[nikPseudoAttribute];
  Old:=Map[aName];
  if Old>0 then
    raise Exception.Create('TFresnelElement.RegisterCSSPseudo type duplicate "'+aName+'"');
  Result:=CSSLastPseudoID+100+Map.Count;
  Map[aName]:=Result;
end;

procedure TFresnelElement.InitCSSResolver(aResolver: TCSSResolver);
var
  Kind: TCSSNumericalIDKind;
begin
  for Kind in TCSSNumericalIDKind do
    aResolver.NumericalIDs[Kind]:=FCSSNumericalIDs[Kind];
end;

procedure TFresnelElement.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if AComponent=Self then exit;
  if Operation=opRemove then
  begin
    if FParent=AComponent then
      FParent:=nil;
    if FChildren<>nil then
      FChildren.Remove(AComponent);
  end;
end;

function TFresnelElement.CSSReadNextValue(const aValue: string; var p: integer
  ): string;
var
  l: SizeInt;
  StartP: Integer;
begin
  Result:='';
  l:=length(aValue);
  if p>l then exit;
  while (p<=l) and (aValue[p] in [' ',#9,#10,#13]) do inc(p);
  StartP:=p;
  while (p<=l) and not (aValue[p] in [' ',#9,#10,#13]) do inc(p);
  Result:=copy(aValue,StartP,p-StartP);
end;

procedure TFresnelElement.CSSWarning(const ID: int64; Msg: string);
begin
  if FCSSPosElement<>nil then
    Msg:=FCSSPosElement.SourceFileName+'('+IntToStr(FCSSPosElement.SourceRow)+','+IntToStr(FCSSPosElement.SourceCol)+') '+Msg;
  Msg:=Msg+'['+IntToStr(ID)+'] '+Msg;
  DebugLn(['TFresnelElement.CSSError ',Msg]);
end;

procedure TFresnelElement.CSSInvalidValueWarning(const ID: int64;
  Attr: TFresnelCSSAttribute; const aValue: string);
begin
  CSSWarning(ID,'invalid '+FresnelCSSAttributeNames[Attr]+' value "'+AValue+'"');
end;

function TFresnelElement.CheckCSSLength(Attr: TFresnelCSSAttribute;
  const AValue: string; const Checks: TFresnelLengthChecks): boolean;
var
  p, StartP: PChar;
begin
  Result:=false;
  if AValue='auto' then
    Result:=true
  else if AValue='' then
    CSSWarning(20221016220554,'missing '+FresnelCSSAttributeNames[Attr]+' value')
  else begin
    // <float><unit>
    StartP:=PChar(AValue);
    p:=StartP;
    if p^='-' then
    begin
      if flcNoNegative in Checks then
      begin
        CSSInvalidValueWarning(20221103222222,Attr,AValue);
        exit;
      end;
      inc(p);
    end;
    if not (p^ in ['0'..'9']) then
    begin
      CSSInvalidValueWarning(20221016220901,Attr,AValue);
      exit;
    end;
    inc(p);
    while p^ in ['0'..'9'] do inc(p);
    if p^='.' then
    begin
      // float
      if flcInteger in Checks then
      begin
        CSSInvalidValueWarning(20221103222126,Attr,AValue);
        exit;
      end;
      inc(p);
      if not (p^ in ['0'..'9']) then
      begin
        CSSInvalidValueWarning(20221016221010,Attr,AValue);
        exit;
      end;
      inc(p);
      while p^ in ['0'..'9'] do inc(p);
    end;
    if (p^ in ['e','E']) and (p[1] in ['-','0'..'9']) then
    begin
      // exponent
      if flcInteger in Checks then
      begin
        CSSInvalidValueWarning(20221103222139,Attr,AValue);
        exit;
      end;
      inc(p);
      if p^='-' then
        inc(p);
      if not (p^ in ['0'..'9']) then
      begin
        CSSInvalidValueWarning(20221016220901,Attr,AValue);
        exit;
      end;
      inc(p);
      while p^ in ['0'..'9'] do inc(p);
    end;

    case p^ of
    '%':
      begin
        if flcNoPercentage in Checks then
        begin
          CSSInvalidValueWarning(20221103222155,Attr,AValue);
          exit;
        end;
        inc(p);
      end;
    'c':
      case p[1] of
      'm','h': inc(p,2); // cm, ch
      end;
    'e':
      case p[1] of
      'm','x': inc(p,2); // em, ex
      end;
    'i':
      if p[1]='n' then
        inc(p,2); // in
    'm':
      if p[1]='m' then
        inc(p,2); // mm
    'p':
      case p[1] of
      'x','t','c': inc(p,2); // px, pt, pc
      end;
    'r':
      if (p[1]='e') and (p[2]='m') then inc(p,3);
    'v':
      case p[1] of
      'w','h': inc(p,2); // vw, vh
      'm':
        case p[2] of
        'a': if p[3]='x' then inc(p,4);
        'i': if p[3]='n' then inc(p,4);
        end;
      end;
    end;
    // note: no unit is ok
    if p-StartP<>length(AValue) then
      CSSInvalidValueWarning(20221016221747,Attr,AValue)
    else
      Result:=true;
  end;
end;

procedure TFresnelElement.ComputeCSSAttribute(Attr: TFresnelCSSAttribute);
var
  aValue: String;
begin
  aValue:=FCSSAttributes[Attr];
  if aValue='initial' then
    aValue:=GetCSSInitialAttribute(ElementAttrToAttrId(Attr));
  if aValue='inherit' then
    aValue:=GetCSSInheritAttribute(ElementAttrToAttrId(Attr));
  if aValue='unset' then
    aValue:='';
  FCSSComputed[Attr]:=aValue;
end;

function TFresnelElement.GetDPI(IsHorizontal: boolean): TFresnelLength;
begin
  if Parent<>nil then
    Result:=Parent.GetDPI(IsHorizontal)
  else
    Result:=FresnelDefaultDPI;
end;

function TFresnelElement.GetViewport: TFresnelViewport;
var
  El: TFresnelElement;
begin
  El:=GetRoot;
  if El is TFresnelViewport then
    Result:=TFresnelViewport(El)
  else
    Result:=nil;
end;

function TFresnelElement.GetComputedCSSLength(Attr: TFresnelCSSAttribute;
  UseInherited: boolean; UseNaNOnFail: boolean): TFresnelLength;
var
  El: TFresnelElement;
  s: String;
begin
  if CSSStrToFloat(FCSSComputed[Attr],Result) then
    exit;
  if UseInherited then
  begin
    El:=Parent;
    while El<>nil do
    begin
      if CSSStrToFloat(El.FCSSComputed[Attr],Result) then
        exit;
      El:=El.Parent;
    end;
  end;
  s:=GetCSSInitialAttribute(ElementAttrToAttrId(Attr));
  if CSSStrToFloat(s,Result) then
    exit;
  if UseNaNOnFail then
    Result:=NaN
  else
    Result:=0;
end;

function TFresnelElement.GetComputedCSString(Attr: TFresnelCSSAttribute;
  UseInherited: boolean): string;
var
  El: TFresnelElement;
begin
  Result:=FCSSComputed[Attr];
  if Result<>'' then exit;
  if UseInherited then
  begin
    El:=Parent;
    while El<>nil do
    begin
      Result:=El.CSSComputedAttribute[Attr];
      if Result<>'' then
        exit;
      El:=El.Parent;
    end;
  end;
  Result:=GetCSSInitialAttribute(ElementAttrToAttrId(Attr));
end;

procedure TFresnelElement.WriteComputedAttributes(Title: string);
var
  Attr: TFresnelCSSAttribute;
begin
  writeln('TFresnelElement.WriteComputedAttributes ',Title,' ',GetPath,'================');
  for Attr in TFresnelCSSAttribute do
    if FCSSComputed[Attr]<>'' then
      writeln('  ',Attr,'="',FCSSComputed[Attr],'"');
end;

function TFresnelElement.GetMaxWidthIntrinsicContentBox: TFresnelLength;
begin
  Result:=GetViewport.MaxPreferredWidth;
end;

function TFresnelElement.GetMaxWidthContentBox: TFresnelLength;
var
  aValue: String;
begin
  aValue:=CSSComputedAttribute[fcaMinWidth];
  case aValue of
  '',
  'auto': Result:=GetMaxWidthIntrinsicContentBox;
  'max-content': Result:=GetPreferredContentBox_MaxWidth(GetViewport.MaxPreferredWidth).X;
  'min-content': Result:=GetMinWidthIntrinsicContentBox;
  else
    Result:=GetComputedCSSLength(fcaMaxWidth,false);
  end;
  if Result<0 then
    Result:=0;
end;

function TFresnelElement.GetMaxWidthBorderBox: TFresnelLength;
begin
  // border and padding cannot be negative
  Result:=GetComputedCSSLength(fcaBorderLeft,false)
         +GetComputedCSSLength(fcaBorderRight,false)
         +GetComputedCSSLength(fcaPaddingLeft,false)
         +GetComputedCSSLength(fcaPaddingRight,false)
         +GetMinWidthContentBox;
  if IsNan(Result) or (Result<0) then
    Result:=0;
end;

function TFresnelElement.GetMinWidthIntrinsicContentBox: TFresnelLength;
begin
  Result:=0;
end;

function TFresnelElement.GetMinWidthContentBox: TFresnelLength;
var
  aValue: String;
begin
  aValue:=CSSComputedAttribute[fcaMinWidth];
  case aValue of
  '',
  'auto': Result:=GetMinWidthIntrinsicContentBox;
  'max-content': Result:=GetPreferredContentBox_MaxWidth(GetViewport.MaxPreferredWidth).X;
  'min-content': Result:=GetMinWidthIntrinsicContentBox;
  else
    Result:=GetComputedCSSLength(fcaMinWidth,false);
  end;
  if Result<0 then
    Result:=0;
end;

function TFresnelElement.GetMinWidthBorderBox: TFresnelLength;
begin
  // border and padding cannot be negative
  Result:=GetComputedCSSLength(fcaBorderLeft,false)
         +GetComputedCSSLength(fcaBorderRight,false)
         +GetComputedCSSLength(fcaPaddingLeft,false)
         +GetComputedCSSLength(fcaPaddingRight,false)
         +GetMinWidthContentBox;
  if Result<0 then
    Result:=0;
end;

function TFresnelElement.GetPreferredContentBox_MaxWidth(
  MaxWidth: TFresnelLength): TFresnelPoint;
begin
  Result:=default(TFresnelPoint);
  if MaxWidth=0 then ;
end;

function TFresnelElement.GetPreferredBorderBox_MaxWidth(MaxWidth: TFresnelLength
  ): TFresnelPoint;
var
  ExtraWidth, ExtraHeight: TFresnelLength;
begin
  // border and padding cannot be negative
  ExtraWidth:=GetComputedCSSLength(fcaBorderLeft,false)
             +GetComputedCSSLength(fcaBorderRight,false)
             +GetComputedCSSLength(fcaPaddingLeft,false)
             +GetComputedCSSLength(fcaPaddingRight,false);
  ExtraHeight:=GetComputedCSSLength(fcaBorderTop,false)
             +GetComputedCSSLength(fcaBorderBottom,false)
             +GetComputedCSSLength(fcaPaddingTop,false)
             +GetComputedCSSLength(fcaPaddingBottom,false);
  Result:=GetPreferredContentBox_MaxWidth(Max(0,MaxWidth-ExtraWidth));
  Result.X:=Result.X+ExtraWidth;
  Result.Y:=Result.Y+ExtraHeight;
end;

procedure TFresnelElement.UpdateRenderedAttributes;
var
  Attr: TFresnelCSSAttribute;
begin
  FRendered:=false;
  FRenderedBorderBox:=default(TFresnelRect);
  for Attr in TFresnelCSSAttribute do
    FCSSRendered[Attr]:=FCSSComputed[Attr];
end;

function TFresnelElement.GetBoundingClientRect: TFresnelRect;
var
  Node: TFresnelLayoutNode;
begin
  if (not Rendered) then
  begin
    Result.Clear;
    exit;
  end;
  Result:=RenderedBorderBox;
  Node:=LayoutNode.Parent;
  while Node<>nil do begin
    Result.Offset(Node.Element.RenderedContentBox.TopLeft);
    Node:=Node.Parent;
  end;
  //debugln(['TFresnelElement.GetBoundingClientRect ',Name,' Result=',Result.Left,',',Result.Top,',',Result.Right,',',Result.Bottom]);
end;

function TFresnelElement.GetRenderedCSSLength(Attr: TFresnelCSSAttribute;
  UseInherited: boolean; UseNaNOnFail: boolean): TFresnelLength;
var
  El: TFresnelElement;
  s: String;
begin
  if CSSStrToFloat(FCSSRendered[Attr],Result) then
    exit;
  if UseInherited then
  begin
    El:=Parent;
    while El<>nil do
    begin
      if CSSStrToFloat(El.FCSSRendered[Attr],Result) then
        exit;
      El:=El.Parent;
    end;
  end;
  s:=GetCSSInitialAttribute(ElementAttrToAttrId(Attr));
  if CSSStrToFloat(s,Result) then
    exit;
  if UseNaNOnFail then
    Result:=NaN
  else
    Result:=0;
end;

function TFresnelElement.GetRenderedCSString(Attr: TFresnelCSSAttribute;
  UseInherited: boolean): string;
var
  El: TFresnelElement;
begin
  Result:=FCSSRendered[Attr];
  if Result<>'' then exit;
  if UseInherited then
  begin
    El:=Parent;
    while El<>nil do
    begin
      Result:=El.CSSRenderedAttribute[Attr];
      if Result<>'' then
        exit;
      El:=El.Parent;
    end;
  end;
  Result:=GetCSSInitialAttribute(ElementAttrToAttrId(Attr));
end;

procedure TFresnelElement.AddEventListener(aID: TEventID; aHandler: TFresnelEventHandler);
begin
  EventDispatcher.RegisterHandler(aHandler,aID);
end;

procedure TFresnelElement.AddEventListener(const aName: TEventName; aHandler: TFresnelEventHandler);
begin
  EventDispatcher.RegisterHandler(aHandler,aName);
end;

function TFresnelElement.ElementAttrToAttrId(Attr: TFresnelCSSAttribute
  ): TCSSNumericalID;
begin
  Result:=ord(Attr)+FFresnelElementBaseAttrID;
end;

function TFresnelElement.GetFont: IFresnelFont;
var
  ViewPort: TFresnelViewport;
begin
  if FFontDescValid then
    exit(FFont);
  FFontDescValid:=true;
  FFontDesc.Family:=GetComputedCSString(fcaFontFamily,true);
  FFontDesc.Style:=GetComputedCSString(fcaFontStyle,true);
  FFontDesc.Variant_:=GetComputedCSString(fcaFontVariant,true);
  FFontDesc.Weight:=GetComputedCSString(fcaFontWeight,true);
  FFontDesc.Size:=GetComputedCSString(fcaFontSize,true);
  FFontDesc.Kerning:=GetComputedCSString(fcaFontKerning,true);
  if FFont<>nil then
  begin
    if (FFont.GetFamily=FFontDesc.Family)
        and (FFont.GetStyle=FFontDesc.Style)
        and (FFont.GetVariant=FFontDesc.Variant_)
        and (FFont.GetWeight=FFontDesc.Weight)
        and (FFont.GetSize=FFontDesc.Size)
        and (FFont.GetKerning=FFontDesc.Kerning) then
      exit(FFont); // still valid
    FFont:=nil;
  end;

  ViewPort:=GetViewport;
  FFont:=ViewPort.AllocateFont(FFontDesc);
  Result:=FFont;
end;

procedure TFresnelElement.DomChanged;
begin
  if FParent<>nil then
    FParent.DomChanged;
end;

function TFresnelElement.HasParent: Boolean;
begin
  Result:=Parent<>nil;
end;

procedure TFresnelElement.SetParentComponent(Value: TComponent);
var
  aStreamRoot: IFresnelStreamRoot;
begin
  debugln(['TFresnelElement.SetParentComponent Self=',DbgSName(Self),' NewParent=',DbgSName(Value)]);
  if Value=nil then
    Parent:=nil
  else if Value is TFresnelElement then
    Parent:=TFresnelElement(Value)
  else if Supports(Value,IFresnelStreamRoot,aStreamRoot) then
  begin
    debugln(['TFresnelElement.SetParentComponent Self=',DbgSName(Self),' redirecting to viewport']);
    Parent:=aStreamRoot.GetViewport;
  end
  else
    raise EFresnel.Create('TFresnelElement.SetParentComponent Self='+DbgSName(Self)+' NewParentComponent='+DbgSName(Value));
end;

procedure TFresnelElement.GetChildren(Proc: TGetChildProc; Root: TComponent);
var
  i: Integer;
begin
  for i:=0 to NodeCount-1 do
    if Nodes[i].Owner=Root then
      Proc(Nodes[i]);

  if Root = Self then
    for i:=0 to ComponentCount-1 do
      if Components[i].GetParentComponent = nil then
        Proc(Components[i]);
end;

function TFresnelElement.GetParentComponent: TComponent;
begin
  Result:=Parent;
end;

constructor TFresnelElement.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FChildren:=TFPList.Create;
  FCSSClasses:=TStringList.Create;
  FCSSClasses.Delimiter:=' ';
  FCSSClasses.FPOAttachObserver(Self);
  FEventDispatcher:=TFresnelEventDispatcher.Create(Self);
end;

destructor TFresnelElement.Destroy;
begin
  Clear;
  FCSSClasses.FPOAttachObserver(Self);
  FreeAndNil(FLayoutNode);
  FreeAndNil(FChildren);
  FreeAndNil(FCSSClasses);
  FreeAndNil(FEventDispatcher);
  inherited Destroy;
end;

procedure TFresnelElement.Clear;
var
  i: Integer;
begin
  FCSSClasses.Clear;
  for i:=NodeCount-1 downto 0 do
    Nodes[i].Parent:=nil;
  FChildren.Clear;
end;

function TFresnelElement.GetRoot: TFresnelElement;
begin
  Result:=Self;
  while Result.Parent<>nil do
    Result:=Result.Parent;
end;

function TFresnelElement.GetPath: string;
begin
  if Parent<>nil then
    Result:=Parent.GetPath+'.'
  else
    Result:='';
  if Name='' then
    Result:=Result+ClassName
  else
    Result:=Result+Name;
end;

function TFresnelElement.AcceptChildrenAtDesignTime: boolean;
begin
  Result:=true;
end;

procedure TFresnelElement.FPOObservedChanged(ASender: TObject;
  Operation: TFPObservedOperation; Data: Pointer);
begin
  if Operation=ooFree then exit;
  if Data=nil then ;
  if ASender=FCSSClasses then
    DomChanged;
end;

class constructor TFresnelElement.InitFresnelElementClass;
var
  Kind: TCSSNumericalIDKind;
  Attr: TFresnelCSSAttribute;
  id: TCSSNumericalID;
  PseudoAttr: TFresnelCSSPseudo;
begin
  for Kind in TCSSNumericalIDKind do
  begin
    FCSSNumericalIDs[Kind]:=TCSSNumericalIDs.Create(Kind);
    FCSSIDToName[Kind]:=nil;
    FCSSIDToNameCount[Kind]:=0;
  end;

  // register type
  FFresnelElementTypeID:=RegisterCSSType(CSSTypeName);

  // register attributes
  FFresnelElementBaseAttrID:=CSSIDNone;
  for Attr in TFresnelCSSAttribute do
  begin
    id:=RegisterCSSAttr(FresnelCSSAttributeNames[Attr]);
    if FFresnelElementBaseAttrID=CSSIDNone then
      FFresnelElementBaseAttrID:=id;
  end;

  // register pseudo attributes
  FFresnelElementBasePseudoID:=CSSIDNone;
  for PseudoAttr in TFresnelCSSPseudo do
  begin
    id:=RegisterCSSPseudo(FresnelCSSPseudoNames[PseudoAttr]);
    if FFresnelElementBasePseudoID=CSSIDNone then
      FFresnelElementBasePseudoID:=id;
  end;
end;

class destructor TFresnelElement.FinalFresnelElementClass;
var
  Kind: TCSSNumericalIDKind;
begin
  for Kind in TCSSNumericalIDKind do
  begin
    FreeAndNil(FCSSNumericalIDs[Kind]);
    FCSSIDToName[Kind]:=nil;
    FCSSIDToNameCount[Kind]:=0;
  end;
end;

class function TFresnelElement.CSSTypeID: TCSSNumericalID;
begin
  Result:=FFresnelElementTypeID;
end;

class function TFresnelElement.CSSTypeName: TCSSString;
begin
  Result:='element';
end;

procedure TFresnelElement.ClearCSSValues;
var
  Attr: TFresnelCSSAttribute;
  i: Integer;
begin
  for Attr in TFresnelCSSAttribute do
  begin
    FCSSAttributes[Attr]:='';
    FCSSComputed[Attr]:='';
  end;
  for i:=0 to NodeCount-1 do
    Nodes[i].ClearCSSValues;
end;

function TFresnelElement.GetCSSAttribute(const AttrID: TCSSNumericalID
  ): TCSSString;
var
  Attr: TFresnelCSSAttribute;
begin
  if (AttrID<FFresnelElementBaseAttrID) or (AttrID>FFresnelElementBaseAttrID+ord(High(TFresnelCSSAttribute))) then
    exit('');
  Attr:=TFresnelCSSAttribute(AttrID-FFresnelElementBaseAttrID);
  Result:=CSSAttribute[Attr];
end;

function TFresnelElement.GetCSSAttributeClass: TCSSString;
begin
  FCSSClasses.Delimiter:=' ';
  Result:=FCSSClasses.DelimitedText;
end;

function TFresnelElement.GetCSSChild(const anIndex: integer): ICSSNode;
begin
  Result:=Nodes[anIndex];
end;

function TFresnelElement.GetCSSChildCount: integer;
begin
  Result:=NodeCount;
end;

function TFresnelElement.GetCSSDepth: integer;
var
  aChild: TFresnelElement;
begin
  // ToDo: store
  Result:=0;
  aChild:=Parent;
  while aChild<>nil do
  begin
    inc(Result);
    aChild:=aChild.Parent;
  end;
end;

function TFresnelElement.GetCSSEmpty: boolean;
begin
  Result:=NodeCount=0;
end;

function TFresnelElement.GetCSSID: TCSSString;
begin
  Result:=Name;
end;

function TFresnelElement.GetCSSIndex: integer;
begin
  // ToDo: store
  if Parent=nil then
    Result:=-1
  else
    Result:=Parent.FChildren.IndexOf(Self);
end;

function TFresnelElement.GetCSSNextOfType: ICSSNode;
var
  i, Cnt: Integer;
  MyID: TCSSNumericalID;
  aChild: TFresnelElement;
begin
  Result:=nil;
  i:=GetCSSIndex;
  if i<0 then exit;
  inc(i);
  MyID:=CSSTypeID;
  Cnt:=Parent.NodeCount;
  while i<Cnt do
  begin
    aChild:=Parent[i];
    if aChild.CSSTypeID=MyID then
      exit(aChild);
    inc(i);
  end;
end;

function TFresnelElement.GetCSSNextSibling: ICSSNode;
var
  i: Integer;
begin
  i:=GetCSSIndex;
  if (i<0) or (i+1>=Parent.NodeCount) then
    Result:=nil
  else
    Result:=Parent[i+1];
end;

function TFresnelElement.GetCSSParent: ICSSNode;
begin
  Result:=Parent;
end;

function TFresnelElement.GetCSSPreviousOfType: ICSSNode;
var
  i: Integer;
  MyID: TCSSNumericalID;
  aChild: TFresnelElement;
begin
  Result:=nil;
  i:=GetCSSIndex;
  if i<0 then exit;
  dec(i);
  MyID:=CSSTypeID;
  while i>=0 do
  begin
    aChild:=Parent[i];
    if aChild.CSSTypeID=MyID then
      exit(aChild);
    dec(i);
  end;
end;

function TFresnelElement.GetCSSPreviousSibling: ICSSNode;
var
  i: Integer;
begin
  i:=GetCSSIndex;
  if i<1 then
    Result:=nil
  else
    Result:=Parent[i-1];
end;

function TFresnelElement.GetCSSPseudo(const AttrID: TCSSNumericalID
  ): TCSSString;
var
  Pseudo: TFresnelCSSPseudo;
begin
  if (AttrID<FFresnelElementBasePseudoID) or (AttrID>FFresnelElementBasePseudoID+ord(High(TFresnelCSSPseudo))) then
    exit('');
  Pseudo:=TFresnelCSSPseudo(AttrID-FFresnelElementBasePseudoID);
  Result:=CSSPseudo[Pseudo];
end;

function TFresnelElement.GetCSSTypeID: TCSSNumericalID;
begin
  Result:=CSSTypeID;
end;

function TFresnelElement.GetCSSTypeName: TCSSString;
begin
  Result:=CSSTypeName;
end;

function TFresnelElement.HasCSSAttribute(const AttrID: TCSSNumericalID
  ): boolean;
begin
  Result:=(AttrID>=FFresnelElementBaseAttrID) and (AttrID<=FFresnelElementBaseAttrID+ord(High(TFresnelCSSAttribute)));
end;

function TFresnelElement.HasCSSClass(const aClassName: TCSSString): boolean;
var
  i: Integer;
begin
  for i:=0 to CSSClasses.Count-1 do
    if aClassName=CSSClasses[i] then
      exit(true);
  Result:=false;
end;

function TFresnelElement.HasCSSPseudo(const AttrID: TCSSNumericalID
  ): boolean;
begin
  Result:=(AttrID>=FFresnelElementBasePseudoID) and (AttrID<=FFresnelElementBasePseudoID+ord(High(TFresnelCSSPseudo)));
end;

procedure TFresnelElement.SetCSSValue(AttrID: TCSSNumericalID;
  Value: TCSSElement);
var
  Attr: TFresnelCSSAttribute;
  s: TCSSString;
begin
  if (AttrID<FFresnelElementBaseAttrID) or (AttrID>ord(High(TFresnelCSSAttribute))+FFresnelElementBaseAttrID) then
    raise Exception.Create('TFresnelElement.SetCSSValue invalid AttrID '+IntToStr(AttrID));
  Attr:=TFresnelCSSAttribute(AttrID-FFresnelElementBaseAttrID);
  s:=Value.AsString;
  {$IFDEF VerboseCSSResolver}
  writeln('TFresnelElement.SetCSSValue ',FresnelAttributeNames[Attr],':="',s,'"');
  {$ENDIF}
  FCSSPosElement:=Value;
  try
    CSSAttribute[Attr]:=s;
  finally
    FCSSPosElement:=nil;
  end;
end;

function TFresnelElement.CheckCSSValue(AttrID: TCSSNumericalID;
  Value: TCSSElement): boolean;
var
  Attr: TFresnelCSSAttribute;
  s: TCSSString;
begin
  if (AttrID<FFresnelElementBaseAttrID) or (AttrID>ord(High(TFresnelCSSAttribute))+FFresnelElementBaseAttrID) then
    raise Exception.Create('TFresnelElement.SetCSSValue invalid AttrID '+IntToStr(AttrID));
  Result:=true;
  Attr:=TFresnelCSSAttribute(AttrID-FFresnelElementBaseAttrID);
  s:=Value.AsString;
  FCSSPosElement:=Value;
  try
    case Attr of
    fcaDisplay: Result:=CheckOrSetCSSDisplay(s,true);
    fcaDisplayBox: Result:=CheckCSSDisplayBox(s);
    fcaDisplayInside: Result:=CheckCSSDisplayInside(s);
    fcaDisplayOutside: Result:=CheckCSSDisplayOutside(s);
    fcaPosition: Result:=CheckCSSPosition(s);
    fcaOverflow: Result:=CheckOrSetCSSOverflow(s,true);
    fcaOverflowX: Result:=CheckCSSOverflowX(s);
    fcaOverflowY: Result:=CheckCSSOverflowY(s);
    fcaZIndex: Result:=CheckCSSZIndex(s);
    fcaClear: Result:=CheckCSSClear(s);
    fcaDirection: Result:=CheckCSSDirection(s);
    fcaLeft,
    fcaTop,
    fcaRight,
    fcaBottom,
    fcaWidth,
    fcaHeight: Result:=CheckCSSLength(Attr,s,[flcNoNegative]);
    fcaBorder: Result:=CheckOrSetCSSBorder(s,true);
    fcaBorderLeft: Result:=CheckOrSetCSSBorderLeft(s,true);
    fcaBorderRight: Result:=CheckOrSetCSSBorderRight(s,true);
    fcaBorderTop: Result:=CheckOrSetCSSBorderTop(s,true);
    fcaBorderBottom: Result:=CheckOrSetCSSBorderBottom(s,true);
    fcaBorderWidth: Result:=CheckOrSetCSSBorderWidth(s,true);
    fcaBorderLeftWidth,
    fcaBorderRightWidth,
    fcaBorderTopWidth,
    fcaBorderBottomWidth: Result:=CheckCSSBorderXWidth(Attr,s);
    fcaBorderColor: Result:=CheckCSSBorderColor(s);
    fcaBoxSizing: Result:=CheckCSSBoxSizing(s);
    fcaFloat: Result:=CheckCSSFloat(s);
    fcaFont: Result:=CheckCSSFontKerning(s);
    fcaFontFamily: ; //Result:=CheckCSSFontFamily(s);
    fcaFontFeatureSettings: ; //Result:=CheckCSSFontFeatureSettings(s);
    fcaFontKerning: Result:=CheckCSSFontKerning(s);
    fcaFontSize: Result:=CheckCSSFontSize(s);
    fcaFontStyle: Result:=CheckCSSFontStyle(s);
    fcaFontWeight: Result:=CheckCSSFontWeight(s);
    fcaFontVariant: Result:=CheckCSSFontVariant(s);
    fcaLineHeight: Result:=CheckCSSLineHeight(s);
    fcaMargin: Result:=CheckOrSetCSSMargin(s,true);
    fcaMarginLeft,
    fcaMarginTop,
    fcaMarginRight,
    fcaMarginBottom: Result:=CheckCSSLength(Attr,s);
    fcaMarginBlock: Result:=CheckOrSetCSSMarginBlock(s,true);
    fcaMarginBlockEnd,
    fcaMarginBlockStart: Result:=CheckCSSLength(Attr,s);
    fcaMarginInline: Result:=CheckOrSetCSSMarginInline(s,true);
    fcaMarginInlineEnd,
    fcaMarginInlineStart: Result:=CheckCSSLength(Attr,s);
    fcaMinWidth,
    fcaMaxWidth,
    fcaMinHeight,
    fcaMaxHeight: Result:=CheckCSSLength(Attr,s,[flcNoNegative]);
    fcaOpacity: Result:=CheckCSSOpacity(s);
    fcaPadding: Result:=CheckOrSetCSSPadding(s,true);
    fcaPaddingLeft,
    fcaPaddingTop,
    fcaPaddingRight,
    fcaPaddingBottom: Result:=CheckCSSLength(Attr,s,[flcNoNegative]);
    fcaVisibility: Result:=CheckCSSVisibility(s);
    fcaBackgroundColor: Result:=CheckCSSBackgroundColor(s);
    fcaColor: Result:=CheckCSSColor(s);
    end;
  finally
    FCSSPosElement:=nil;
  end;
end;

function TFresnelElement.GetCSSInitialAttribute(const AttrID: TCSSNumericalID
  ): TCSSString;
var
  Attr: TFresnelCSSAttribute;
begin
  if (AttrID<FFresnelElementBaseAttrID) or (AttrID>FFresnelElementBaseAttrID+ord(High(TFresnelCSSAttribute))) then
    exit('');
  Attr:=TFresnelCSSAttribute(AttrID-FFresnelElementBaseAttrID);
  case Attr of
  fcaBorderLeftWidth,
  fcaBorderRightWidth,
  fcaBorderTopWidth,
  fcaBorderBottomWidth: Result:='0';
  fcaMarginLeft,
  fcaMarginTop,
  fcaMarginRight,
  fcaMarginBottom: Result:='0';
  fcaPaddingLeft,
  fcaPaddingTop,
  fcaPaddingRight,
  fcaPaddingBottom: Result:='0';
  fcaColor: Result:='#000';
  else
    Result:='';
  end;
end;

function TFresnelElement.GetCSSInheritAttribute(const AttrID: TCSSNumericalID
  ): TCSSString;
var
  El: TFresnelElement;
begin
  El:=Parent;
  while El<>nil do
  begin
    Result:=El.GetCSSAttribute(AttrID);
    if Result<>'' then exit;
    El:=El.Parent;
  end;
  Result:='';
end;

procedure TFresnelElement.ComputeCSS;
const
  NormalAttr = [low(TFresnelCSSAttribute)..high(TFresnelCSSAttribute)]
       -[fcaFontFamily,fcaFontSize,fcaFontStyle,fcaFontWeight];
var
  Attr: TFresnelCSSAttribute;
begin
  // lengths can depend on font, so compute it first
  FFontDescValid:=false;
  ComputeCSSAttribute(fcaFontFamily);
  ComputeCSSAttribute(fcaFontSize);
  ComputeCSSAttribute(fcaFontStyle);
  ComputeCSSAttribute(fcaFontWeight);
  for Attr in NormalAttr do
    ComputeCSSAttribute(Attr);
end;

end.

