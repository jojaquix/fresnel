{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit Fresnel;

{$warn 5023 off : no warning about unused units}
interface

uses
  Fresnel.Controls, Fresnel.DOM, Fresnel.Layouter, Fresnel.LCLControls, Fresnel.Renderer, fresnel.lclevents, fcl.events, 
  fresnel.events, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('Fresnel', @Register);
end.
