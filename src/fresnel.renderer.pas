unit Fresnel.Renderer;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Math, FPImage, Fresnel.DOM, Fresnel.Controls,
  Fresnel.Layouter, LazLoggerBase;

type

  { TFresnelRenderer }

  TFresnelRenderer = class(TComponent)
  private
    FSubPixel: boolean;
  protected
    FOrigin: TFresnelPoint;
    procedure FillRect(const aColor: TFPColor; const aRect: TFresnelRect); virtual; abstract;
    procedure Line(const aColor: TFPColor; const x1, y1, x2, y2: TFresnelLength); virtual; abstract;
    procedure TextOut(const aLeft, aTop: TFresnelLength; const aFont: IFresnelFont; const aColor: TFPColor; const aText: string); virtual; abstract;
    procedure MathRoundRect(var r: TFresnelRect);
    procedure DrawElement(El: TFresnelElement); virtual;
    procedure UpdateRenderedAttributes(El: TFresnelElement); virtual;
    procedure SetOrigin(const AValue: TFresnelPoint);
  public
    procedure Draw(Viewport: TFresnelViewport); virtual;
    property SubPixel: boolean read FSubPixel write FSubPixel;
    property Origin: TFresnelPoint read FOrigin write SetOrigin;
  end;

implementation

{ TFresnelRenderer }

procedure TFresnelRenderer.SetOrigin(const AValue: TFresnelPoint);
begin
  if CompareFresnelPoint(FOrigin,AValue)=0 then Exit;
  FOrigin:=AValue;
end;

procedure TFresnelRenderer.MathRoundRect(var r: TFresnelRect);
begin
  r.Left:=round(r.Left);
  r.Right:=round(r.Right);
  r.Top:=round(r.Top);
  r.Bottom:=round(r.Bottom);
end;

procedure TFresnelRenderer.DrawElement(El: TFresnelElement);
var
  LNode: TSimpleFresnelLayoutNode;
  aBackgroundColor, aBorderColor, aCaption, aColor: String;
  aBackgroundColorFP, aBorderColorFP, aColorFP: TFPColor;
  aLeft, aTop, aRight, aBottom,
    aMarginLeft, aMarginTop, aMarginRight, aMarginBottom,
    aBorderLeft, aBorderRight, aBorderTop, aBorderBottom,
    aPaddingLeft, aPaddingRight, aPaddingTop, aPaddingBottom: TFresnelLength;
  aBorderBox, aContentBox: TFresnelRect;
  i: Integer;
  OldOrigin: TFresnelPoint;
  VP: TFresnelViewport;
begin
  //DebugLn(['TFresnelRenderer.DrawElement ',El.GetPath,' Origin=',dbgs(Origin)]);
  LNode:=TSimpleFresnelLayoutNode(El.LayoutNode);
  if LNode.SkipRendering then exit;

  El.Rendered:=true;
  if (El is TFresnelViewport) then
  begin
    aLeft:=0;
    aTop:=0;
    VP:=TFresnelViewport(El);
    aContentBox.Left:=0;
    aContentBox.Top:=0;
    aContentBox.Right:=Vp.Width;
    aContentBox.Bottom:=Vp.Height;
    El.RenderedBorderBox:=aContentBox;
    El.RenderedContentBox:=aContentBox;
  end else begin
    aLeft:=El.GetRenderedCSSLength(fcaLeft,false);
    aTop:=El.GetRenderedCSSLength(fcaTop,false);
    aRight:=El.GetRenderedCSSLength(fcaRight,false);
    aBottom:=El.GetRenderedCSSLength(fcaBottom,false);

    aMarginLeft:=El.GetRenderedCSSLength(fcaMarginLeft,false);
    aMarginRight:=El.GetRenderedCSSLength(fcaMarginRight,false);
    aMarginTop:=El.GetRenderedCSSLength(fcaMarginTop,false);
    aMarginBottom:=El.GetRenderedCSSLength(fcaMarginBottom,false);

    aBorderLeft:=El.GetRenderedCSSLength(fcaBorderLeftWidth,false);
    aBorderRight:=El.GetRenderedCSSLength(fcaBorderRightWidth,false);
    aBorderTop:=El.GetRenderedCSSLength(fcaBorderTopWidth,false);
    aBorderBottom:=El.GetRenderedCSSLength(fcaBorderBottomWidth,false);

    aPaddingLeft:=El.GetRenderedCSSLength(fcaPaddingLeft,false);
    aPaddingRight:=El.GetRenderedCSSLength(fcaPaddingRight,false);
    aPaddingTop:=El.GetRenderedCSSLength(fcaPaddingTop,false);
    aPaddingBottom:=El.GetRenderedCSSLength(fcaPaddingBottom,false);

    aBorderBox.Left:=aLeft+aMarginLeft;
    aBorderBox.Top:=aTop+aMarginTop;
    aBorderBox.Right:=aRight-aMarginRight;
    aBorderBox.Bottom:=aBottom-aMarginBottom;
    El.RenderedBorderBox:=aBorderBox;
    if not SubPixel then
      MathRoundRect(aBorderBox);

    aContentBox.Left:=aLeft+aMarginLeft+aBorderLeft+aPaddingLeft;
    aContentBox.Top:=aTop+aMarginTop+aBorderTop+aPaddingTop;
    aContentBox.Right:=aRight-aMarginRight-aBorderRight-aPaddingRight;
    aContentBox.Bottom:=aBottom-aMarginBottom-aBorderBottom-aPaddingBottom;
    El.RenderedContentBox:=aContentBox;
    if not SubPixel then
      MathRoundRect(aContentBox);

    //DebugLn(['TFresnelRenderer.DrawElement ',El.Name,' Border=',dbgs(aBorderBox),' Content=',dbgs(aContentBox)]);

    aBackgroundColor:=El.CSSRenderedAttribute[fcaBackgroundColor];
    if not CSSToFPColor(aBackgroundColor,aBackgroundColorFP) then
      aBackgroundColorFP:=colTransparent;
    if aBackgroundColorFP.Alpha<>alphaTransparent then
    begin
      FillRect(aBackgroundColorFP,aBorderBox);
    end;

    aBorderColor:=El.CSSRenderedAttribute[fcaBorderColor];
    if not CSSToFPColor(aBorderColor,aBorderColorFP) then
      aBorderColorFP:=colTransparent;
    if aBorderColorFP.Alpha<>alphaTransparent then
    begin
      //debugln(['TFresnelRenderer.DrawElement drawing border ',El.Name]);
      // left border
      for i:=0 to Ceil(aBorderLeft)-1 do
        Line(aBorderColorFP,aBorderBox.Left+i,aBorderBox.Top,aBorderBox.Left+i,aBorderBox.Bottom);
      // right border
      for i:=0 to ceil(aBorderRight)-1 do
        Line(aBorderColorFP,aBorderBox.Right-i,aBorderBox.Top,aBorderBox.Right-i,aBorderBox.Bottom);
      // top border
      for i:=0 to ceil(aBorderTop)-1 do
        Line(aBorderColorFP,aBorderBox.Left,aBorderBox.Top+i,aBorderBox.Right,aBorderBox.Top+i);
      // bottom border
      for i:=0 to ceil(aBorderBottom)-1 do
        Line(aBorderColorFP,aBorderBox.Left,aBorderBox.Bottom-i,aBorderBox.Right,aBorderBox.Bottom-i);
    end;

    if El is TCustomLabel then
    begin
      aCaption:=TCustomLabel(El).RenderedCaption;
      if aCaption<>'' then
      begin
        aColor:=El.GetRenderedCSString(fcaColor,true);
        if not CSSToFPColor(aColor,aColorFP) then
          aColorFP:=colTransparent;
        if aColorFP.Alpha<>alphaTransparent then
        begin
          TextOut(aContentBox.Left,aContentBox.Top,El.Font,aColorFP,aCaption);
        end;
      end;
    end;
  end;

  OldOrigin:=Origin;
  Origin:=OldOrigin+El.RenderedContentBox.TopLeft;
  for i:=0 to LNode.NodeCount-1 do
  begin
    DrawElement(TSimpleFresnelLayoutNode(LNode.Nodes[i]).Element);
  end;
  Origin:=OldOrigin;
end;

procedure TFresnelRenderer.UpdateRenderedAttributes(El: TFresnelElement);
var
  LNode: TSimpleFresnelLayoutNode;
  i: Integer;
begin
  LNode:=TSimpleFresnelLayoutNode(El.LayoutNode);
  if LNode.SkipRendering then exit;

  El.UpdateRenderedAttributes;
  for i:=0 to LNode.NodeCount-1 do
    UpdateRenderedAttributes(TSimpleFresnelLayoutNode(LNode.Nodes[i]).Element);
end;

procedure TFresnelRenderer.Draw(Viewport: TFresnelViewport);
begin
  //debugln(['TFresnelRenderer.Draw Origin=',dbgs(Origin)]);
  UpdateRenderedAttributes(Viewport);
  DrawElement(Viewport);
end;

end.

