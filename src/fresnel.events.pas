unit fresnel.events;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, fcl.events;

{$ScopedEnums ON}

Const
  evtUnknown = 0;
  evtKeyDown = 1;
  evtKeyUp = 2;
  evtKeyPress = 3;
  evtEnter = 4;
  evtLeave = 5;
  evtClick = 6;
  evtDblClick = 7;
  evtChange = 8;
  evtDrag = 9;
  evtDragEnd = 10;
  evtDragEnter = 11;
  evtDragOver = 12;
  evtDragLeave = 13;
  evtDragStart = 14;
  evtDrop = 15;
  evtMouseMove = 16;
  evtMouseDown = 17;
  evtMouseUp = 18;
  evtMouseOver = 19;
  evtMouseEnter = 20;
  evtMouseLeave = 21;
  evtMouseWheel = 22;
  evtFocusIn = 23;
  evtFocusOut = 24;
  evtFocus = 25;
  evtBlur = 26;





  MaxFresnelEvents = evtBlur;


Type

  { TFresnelEvent }

  TFresnelEvent = Class(TAbstractEvent)
  private
    FDefaultPrevented: Boolean;
  public
    Class Function FresnelEventID : TEventID; virtual; abstract;
    class function StandardEventName(aEventID: TEventID): TEventName;
    class function EventName: TEventName; override;
    Procedure PreventDefault; virtual;
    Property DefaultPrevented : Boolean Read FDefaultPrevented;
  end;
  TFresnelEventClass = Class of TFresnelEvent;

  TFresnelEventHandler = TEventHandler;
  TFresnelEventCallBack = TEventCallBack;
  TFresnelEventHandlerRef = TEventHandlerRef;


  TFresnelUIEvent = class(TFresnelEvent)

  end;

  TMouseButton = (mbMain,mbAux,mbSecond,mbFourth,mbFifth);
  TMouseButtons = set of TMouseButton;

  { TFresnelMouseEvent }

  TFresnelMouseEventInit = Record
    Button: TMouseButton;
    Buttons: TMouseButtons;
    PagePos: TPoint;
    ScreenPos: TPoint;
    Shiftstate: TShiftState;
    ControlPos : TPoint;
  end;

  TFresnelMouseEvent = Class(TFresnelUIEvent)
  private
    FInit : TFresnelMouseEventInit;
    function GetShiftKey(AIndex: Integer): Boolean;
  Public
    Constructor Create(const aInit : TFresnelMouseEventInit); overload;
    Procedure InitEvent(const aInit : TFresnelMouseEventInit);
    Property PageX : Integer Read FInit.PagePos.X;
    Property PageY : Integer Read FInit.PagePos.Y;
    Property ScreenX : Integer Read FInit.ScreenPos.X;
    Property ScreenY : Integer Read FInit.ScreenPos.Y;
    Property X : Integer Read FInit.ControlPos.X;
    Property Y : Integer Read FInit.ControlPos.Y;
    Property Buttons: TMouseButtons Read FInit.Buttons;
    Property Button : TMouseButton Read FInit.Button;
    Property ShiftState : TShiftState Read FInit.Shiftstate;
    Property Altkey : Boolean Index Ord(ssAlt) read GetShiftKey;
    Property MetaKey : Boolean Index Ord(ssMeta) read GetShiftKey;
    Property CtrlKey : Boolean Index Ord(ssCtrl) read GetShiftKey;
    Property ShiftKey : Boolean Index Ord(ssShift) read GetShiftKey;
  end;

  { TFresnelMouseClickEvent }

  TFresnelMouseClickEvent = class(TFresnelMouseEvent)
    class function FresnelEventID : TEventID; override;
  end;

  TFresnelKeyEventInit = Record
    ShiftState : TShiftState;
    IsComposing : Boolean;
    NumKey : Cardinal;
    Key : String;
    Code : ShortString;
  end;

  { TFresnelKeyEvent }

  TFresnelKeyEvent = class(TFresnelEvent)
  Private
    FInit : TFresnelKeyEventInit;
    function GetShiftKey(AIndex: Integer): Boolean;
  Public
    Constructor Create(const aInit : TFresnelKeyEventInit);
    Property Code: ShortString Read FInit.Code;
    Property Key : String Read Finit.Key;
    Property NumKey : Cardinal Read Finit.NumKey;
    Property Altkey : Boolean Index Ord(ssAlt) read GetShiftKey;
    Property MetaKey : Boolean Index Ord(ssMeta) read GetShiftKey;
    Property CtrlKey : Boolean Index Ord(ssCtrl) read GetShiftKey;
    Property ShiftKey : Boolean Index Ord(ssShift) read GetShiftKey;
  end;

  { TFresnelKeyUpEvent }

  TFresnelKeyUpEvent = class(TFresnelKeyEvent)
    Class function FresnelEventID: TEventID; override;
  end;

  { TFresnelKeyDownEvent }

  TFresnelKeyDownEvent = class(TFresnelKeyEvent)
    Class function FresnelEventID: TEventID; override;
  end;

  { TFresnelChangeEvent }

  TFresnelChangeEvent = class(TFresnelEvent)
    Class Function FresnelEventID: TEventID; override;
  end;

  { TFresnelFocusEvent }

  TFresnelFocusEvent = Class(TFresnelUIEvent)
    Class function FresnelEventID: TEventID; override;
  end;

  { TFresnelFocusInEvent }

  TFresnelFocusInEvent = Class(TFresnelFocusEvent)
    Class function FresnelEventID: TEventID; override;
  end;

  { TFresnelFocusOutEvent }

  TFresnelFocusOutEvent = Class(TFresnelFocusEvent)
    Class function FresnelEventID: TEventID; override;
  end;

  { TFresnelBlurEvent }

  TFresnelBlurEvent = Class(TFresnelUIEvent)
    Class function FresnelEventID: TEventID; override;
  end;

  // List taken from
  // https://rawgit.com/w3c/input-events/v1/index.html#interface-InputEvent-Attributes

  TFresnelInputType = (
   insertText,insertReplacementText,insertLineBreak,
   insertParagraph,insertOrderedList,insertUnorderedList,
   insertHorizontalRule,insertFromYank,insertFromDrop,
   insertFromPaste,insertFromPasteAsQuotation,insertTranspose,
   insertCompositionText,insertLink,
   deleteWordBackward,deleteWordForward,deleteSoftLineBackward,
   deleteSoftLineForward,deleteEntireSoftLine,deleteHardLineBackward,
   deleteHardLineForward,deleteByDrag,deleteByCut,
   deleteContent,deleteContentBackward,deleteContentForward,
   historyUndo,historyRedo,
   formatBold,formatItalic,formatUnderline,formatStrikeThrough,
   formatSuperscript,formatSubscript,formatJustifyFull,formatJustifyCenter,
   formatJustifyRight,formatJustifyLeft,formatIndent, formatOutdent,
   formatRemove,formatSetBlockTextDirection,formatSetInlineTextDirection,
   formatBackColor,formatFontColor,formatFontName
  );
  TFresnelInputEventInit = record
    data : string;
    inputtype : TFresnelInputType;
  end;

  { TFresnelInputEvent }

  TFresnelInputEvent = Class(TFresnelUIEvent)
  Private
    FInit : TFresnelInputEventInit;
    function GetInputType: string;
  Public
    Constructor Create(aInit : TFresnelInputEventInit);
    Property Data : String Read FInit.Data;
    Property FresnelInputType : TFresnelInputType Read FInit.inputtype;
    Property InputType : string Read GetInputType;
  end;


  TDeltaMode = (pixel,line,page);
  TFresnelWheelInit = record
    deltaMode : TDeltaMode;
    deltaX : Integer;
    deltaY : Integer;
    DeltaZ : Integer;
  end;

  TFresnelWheelEventInit = Record
    MouseInit : TFresnelMouseEventInit;
    WheelInit : TFresnelWheelInit;
  end;

  { TFresnelWheelEvent }

  TFresnelWheelEvent = Class(TFresnelMouseEvent)
  Private
    FWheelInit : TFresnelWheelInit;
  Public
    Constructor Create(aInit : TFresnelWheelEventInit); reintroduce;
    Property DeltaMode : TDeltaMode Read FWheelInit.DeltaMode;
    Property DeltaX : Integer Read FWheelInit.DeltaX;
    Property DeltaY : Integer Read FWheelInit.DeltaY;
    Property DeltaZ : Integer Read FWheelInit.DeltaZ;
  end;

   { TFresnelEventDispatcher }

   TFresnelEventDispatcher = Class(TEventDispatcher)
   Private
     Class Var _Registry : TEventRegistry;
   Protected
     class Function CreateFresnelRegistry : TEventRegistry; virtual;
     Function GetRegistry: TEventRegistry; override;
   Public
     Class Function FresnelRegistry : TEventRegistry;
     Class Procedure RegisterFresnelEvents;
     Class Destructor Done;
   end;


implementation

uses TypInfo;

Const
  FresnelEventNames : Array[0..MaxFresnelEvents] of TEventName = (
    '?',
    'KeyDown',
    'KeyUp',
    'KeyPress',
    'Enter',
    'Leave',
    'Click',
    'DblClick',
    'Change',
    'Drag',
    'DragEnd',
    'DragEnter',
    'DragOver',
    'DragLeave',
    'DragStart',
    'Drop',
    'MouseMove',
    'MouseDown',
    'MouseUp',
    'MouseOver',
    'MouseEnter',
    'MouseLeave',
    'MouseWheel',
    'FocusIn',
    'FocusOut',
    'Focus',
    'Blur'
  );

{ TFresnelWheelEvent }

constructor TFresnelWheelEvent.Create(aInit: TFresnelWheelEventInit);
begin
  Inherited Create(aInit.MouseInit);
  FWheelInit:=aInit.WheelInit;
end;

{ TFresnelInputEvent }

function TFresnelInputEvent.GetInputType: string;
begin
  Result:=GetEnumName(TypeInfo(TFresnelInputType),Ord(FInit.inputtype));
end;

constructor TFresnelInputEvent.Create(aInit: TFresnelInputEventInit);
begin
  FInit:=AInit;
end;

{ TFresnelBlurEvent }

class function TFresnelBlurEvent.FresnelEventID: TEventID;
begin
  Result:=evtBlur;
end;

{ TFresnelFocusOutEvent }

class function TFresnelFocusOutEvent.FresnelEventID: TEventID;
begin
  Result:=evtFocusOut;
end;

{ TFresnelFocusInEvent }

class function TFresnelFocusInEvent.FresnelEventID: TEventID;
begin
  Result:=evtFocusin;
end;

{ TFresnelFocusEvent }

class function TFresnelFocusEvent.FresnelEventID: TEventID;
begin
  Result:=evtFocus;
end;

{ TFresnelKeyDownEvent }

class function TFresnelKeyDownEvent.FresnelEventID: TEventID;
begin
  Result:=evtKeyDown;
end;

{ TFresnelKeyUpEvent }

class function TFresnelKeyUpEvent.FresnelEventID: TEventID;
begin
  Result:=evtKeyUp;
end;

{ TFresnelKeyEvent }


function TFresnelKeyEvent.GetShiftKey(AIndex: Integer): Boolean;
begin
  Result:=TShiftStateEnum(aIndex) in Finit.ShiftState;
end;


constructor TFresnelKeyEvent.Create(const aInit: TFresnelKeyEventInit);
begin
  FInit:=aInit;
end;

{ TFresnelMouseEvent }

function TFresnelMouseEvent.GetShiftKey(AIndex: Integer): Boolean;
begin
  Result:=TShiftStateEnum(aIndex) in ShiftState;
end;

constructor TFresnelMouseEvent.Create(const aInit: TFresnelMouseEventInit);
begin
  InitEvent(aInit);
end;

procedure TFresnelMouseEvent.InitEvent(const aInit: TFresnelMouseEventInit);
begin
  FInit:=aInit;
end;


{ TFresnelChangeEvent }

class function TFresnelChangeEvent.FresnelEventID: TEventID;
begin
  Result:=evtChange;
end;

{ TFresnelMouseClickEvent }

class function TFresnelMouseClickEvent.FresnelEventID: TEventID;
begin
  Result:=evtClick;
end;

{ TFresnelEvent }

class function TFresnelEvent.StandardEventName(aEventID : TEventID): TEventName;
begin
  If (aEventID>=0) and (aEventID<Length(FresnelEventNames)) then
    Result:=FresnelEventNames[aEventID]
  else
    Result:=IntToStr(aEventID);
end;

class function TFresnelEvent.EventName: TEventName;
begin
  Result:=StandardEventName(FresnelEventID);
end;

procedure TFresnelEvent.PreventDefault;
begin
  FDefaultPrevented:=True;
end;

{ TFresnelEventDispatcher }
Type

  { TFresnelRegistry }

  TFresnelRegistry = Class(TEventRegistry)
    Class function DefaultIDOffset : TEventID; override;
  end;

{ TFresnelRegistry }

class function TFresnelRegistry.DefaultIDOffset: TEventID;
begin
  Result:=MaxFresnelEvents+1;
end;

class function TFresnelEventDispatcher.CreateFresnelRegistry: TEventRegistry;
begin
  Result:=TFresnelRegistry.Create;
end;

function TFresnelEventDispatcher.GetRegistry: TEventRegistry;
begin
  Result:=FresnelRegistry;
end;

class function TFresnelEventDispatcher.FresnelRegistry: TEventRegistry;
begin
  if _Registry=Nil then
    _Registry:=CreateFresnelRegistry;
  Result:=_Registry;
end;

class procedure TFresnelEventDispatcher.RegisterFresnelEvents;

  Procedure R(aClass : TFresnelEventClass);

  begin
    FresnelRegistry.RegisterEventWithID(aClass.FresnelEventID,aClass);
  end;

begin
  R(TFresnelChangeEvent);
  R(TFresnelMouseClickEvent);
end;

class destructor TFresnelEventDispatcher.Done;
begin
  FreeAndNil(_Registry);
end;

end.

