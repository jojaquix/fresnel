# Fresnel

This repository contains the sources for Project Fresnel

## What is project Fresnel ?

Project Fresnel is a new UI paradigma for Lazarus projects. 
Instead of using LCL controls, CSS-based custom drawn controls will be used to create your UI.

## Why is this project needed ?

The design of the VCL and LCL is old. In the browser, UX and UI have evolved far beyond
what the LCL has to offer, largely thanks to the strength of CSS. The choice for CSS
as a mechanism for layouting and display is therefore logical. 
This will also allow to reuse existing CSS frameworks.

## What's with the name ?
UI is about look and feel. Look and feel means light. 
[Fresnel](https://en.wikipedia.org/wiki/Augustin-Jean_Fresnel) is a French
Physicist who made important contributions to the wave theory of light.

## Goals of project Fresnel:

- 100% Pascal code.
- Create a set of controls that are independent of the LCL.
- The layout and look of the controls are governed by CSS.
- A Lazarus application must be able to run LCL forms alongside 'Fresnel' forms.
   This will ensure easy porting.
- Different drawing backends must be possible.

