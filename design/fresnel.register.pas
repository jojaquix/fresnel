{
 Copyright (C) 2022 Mattias Gaertner mattias@freepascal.org

*****************************************************************************
 This file is part of the Fresnel project.

 See the file COPYING.modifiedLGPL.txt, included in this distribution,
 for details about the license.
*****************************************************************************
}
unit Fresnel.Register;

{$mode objfpc}{$H+}

interface

uses
  LCLProc, LCLType, Classes, SysUtils, FormEditingIntf, PropEdits, LCLIntf,
  Graphics, Controls, Forms, ProjectIntf, LazLoggerBase, Fresnel.DOM,
  Fresnel.Controls, Fresnel.LCLControls, Fresnel.StylePropEdit;

type

  { TFresnelFormMediator - mediator for TFresnelForm }

  TFresnelFormMediator = class(TDesignerMediator,IFresnelFormDesigner)
  private
    FDsgnForm: TFresnelForm;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure SetLCLForm(const AValue: TForm); override;
  public
    // needed by the Lazarus form editor
    class function CreateMediator(TheOwner, aForm: TComponent): TDesignerMediator;
      override;
    class function FormClass: TComponentClass; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function ComponentAtPos(p: TPoint; MinClass: TComponentClass;
                                Flags: TDMCompAtPosFlags): TComponent; override;
    function ComponentIsIcon(AComponent: TComponent): boolean; override;
    function ComponentIsVisible(AComponent: TComponent): Boolean; override;
    function GetComponentOriginOnForm(AComponent: TComponent): TPoint; override;
    function ParentAcceptsChild(Parent: TComponent;
                                ChildClass: TComponentClass): boolean; override;
    procedure GetBounds(AComponent: TComponent; out CurBounds: TRect); override;
    procedure GetClientArea(AComponent: TComponent; out
                      CurClientArea: TRect; out ScrollOffset: TPoint); override;
    procedure Paint; override;
    procedure SetBounds(AComponent: TComponent; NewBounds: TRect); override;
  public
    // needed by Fresnel
    procedure InvalidateRect(Sender: TObject; ARect: TRect; Erase: boolean);
    procedure SetDesignerFormBounds(Sender: TObject; NewBounds: TRect);
    function GetDesignerClientHeight: integer;
    function GetDesignerClientWidth: integer;
    property DsgnForm: TFresnelForm read FDsgnForm;
  end;

  { TFileDescPascalUnitWithFresnelForm }

  TFileDescPascalUnitWithFresnelForm = class(TFileDescPascalUnitWithResource)
  public
    constructor Create; override;
    function GetInterfaceUsesSection: string; override;
    function GetLocalizedName: string; override;
    function GetLocalizedDescription: string; override;
  end;

  { TFresnelStylePropertyEditor }

  TFresnelStylePropertyEditor = class(TStringPropertyEditor)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure Edit; override;
  end;

  { TFresnelStyleSheetPropertyEditor }

  TFresnelStyleSheetPropertyEditor = class(TClassPropertyEditor)
  public
    procedure Edit; override;
    function GetAttributes: TPropertyAttributes; override;
  end;

procedure Register;

implementation

{$R fresneldsgnimg.res}

procedure Register;
begin
  FormEditingHook.RegisterDesignerMediator(TFresnelFormMediator);
  RegisterComponents('Fresnel',[TDiv,TSpan,TLabel,TBody]);
  RegisterProjectFileDescriptor(TFileDescPascalUnitWithFresnelForm.Create,
                                FileDescGroupName);

  RegisterPropertyEditor(TypeInfo(String), TFresnelElement, 'Style', TFresnelStylePropertyEditor);
  RegisterPropertyEditor(TypeInfo(String), TCustomFresnelForm, 'Style', THiddenPropertyEditor);
  RegisterPropertyEditor(TypeInfo(TStrings), TCustomFresnelForm, 'Stylesheet', TFresnelStyleSheetPropertyEditor);
end;

{ TFresnelFormMediator }

procedure TFresnelFormMediator.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if FDsgnForm=AComponent then
    begin
      FDsgnForm.Designer:=nil;
      FDsgnForm:=nil;
    end;
  end;
end;

procedure TFresnelFormMediator.SetLCLForm(const AValue: TForm);
begin
  if LCLForm=AValue then exit;
  inherited SetLCLForm(AValue);
  if FDsgnForm=nil then exit;
  FDsgnForm.Canvas:=LCLForm.Canvas;
end;

class function TFresnelFormMediator.CreateMediator(TheOwner, aForm: TComponent
  ): TDesignerMediator;
var
  Mediator: TFresnelFormMediator;
begin
  Result:=inherited CreateMediator(TheOwner,aForm);
  Mediator:=TFresnelFormMediator(Result);
  Mediator.FDsgnForm:=aForm as TFresnelForm;
  Mediator.FDsgnForm.Designer:=Mediator;
  Mediator.FreeNotification(aForm);
end;

class function TFresnelFormMediator.FormClass: TComponentClass;
begin
  Result:=TFresnelForm;
end;

procedure TFresnelFormMediator.GetBounds(AComponent: TComponent; out
  CurBounds: TRect);
var
  El: TFresnelElement;
  aBox: TFresnelRect;
begin
  if AComponent=FDsgnForm then
  begin
    CurBounds:=FDsgnForm.FormBoundsRect;
  end else if AComponent is TFresnelElement then
  begin
    El:=TFresnelElement(AComponent);
    aBox:=El.GetBoundingClientRect;
    FresnelRectToRect(aBox,CurBounds);
  end else
    inherited GetBounds(AComponent,CurBounds);
  //debugln(['TFresnelFormMediator.GetBounds ',DbgSName(AComponent),' ',dbgs(CurBounds)]);
end;

procedure TFresnelFormMediator.SetBounds(AComponent: TComponent;
  NewBounds: TRect);
begin
  //debugln(['TFresnelFormMediator.SetBounds ',DbgSName(AComponent),' ',dbgs(NewBounds)]);
  if AComponent=FDsgnForm then
  begin
    FDsgnForm.FormBoundsRect:=NewBounds;
  end else if AComponent is TFresnelElement then
  begin
    // bounds are controlled by CSS
    // todo: absolute, fixed
  end else begin
    inherited SetBounds(AComponent, NewBounds);
  end;
end;

procedure TFresnelFormMediator.GetClientArea(AComponent: TComponent; out
  CurClientArea: TRect; out ScrollOffset: TPoint);
begin
  if AComponent=FDsgnForm then
  begin
    CurClientArea:=FDsgnForm.Form.ClientRect;
    ScrollOffset:=Point(0,0);
  end else begin
    inherited GetClientArea(AComponent, CurClientArea, ScrollOffset);
  end;
end;

function TFresnelFormMediator.GetComponentOriginOnForm(AComponent: TComponent
  ): TPoint;
var
  El: TFresnelElement;
  BorderBox: TFresnelRect;
begin
  if AComponent=FDsgnForm then
  begin
    Result:=Point(0,0);
  end else if AComponent is TFresnelElement then
  begin
    El:=TFresnelElement(AComponent);
    if not El.Rendered then
      exit(Point(0,0));
    BorderBox:=El.GetBoundingClientRect;
    Result.X:=round(BorderBox.Left);
    Result.Y:=round(BorderBox.Top);
  end else
    Result:=inherited GetComponentOriginOnForm(AComponent);
end;

procedure TFresnelFormMediator.Paint;
begin
  if FDsgnForm=nil then exit;
  FDsgnForm.Renderer.Draw(FDsgnForm);
end;

function TFresnelFormMediator.ComponentIsIcon(AComponent: TComponent): boolean;
begin
  if AComponent is TFresnelElement then
    Result:=false
  else
    Result:=inherited ComponentIsIcon(AComponent);
end;

function TFresnelFormMediator.ComponentIsVisible(AComponent: TComponent
  ): Boolean;
begin
  if AComponent=FDsgnForm then
    Result:=true
  else if AComponent is TFresnelElement then
    Result:=TFresnelElement(AComponent).Rendered
  else
    Result:=true;
end;

function TFresnelFormMediator.ComponentAtPos(p: TPoint;
  MinClass: TComponentClass; Flags: TDMCompAtPosFlags): TComponent;
begin
  if Flags=[] then ;
  Result:=DsgnForm.GetElementAt(p.X,p.Y);
  if Result=nil then exit;
  if (MinClass<>nil) and not Result.InheritsFrom(MinClass) then
    Result:=nil;
end;

function TFresnelFormMediator.ParentAcceptsChild(Parent: TComponent;
  ChildClass: TComponentClass): boolean;
begin
  //debugln(['TFresnelFormMediator.ParentAcceptsChild START Parent=',DbgSName(Parent),' Child=',DbgSName(ChildClass)]);
  if ChildClass.InheritsFrom(TControl) then
    Result:=false
  else if ChildClass.InheritsFrom(TFresnelViewport) then
    Result:=false
  else if Parent is TFresnelElement then
  begin
    if Parent is TReplacedElement then
      exit(false);
    Result:=ChildClass.InheritsFrom(TFresnelElement);
  end else
    Result:=inherited ParentAcceptsChild(Parent, ChildClass);
  //debugln(['TFresnelFormMediator.ParentAcceptsChild END Parent=',DbgSName(Parent),' Child=',DbgSName(ChildClass),' Result=',Result]);
end;

constructor TFresnelFormMediator.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TFresnelFormMediator.Destroy;
begin
  if FDsgnForm<>nil then FDsgnForm.Designer:=nil;
  FDsgnForm:=nil;
  inherited Destroy;
end;

procedure TFresnelFormMediator.InvalidateRect(Sender: TObject; ARect: TRect;
  Erase: boolean);
begin
  if (LCLForm=nil) or (not LCLForm.HandleAllocated) then exit;
  LCLIntf.InvalidateRect(LCLForm.Handle,@ARect,Erase);
end;

procedure TFresnelFormMediator.SetDesignerFormBounds(Sender: TObject;
  NewBounds: TRect);
begin
  if LCLForm=nil then exit;
  LCLForm.BoundsRect:=NewBounds;
end;

function TFresnelFormMediator.GetDesignerClientHeight: integer;
begin
  if LCLForm=nil then
    Result:=FDsgnForm.Form.ClientHeight
  else
    Result:=LCLForm.ClientHeight;
end;

function TFresnelFormMediator.GetDesignerClientWidth: integer;
begin
  if LCLForm=nil then
    Result:=FDsgnForm.Form.ClientWidth
  else
    Result:=LCLForm.ClientWidth;
end;

{ TFileDescPascalUnitWithFresnelForm }

constructor TFileDescPascalUnitWithFresnelForm.Create;
begin
  inherited Create;
  Name:='FresnelForm';
  ResourceClass:=TFresnelForm;
end;

function TFileDescPascalUnitWithFresnelForm.GetInterfaceUsesSection: string;
begin
  Result:='Classes, SysUtils, Fresnel.LCLControls, Fresnel.DOM, Fresnel.Controls';
end;

function TFileDescPascalUnitWithFresnelForm.GetLocalizedName: string;
begin
  Result:='FresnelForm';
end;

function TFileDescPascalUnitWithFresnelForm.GetLocalizedDescription: string;
begin
  Result:='Create a new Fresnel form';
end;

{ TFresnelStylePropertyEditor }

function TFresnelStylePropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paDialog, paRevertable];
end;

procedure TFresnelStylePropertyEditor.Edit;
var
  TheDialog : TStylePropEditDialog;
  AString: String;
begin
  AString := GetStrValue; // read first to get nicer error messages

  TheDialog := TStylePropEditDialog.Create(nil);
  try
    TheDialog.Caption := 'CSS Inline Style Editor';
    TheDialog.Editor := Self;
    TheDialog.CSSSynEdit.Text := AString;
    TheDialog.CSSSynEditChange(nil);
    if (TheDialog.ShowModal = mrOK) then
    begin
      TheDialog.Apply;
    end;
  finally
    TheDialog.Free;
  end;
end;

{ TFresnelStyleSheetPropertyEditor }

procedure TFresnelStyleSheetPropertyEditor.Edit;
var
  TheDialog : TStylePropEditDialog;
  aList: TStrings;
begin
  aList := TStrings(GetObjectValue);

  TheDialog := TStylePropEditDialog.Create(nil);
  try
    TheDialog.Caption := 'CSS Stylesheet Editor';
    TheDialog.Editor := Self;
    TheDialog.CSSSynEdit.Text := aList.Text;
    TheDialog.CSSSynEditChange(nil);
    if (TheDialog.ShowModal = mrOK) then
    begin
      TheDialog.Apply;
    end;
  finally
    TheDialog.Free;
  end;
end;

function TFresnelStyleSheetPropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paDialog, paRevertable, paReadOnly];
end;

end.

